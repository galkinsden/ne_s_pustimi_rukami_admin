import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {CommonModule, DatePipe} from '@angular/common';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import {PagesModule} from './pages/pages.module';
import {LoginComponent} from './pages/login/login.component';
import {RegisterComponent} from './modules/register/register.component';
import {UserService} from './libraries/user/user.service';
import {AuthService} from './libraries/auth/auth.service';
import {AuthGuard} from './pages/guard.service';
import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';
import {AppHttpInterceptor} from './connectors/app-http.interceptor';
import {PlanModule} from "./modules/plan/plan.module";
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from "@angular/material";
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from "@angular/material-moment-adapter";
import {MenuModule} from "./modules/menu/menu.module";
import {UnSnackBarModule} from "./libraries/common/ui/snackbar/un-snack-bar.module";
import {UnSnackBarService} from "./libraries/common/ui/snackbar/un-snack-bar.service";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {WrapperPageComponent} from "./pages/wrapper-page/wrapper-page.component";
import {SettingsModule} from "./modules/settings/settings.module";
import {UnChartsModule} from "./modules/charts/charts.module";
import {RegisterModule} from "./modules/register/register.module";
import {IngredientsModule} from "./modules/ingredient/ingredients.module";
import {StickyModule} from "./libraries/common/utils/sticky/sticky.module";
import {DisableControlModule} from "./libraries/common/utils/disable-control/disable-control.module";
import {ProductsModule} from "./modules/products/products.module";

/**
 * Базовый модуль приложения
 */
@NgModule({
	declarations: [
        AppComponent,
        LoginComponent,
        WrapperPageComponent
	],
	imports: [
        CommonModule,
		BrowserModule,
		FormsModule,
		HttpClientModule,
		NgbModule.forRoot(),
		PagesModule,
        BrowserAnimationsModule,
        NoopAnimationsModule,
        PlanModule,
        IngredientsModule,
        ProductsModule,
        MenuModule,
        UnSnackBarModule,
        FormsModule,
        ReactiveFormsModule,
        SettingsModule,
        RegisterModule,
        UnChartsModule,
        StickyModule,
        DisableControlModule
	],
	providers: [
        AuthGuard,
        AuthService,
        UserService,
        UnSnackBarService,
		DatePipe,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AppHttpInterceptor,
            multi: true
        },
        {provide: MAT_DATE_LOCALE, useValue: 'ru-RU'},
        // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
        // `MatMomentDateModule` in your applications root module. We provide it at the component level
        // here, due to limitations of our example generation script.
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
	],
	bootstrap: [AppComponent]
})
export class AppModule {}
