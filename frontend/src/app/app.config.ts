﻿interface IAppConfig {
    apiUrl: string;
    token: string;
}

export const appConfig: IAppConfig = {
    apiUrl: 'http://5.23.48.252:4002',
    token: 'Bearer '
};
