import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/observable';
import {IUser} from '../libraries/user/user.interface';
import {appConfig} from "../app.config";
import {Router} from "@angular/router";
import {HttpHeaders} from "@angular/common/http/src/headers";

export interface IHttpHeadersIntercept {
    name: string,
    value: string
}

@Injectable()
export class AppHttpInterceptor implements HttpInterceptor {

    public constructor(
        private router: Router
    ) {

    }

    public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<Object>> {
        const currentUser: IUser = JSON.parse(localStorage.getItem('currentUser'));
        if (req && req.headers && req.headers.get('meta') && req.headers.get('meta') === 'outgoing') {
            const mapReq: HttpRequest<any> = req.clone({
                headers: req.headers.delete('meta')
            });
            return next.handle(mapReq);
        } else if (currentUser && currentUser.token) {
            const authReq: HttpRequest<any> = req.clone({
                headers: this.getHeadersIntercept().reduce((acc: HttpHeaders, header: IHttpHeadersIntercept) => acc.append(header.name, header.value), req.headers)
            });
            return next.handle(authReq);
        } else {
            return next.handle(req);
        }
    }

    public getHeadersIntercept(): IHttpHeadersIntercept[] {
        const currentUser: IUser = JSON.parse(localStorage.getItem('currentUser'));
        return [
            {
                name: 'Authorization',
                value: appConfig.token + currentUser.token
            },
            {
                name: 'userId',
                value: currentUser.id
            },
            {
                name: 'permissionCode',
                value: this.router.url.split('?')[0].split('/').pop()
            }
        ]
    }
}