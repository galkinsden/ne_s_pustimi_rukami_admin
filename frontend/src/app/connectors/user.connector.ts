import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {IUser} from '../libraries/user/user.interface';
import {ConnectorErrorModel, IError} from '../libraries/common/utils/connector-error.model';
import {appConfig} from '../app.config';
import {UserModel} from '../libraries/user/user.model';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {EPermissionsCode} from "../libraries/user/permissions/permissions-code.enum";

export interface IUserConnectorGetAllResponse {
    data: IUser[];
    error: IError;
}
export interface IUserConnectorGetByIdResponse {
    data: IUser;
    error: IError;
}
export interface IUserConnectorUpdateResponse {
    data: boolean;
    error: IError;
}
export interface IUserConnectorCreateResponse {
    data: boolean;
    error: IError;
}
export interface IUserConnectorCheckPermissionResponse {
    data: boolean;
    error: IError;
}
export interface IUserConnectorDeleteResponse {
    data: boolean;
    error: IError;
}

export class UserConnector {

    constructor(private http: HttpClient) {}

    public getAll(): Observable<IUser[] | ConnectorErrorModel> {
        return this.http.get(`${appConfig.apiUrl}/users/getAll`)
            .map((res: IUserConnectorGetAllResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public getById(id: string): Observable<IUser | ConnectorErrorModel> {
        return this.http.get(`${appConfig.apiUrl}/users/${id}`)
            .map((res: IUserConnectorGetByIdResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public create(user: UserModel): Observable<boolean | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/users/setCreate`, {user: user})
            .map((res: IUserConnectorCreateResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public update(user: UserModel): Observable<boolean | ConnectorErrorModel> {
        return this.http.put(`${appConfig.apiUrl}/users/${user.id}`, user)
            .map((res: IUserConnectorUpdateResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public delete(id: string): Observable<boolean | ConnectorErrorModel> {
        return this.http.delete(`${appConfig.apiUrl}/users/${id}`)
            .map((res: IUserConnectorDeleteResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    private handleError(res: IError): Observable<ConnectorErrorModel> {
        return Observable.throw(new ConnectorErrorModel(res));
    }
}