import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {ConnectorErrorModel, IError} from '../libraries/common/utils/connector-error.model';
import {appConfig} from '../app.config';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

export interface IChartsConnectorGetPlanByGroupResponse {
    data: any;
    error: IError;
}

export class ChartsConnector {

    constructor(private http: HttpClient) {}

    public getPlanByGroup(userId: string): Observable<any | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/charts`, {userId: userId})
            .map((res: IChartsConnectorGetPlanByGroupResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    private handleError(res: IError): Observable<ConnectorErrorModel> {
        return Observable.throw(new ConnectorErrorModel(res));
    }
}