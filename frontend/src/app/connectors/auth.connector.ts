import {HttpClient, HttpErrorResponse, HttpEvent} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {IUser} from '../libraries/user/user.interface';
import {ConnectorErrorModel, IError} from '../libraries/common/utils/connector-error.model';
import {appConfig} from '../app.config';
import {EStatusCode} from '../libraries/common/utils/status-code.enum';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

export interface IAuthConnectorLogin {
    data: IUser;
    error: IError;
}

export class AuthConnector {

    constructor(private http: HttpClient) {}

    public login(username: string, password: string): Observable<IUser | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/users/getAuth`, { username: username, password: password })
            .map((res: IAuthConnectorLogin) => res.data)
            .catch((res: HttpErrorResponse) => this.parseError(res.error && res.error.error ? res.error.error : res.error));
    }


    private parseError(res: IError): Observable<ConnectorErrorModel> {
        if (res.status === EStatusCode.UNAUTHORIZED) {
            // 401 unauthorized response so log user out of client
            window.location.href = '/login';
        }
        return Observable.throw(new ConnectorErrorModel(res));
    }
}
