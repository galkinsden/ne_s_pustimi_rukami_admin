﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../libraries/auth/auth.service';
import {StateModel} from '../../libraries/common/utils/state/state.model';
import {EStateModel} from '../../libraries/common/utils/state/state-model.enum';
import {UserModel} from '../../libraries/user/user.model';
import {ConnectorErrorModel} from '../../libraries/common/utils/connector-error.model';
import {UnSnackBarService} from '../../libraries/common/ui/snackbar/un-snack-bar.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
    moduleId: module.id,
    templateUrl: 'login.component.html',
    styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
    public state: StateModel = new StateModel(EStateModel.NONE);
    public returnUrl: string;
    public userForm: FormGroup;

    public static PATH: string = 'login';

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authService: AuthService,
        private unSnackBar: UnSnackBarService
    ) {

    }

    public ngOnInit(): void {
        this.userForm = new FormGroup({
            name: new FormControl('', Validators.required),
            password: new FormControl('', Validators.required),
        });
        // reset login status
        this.authService.logout();

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    public login(): void {
        if (this.userForm.valid) {
            this.state = new StateModel(EStateModel.LOADING);
            this.authService.login(this.userForm.value.name, this.userForm.value.password)
                .subscribe(
                    (userModel: UserModel) => {
                        this.state = new StateModel(EStateModel.LOADED);
                        this.unSnackBar.openSucces(`Здравствуйте, ${userModel.username}. Добро пожаловать в платформу Union.`);
                        this.router.navigate([this.returnUrl]);
                    },
                    (error: ConnectorErrorModel) => {
                        this.state = new StateModel(EStateModel.ERROR);
                        this.unSnackBar.openError(error && error.description ? error.description : 'Ошибка авторизации');
                    });
        }
    }
}
