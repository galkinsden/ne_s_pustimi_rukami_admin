import {PageRoutingModule} from './page.routing.module';
import {NgModule} from '@angular/core';
import {ErrorComponent} from './error/error.component';
import {ClockModule} from '../libraries/common/ui/clock/clock.module';
import {EmptyAreaModule} from '../libraries/common/ui/empty-area/empty-area.module';
import {UnSnackBarModule} from 'app/libraries/common/ui/snackbar/un-snack-bar.module';
import {PreloaderModule} from "../libraries/common/ui/preloader/preloader.module";
import {MatButtonModule, MatCheckboxModule, MatInputModule, MatSnackBarModule} from "@angular/material";

@NgModule({
	imports: [
        PageRoutingModule,
		ClockModule,
		EmptyAreaModule,
        PreloaderModule,
        UnSnackBarModule,
	],
	declarations: [
        ErrorComponent,
	],
	exports: [
		PageRoutingModule,
        ErrorComponent,
        PreloaderModule,
        UnSnackBarModule,

        // material
        MatButtonModule,
        MatCheckboxModule,
        MatInputModule,
        MatSnackBarModule
        // material
	]
})
export class PagesModule {}
