import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ErrorComponent} from './error/error.component';
import {AuthGuard} from './guard.service';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from '../modules/register/register.component';
import {WrapperPageComponent} from "./wrapper-page/wrapper-page.component";
import {PlanComponent} from "../modules/plan/plan.component";
import {SettingsComponent} from "../modules/settings/settings.component";
import {ChartsComponent} from "../modules/charts/charts.component";
import {IngredientsComponent} from "../modules/ingredient/ingredients.component";
import {ProductsComponent} from "../modules/products/products.component";

const routes: Routes = [
    {
        path: '',
        canActivate: [AuthGuard],
        component: WrapperPageComponent,
        children: [
            { path: PlanComponent.PATH, component: PlanComponent },
            { path: IngredientsComponent.PATH, component: IngredientsComponent },
            { path: ProductsComponent.PATH, component: ProductsComponent },
     /*       { path: SettingsComponent.PATH, component: SettingsComponent },
            { path: ChartsComponent.PATH, component: ChartsComponent },
            { path: RegisterComponent.PATH, component: RegisterComponent },*/
            { path: ErrorComponent.PATH, component: ErrorComponent },
            {path: '', redirectTo: IngredientsComponent.PATH, pathMatch: 'full'}
        ]
    },
    { path: LoginComponent.PATH, component: LoginComponent },
    {
        path: '**',
        redirectTo: `/${PlanComponent.PATH}`,
        pathMatch: 'full'
    }
];

/**
 * Роутинг приложения
 */
@NgModule({
		imports: [RouterModule.forRoot(routes)],
		providers: [AuthGuard],
		exports: [RouterModule]
	})
export class PageRoutingModule {}
