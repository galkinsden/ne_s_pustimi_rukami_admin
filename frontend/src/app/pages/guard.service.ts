import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Observable} from "rxjs/Observable";
import {AuthService} from "../libraries/auth/auth.service";
import 'rxjs/add/operator/take';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router, private authService: AuthService) {}

    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        // Если уже авторизован
        if (localStorage.getItem('currentUser')) {
            this.authService.loginFromLocalStorage();
            return true;
        }

        // Если не авторизован, то редиректим на страницу login
        this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});
        return false;
    }

}