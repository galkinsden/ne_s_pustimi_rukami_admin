import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {IMenu} from "../../modules/menu/menu-item/menu-item.interface";
import {LoginComponent} from "../login/login.component";
import {PlanComponent} from "../../modules/plan/plan.component";
import {SettingsComponent} from "../../modules/settings/settings.component";
import {Observable} from "rxjs/Observable";
import {AuthService} from "../../libraries/auth/auth.service";
import {ChartsComponent} from "../../modules/charts/charts.component";
import {RegisterComponent} from "../../modules/register/register.component";
import {IngredientsComponent} from "../../modules/ingredient/ingredients.component";
import {ProductsComponent} from "../../modules/products/products.component";

@Component({
    selector: 'wrapper-page',
    templateUrl: './wrapper-page.component.html',
    styleUrls: ['./wrapper-page.component.scss']
})
export class WrapperPageComponent implements OnInit {

    public isLoggedIn$: Observable<boolean>;

    @Input() public menu: IMenu[] = [
        {
            name: 'План приема', url: PlanComponent.PATH, icon: 'list'
        },
        {
            name: 'Товары', url: ProductsComponent.PATH, icon: 'card_giftcard'
        },
        {
            name: 'Ингредиенты', url: IngredientsComponent.PATH, icon: 'apps'
        },
        /*{
            name: 'Статистика', url: ChartsComponent.PATH, icon: 'assessment'
        },
        {
            name: 'Настройки', url: SettingsComponent.PATH, icon: 'settings'
        },
        {
            name: 'Регистрация', url: RegisterComponent.PATH, icon: 'person_add'
        },*/
        {
            name: 'Выйти', url: LoginComponent.PATH, icon: 'power_settings_new'
        }
    ];

    public constructor(private authService: AuthService) {

    }

    public ngOnInit(): void {
        this.isLoggedIn$ = this.authService.isLoggedIn;
    }
}
