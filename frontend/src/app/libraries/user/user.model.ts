import {IUser} from './user.interface';
import {UserPermissionsModel} from "./permissions/user-permissions.model";

export class UserModel implements IUser {

    public readonly id: string;
    public readonly username: string;
    public readonly password: string;
    public readonly firstName: string;
    public readonly lastName: string;
    public readonly token?: string;
    public readonly permissions: UserPermissionsModel;

    public constructor(src: IUser) {
        this.id = src && src.id || '';
        this.username = src && src.username || '';
        this.password = src && src.password || '';
        this.firstName = src && src.firstName || '';
        this.lastName = src && src.lastName || '';
        this.token = src && src.token || '';
        this.permissions = src && src.permissions ? new UserPermissionsModel(src.permissions) : new UserPermissionsModel(null);
    }
}
