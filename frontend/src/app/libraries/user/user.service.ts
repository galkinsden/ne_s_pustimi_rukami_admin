﻿import {Observable} from 'rxjs/Observable';
﻿﻿import { Injectable } from '@angular/core';
import { IUser } from './user.interface';
import {UserConnector} from '../../connectors/user.connector';
import {UserModel} from './user.model';
import {HttpClient} from '@angular/common/http';
import {ConnectorErrorModel} from '../common/utils/connector-error.model';
import {EPermissionsCode} from "./permissions/permissions-code.enum";


@Injectable()
export class UserService {

    private userConnector: UserConnector;

    constructor(http: HttpClient) {
        this.userConnector = new UserConnector(http);
    }

    public getAll(): Observable<UserModel[]> {
        return this.userConnector.getAll()
            .map((res: IUser[]) => res.map((r: IUser) => new UserModel(r)));
    }

    public getById(id: string): Observable<UserModel> {
        return this.userConnector.getById(id)
            .map((res: IUser) => new UserModel(res));
    }

    public create(user: UserModel): Observable<boolean | ConnectorErrorModel> {
        return this.userConnector.create(user);
    }

    public update(user: IUser): Observable<boolean | ConnectorErrorModel> {
        return this.userConnector.update(user);
    }

    public delete(id: string): Observable<boolean | ConnectorErrorModel> {
        return this.userConnector.delete(id);
    }
}