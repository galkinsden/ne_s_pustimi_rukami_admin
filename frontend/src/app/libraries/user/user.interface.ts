import {IUserPermissions} from './permissions/user-permissions.interface';

export interface IUser {
    id: string;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    permissions: IUserPermissions;
    token?: string;
}
