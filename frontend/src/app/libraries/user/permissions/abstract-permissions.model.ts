import {IAbstractPermissions} from "./abstract-permissions.interface";

export class AbstractPermissionsModel {

    public readonly read: boolean = false;
    public readonly write: boolean = false;

    public constructor(src: IAbstractPermissions) {
        this.read = src && src.read;
        this.write = src && src.write;
    }
}
