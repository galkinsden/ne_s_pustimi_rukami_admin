import {IPlanPermissions} from "./plan/plan-permissions.interface";

export interface IUserPermissions {
    plan: IPlanPermissions,
    register: IPlanPermissions,
    charts: IPlanPermissions,
    settings: IPlanPermissions,
    ingredients: IPlanPermissions,
    products: IPlanPermissions,
}