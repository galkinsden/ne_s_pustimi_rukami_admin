export interface IAbstractPermissions {
    read: boolean;
    write: boolean;
}