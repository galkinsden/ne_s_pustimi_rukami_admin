import {AbstractPermissionsModel} from "../abstract-permissions.model";
import {IPlanPermissions} from "./plan-permissions.interface";

export class PlanPermissionsModel extends AbstractPermissionsModel {

    public constructor(src: IPlanPermissions) {
        super(src);
    }

}