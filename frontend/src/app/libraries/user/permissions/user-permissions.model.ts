import {IUserPermissions} from './user-permissions.interface';
import {PlanPermissionsModel} from "./plan/plan-permissions.model";
import {AbstractPermissionsModel} from "./abstract-permissions.model";

export class UserPermissionsModel implements IUserPermissions {

    public readonly plan: AbstractPermissionsModel;
    public readonly charts: AbstractPermissionsModel;
    public readonly register: AbstractPermissionsModel;
    public readonly settings: AbstractPermissionsModel;
    public readonly ingredients: AbstractPermissionsModel;
    public readonly products: AbstractPermissionsModel;

    public constructor(src: IUserPermissions) {
        this.plan = src && src.plan ? new AbstractPermissionsModel(src.plan) : new AbstractPermissionsModel(null);
        this.charts = src && src.charts ? new AbstractPermissionsModel(src.charts) : new AbstractPermissionsModel(null);
        this.register = src && src.register ? new AbstractPermissionsModel(src.register) : new AbstractPermissionsModel(null);
        this.settings = src && src.settings ? new AbstractPermissionsModel(src.settings) : new AbstractPermissionsModel(null);
        this.ingredients = src && src.ingredients ? new AbstractPermissionsModel(src.ingredients) : new AbstractPermissionsModel(null);
        this.products = src && src.products ? new AbstractPermissionsModel(src.products) : new AbstractPermissionsModel(null);
    }
}
