export enum EPermissionsCode {
    plan = 'plan',
    settings = 'settings',
    register = 'register',
    charts = 'charts',
    ingredients = 'ingredients',
    products = 'products'
}