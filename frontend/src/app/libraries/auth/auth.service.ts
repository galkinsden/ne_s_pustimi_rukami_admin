﻿import {UserModel} from '../user/user.model';
﻿import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {IUser} from '../user/user.interface';
import {AuthConnector} from 'app/connectors/auth.connector';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class AuthService {

    private authConnector: AuthConnector;
    private loggedIn = new BehaviorSubject<boolean>(false);

    constructor(http: HttpClient) {
        this.authConnector = new AuthConnector(http);
    }

    get isLoggedIn() {
        return this.loggedIn.asObservable(); // {2}
    }

    public login(username: string, password: string): Observable<UserModel | any> {
        return this.authConnector.login(username, password)
            .map((res: IUser) => {
                // login successful if there's a jwt token in the response
                if (res && res.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(res));
                }
                this.loggedIn.next(true);
                return new UserModel(res);
            });
        // TODO catch
    }

    public loginFromLocalStorage(): void {
        const user: UserModel = this.getAuthUser();
        if (user) {
            this.loggedIn.next(true);
        }
    }

    public getAuthUser(): UserModel {
        try {
            return new UserModel(JSON.parse(localStorage.getItem('currentUser')));
        } catch(error) {
            return null;
        }
    }

    public logout(): void {
        // remove user from local storage to log user out
        this.loggedIn.next(false);
        localStorage.removeItem('currentUser');
    }
}
