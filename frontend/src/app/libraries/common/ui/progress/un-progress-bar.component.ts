import {Component, Input} from '@angular/core';
import {ProgressSpinnerMode} from '@angular/material';

const prefix: string = 'un-progress-bar';

export enum EModeType {
	ligth = 'light',
	dark = 'dark'
}

@Component({
	selector        : prefix,
	templateUrl     : './un-progress-bar.component.html',
	styleUrls       : ['./un-progress-bar.component.scss'],
})
export class UnProgressBarComponent {

    @Input() public mode: ProgressSpinnerMode = 'indeterminate';
   	@Input() public value: number = 0;
   	@Input() public color: EModeType = EModeType.ligth;

}
