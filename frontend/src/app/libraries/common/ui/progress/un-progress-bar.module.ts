import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UnProgressBarComponent} from "./un-progress-bar.component";
import {MatProgressBarModule} from "@angular/material";

@NgModule({
	declarations: [UnProgressBarComponent],
	exports     : [UnProgressBarComponent],
	imports     : [CommonModule, MatProgressBarModule],
})
export class UnProgressBarModule {
	public static forRoot(): ModuleWithProviders {
		return {ngModule: UnProgressBarModule};
	}
}
