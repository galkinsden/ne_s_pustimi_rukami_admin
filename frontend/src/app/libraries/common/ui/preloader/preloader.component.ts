import {Component, Input} from '@angular/core';
import {ProgressSpinnerMode} from '@angular/material';

const prefix: string = 'un-preloader';

export enum ModType {
	light = 'light',
	dark = 'dark'
}

@Component({
	selector        : prefix,
	templateUrl     : './preloader.component.html',
	styleUrls       : ['./preloader.component.scss'],
})
export class PreloaderComponent {

    @Input() public diameter: number = 50;
    @Input() public mode: ProgressSpinnerMode = 'indeterminate';
   	@Input() public strokeWidth: number = 5;
   	@Input() public value: number = 50;
    @Input() public color: ModType = ModType.light;

}
