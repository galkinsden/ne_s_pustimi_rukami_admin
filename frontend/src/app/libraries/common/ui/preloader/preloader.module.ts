import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PreloaderComponent} from './preloader.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

export {ModType, PreloaderComponent} from './preloader.component';

@NgModule({
	declarations: [PreloaderComponent],
	exports     : [PreloaderComponent],
	imports     : [CommonModule, MatProgressSpinnerModule],
})
export class PreloaderModule {
	public static forRoot(): ModuleWithProviders {
		return {ngModule: PreloaderModule};
	}
}
