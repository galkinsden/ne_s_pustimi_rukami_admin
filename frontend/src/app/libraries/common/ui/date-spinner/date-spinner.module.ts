import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
    MatButtonModule, MatDatepickerModule, MatFormFieldModule, MatIconModule,
    MatInputModule
} from "@angular/material";
import {DateSpinnerComponent} from "./date-spinner.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {BrowserModule} from "@angular/platform-browser";

@NgModule({
    declarations: [DateSpinnerComponent],
    exports     : [DateSpinnerComponent],
    imports     : [
        CommonModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        MatIconModule
    ],
})
export class DateSpinnerModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: DateSpinnerModule};
    }
}
