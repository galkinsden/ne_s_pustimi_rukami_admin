import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {AbstractValueAccessor, MakeProvider} from 'app/libraries/common/utils/abstract-value-accessor';
import {Moment} from 'moment';
import * as moment from 'moment';

export enum EScDateSpinnerMethod {
    ADD = <any>'add',
    SUBTRUCT = <any>'subtract'
}

@Component({
    selector: 'date-spinner',
    templateUrl: './date-spinner.component.html',
    styleUrls: ['./date-spinner.component.scss'],
    host: {
        '[class]': 'className',
    },
    providers   : [
        MakeProvider(DateSpinnerComponent)
    ]
})
export class DateSpinnerComponent extends AbstractValueAccessor implements OnChanges {

    @Output() public onChangeModel = new EventEmitter<Moment>();
    @Input() placeholder: string = '';
    public _selectedDate: Moment = null;
    public _minDate: Moment = null;
    public _maxDate: Moment = null;
    public btnPrevDisabled = false;
    public btnNextDisabled = false;
    public disabled: boolean = false;

    /**
     * Метод смещения даты
     * @method shiftDay
     */
    public shiftDay(dir: number): void {
        this._selectedDate = this.getUpdatedSelectedDate();
        this._selectedDate = this._selectedDate
            ? this._selectedDate[dir > 0 ? EScDateSpinnerMethod.ADD : EScDateSpinnerMethod.SUBTRUCT](1, 'days')
            : null;
        this.value = this._selectedDate.clone();
        this.update();
    }

    public ngOnChanges(changes: SimpleChanges) {

    }

    private update(): void {
        /*this._minDate = this.minDate
            ? this.minDate.clone().startOf('day')
            : null;
        this._maxDate = this.maxDate
            ? this.maxDate.clone().startOf('day')
            : null;
        this.btnPrevDisabled = this.minDate && this._selectedDate
            ? this._selectedDate.isSame(this._minDate)
            : false;
        this.btnNextDisabled = this.maxDate && this._selectedDate
            ? this._selectedDate.isSame(this._maxDate)
            : false;*/
    }

    private getUpdatedSelectedDate(): Moment {
        return this.value ? this.value.clone().startOf('day') : moment().startOf('day');
    }

    public get value(): any { return this._value; }
    public set value(value: any) {
        this._value = value;
        this.onChange(value);
        this._selectedDate = this.getUpdatedSelectedDate();
        this.update();
        this.onChangeModel.emit(value);
    }

    public writeValue(value: any) {
        this._value = value;
    }
}

