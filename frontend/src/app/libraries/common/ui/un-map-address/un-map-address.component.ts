import {Component, Inject, Input, OnDestroy, OnInit} from "@angular/core";
import {FormControl, FormGroup} from "@angular/forms";
import {EAbstractBuyerMode} from "../../../../modules/plan/abstract-buyer/enum/abstract-buyer-mode.enum";
import {Subscription} from "rxjs/Subscription";
import {Observable} from "rxjs/Observable";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Component({
    selector: 'un-map-address',
    templateUrl: './un-map-address.component.html',
    styleUrls: ['./un-map-address.component.scss']
})
export class UnMapAddressComponent implements OnInit, OnDestroy {

    @Input() public formControlAddress: FormControl = new FormControl();
    @Input() public formGroupParent: FormGroup;
    @Input() public mode: EAbstractBuyerMode = EAbstractBuyerMode.VIEW;
    public points: any[] = [];

    public subscriberAddressChanged: Subscription;

    public constructor(
        private http: HttpClient
    ) {

    }

    public ngOnInit(): void {
        this.subscriberAddressChanged = this.formControlAddress.valueChanges
            .debounceTime(300)
            .switchMap(val => this.getPoints(val))
            .subscribe((elem: any) => {
                this.savePoints(elem);
            });
    }

    /**
     * Получаем точки от Яндекса
     * @param elem
     */
    private savePoints(elem: any): void {
        this.points = elem.response.GeoObjectCollection.featureMember.map((point) => {
            return {
                lng: point.GeoObject.Point.pos.split(' ')[0],
                lat: point.GeoObject.Point.pos.split(' ')[1],
                balloonHeader: point.GeoObject.metaDataProperty.GeocoderMetaData.text,
                balloonBody: 'Выбрать>>'
            }
        })
    }

    /**
     * Запрос к Яндексу
     * @param {string} val
     * @returns {Observable<any>}
     */
    private getPoints(val: string): Observable<any> {
        return this.http.get('https://geocode-maps.yandex.ru/1.x/', {
            params: {
                format: 'json',
                geocode: val || ''
            },
            headers: new HttpHeaders().append('meta', 'outgoing')
        });
    }

    public ngOnDestroy(): void {
        this.subscriberAddressChanged && this.subscriberAddressChanged.unsubscribe();
    }

    public isModeCreate(): boolean {
        return this.mode === EAbstractBuyerMode.CREATE;
    }

    public isModeEdit(): boolean {
        return this.mode === EAbstractBuyerMode.EDIT;
    }

    public isModeView(): boolean {
        return this.mode === EAbstractBuyerMode.VIEW;
    }

}