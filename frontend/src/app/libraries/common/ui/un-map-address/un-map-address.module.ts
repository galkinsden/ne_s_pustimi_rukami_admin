import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {UnMapAddressComponent} from "./un-map-address.component";
import {YaCoreModule} from "angular2-yandex-maps";
import {MatFormFieldModule, MatInputModule} from "@angular/material";
import {DisableControlModule} from "../../utils/disable-control/disable-control.module";

@NgModule({
	declarations: [UnMapAddressComponent],
	exports: [UnMapAddressComponent],
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		MatInputModule,
		MatFormFieldModule,
		DisableControlModule,
		YaCoreModule.forRoot()
	],
	providers: [

	],
	entryComponents: [],
})
export class UnMapAddressModule {}
