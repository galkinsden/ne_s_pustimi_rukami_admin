import {ChangeDetectionStrategy, Component} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/distinctUntilChanged';
import * as moment from 'moment/moment';

const interval: number = 1000;

@Component({
    selector: 'clock',
    template: '<div [textContent]="clock$ | async"></div>',
    styleUrls: ['./clock.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})

/**
 * Часы
 * @class ClockComponent
 */
export class ClockComponent {
    public clock$: Observable<string>;

    constructor() {
        this.clock$ = Observable
            .interval(interval)
            .startWith(0)
            .map(() => moment().format('DD MMMM, HH:mm:ss'))
            .distinctUntilChanged();
    }
}
