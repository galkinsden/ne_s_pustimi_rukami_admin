import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Subscriber } from 'rxjs/Subscriber';
import {AuthService} from "../../../auth/auth.service";
import {appConfig} from "../../../../app.config";

@Injectable()
export class UrlHelperService {
    constructor(
        private http: HttpClient, /* Your favorite Http requester */
        private authService: AuthService
    ) {
    }

    get(url: string): Observable<string> {
        return new Observable((observer: Subscriber<string>) => {
            let objectUrl: string = null;

            this.http
                .get(url, {
                    headers: this.getHeaders(),
                    responseType: 'blob'
                })
                .subscribe(m => {
                    objectUrl = URL.createObjectURL(m);
                    observer.next(objectUrl);
                });

            return () => {
                if (objectUrl) {
                    URL.revokeObjectURL(objectUrl);
                    objectUrl = null;
                }
            };
        });
    }

    getHeaders(): HttpHeaders {
        let headers = new HttpHeaders();
        headers.set('Authorization', appConfig.token + this.authService.getAuthUser().token);
        return headers;
    }
}