import {NgModule, ModuleWithProviders} from '@angular/core';
import {UrlHelperService} from "./secure.service";
import {SecurePipe} from "./secure.pipe";

@NgModule({
    declarations: [SecurePipe],
    exports     : [SecurePipe],
    imports     : [],
    providers: [UrlHelperService]
})
export class SecureModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: SecureModule};
    }
}
