import {NgModule} from '@angular/core';
import {DropdownComponent} from "./dropdown.component";
import {CommonModule} from "@angular/common";
import {NgbDropdownModule} from "@ng-bootstrap/ng-bootstrap";
import {OutsideCloseDropdownComponent} from "./outside-dropdown/outside-close-dropdown.component";

/**
 * Роутинг приложения
 */
@NgModule({
        declarations: [DropdownComponent, OutsideCloseDropdownComponent],
		imports: [NgbDropdownModule, CommonModule],
		providers: [],
		exports: [DropdownComponent, OutsideCloseDropdownComponent]
	})
export class DropdownModule {}
