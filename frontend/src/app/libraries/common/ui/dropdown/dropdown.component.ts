import {Component, Input, OnInit} from '@angular/core';
import {NgbDropdownConfig} from "@ng-bootstrap/ng-bootstrap";
import {PlacementArray} from "@ng-bootstrap/ng-bootstrap/util/positioning";

/**
 * План приема
 */
@Component({
    selector: 'dropdown',
    templateUrl: './dropdown.component.html',
    styleUrls: ['./dropdown.component.scss'],
    providers: [NgbDropdownConfig]
})
export class DropdownComponent {

    @Input() placement: PlacementArray;

    @Input() autoClose: boolean | 'outside' | 'inside';


    public constructor(private config: NgbDropdownConfig) {
        this.config.autoClose = this.autoClose || true;
        this.config.placement = this.placement || "bottom";
    }
}
