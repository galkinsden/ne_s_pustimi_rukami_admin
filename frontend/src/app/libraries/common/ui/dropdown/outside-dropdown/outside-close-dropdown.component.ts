import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {NgbDropdownConfig} from "@ng-bootstrap/ng-bootstrap";
import {PlacementArray} from "@ng-bootstrap/ng-bootstrap/util/positioning";

/**
 * План приема
 */
@Component({
    selector: 'outside-close-dropdown',
    templateUrl: './outside-close-dropdown.component.html',
    styleUrls: ['./outside-close-dropdown.component.scss'],
    providers: [NgbDropdownConfig]
})
export class OutsideCloseDropdownComponent {

    @Input() placement: PlacementArray;
    @ViewChild('ngbDropdown') ngbDropdown;
    @Input() autoClose: boolean | 'outside' | 'inside' = 'outside';
    @Input() set open(value: boolean) {
        this.ngbDropdown.open();
    };
    @Input() set close(value: boolean) {
        this.ngbDropdown.close();
    };


    public constructor(private config: NgbDropdownConfig) {
        this.config.autoClose = this.autoClose || true;
        this.config.placement = this.placement || "bottom";
    }
}
