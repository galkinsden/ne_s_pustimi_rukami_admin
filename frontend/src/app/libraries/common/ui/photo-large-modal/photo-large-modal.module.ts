import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {PhotoLargeModalComponent} from './photo-large-modal.component';
import {MatButtonModule, MatIconModule} from "@angular/material";
import {SecureModule} from "../img-secure/secure.module";

@NgModule({
	declarations: [PhotoLargeModalComponent],
	exports: [PhotoLargeModalComponent],
	imports: [CommonModule, FormsModule, MatIconModule, SecureModule, MatButtonModule],
	providers: [],
	entryComponents: [],
})
export class PhotoLargeModalModule {}
