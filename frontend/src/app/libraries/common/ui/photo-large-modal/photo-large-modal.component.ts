import {Component, Inject} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

@Component({
    selector: 'photo-large-modal',
    templateUrl: './photo-large-modal.component.html',
    styleUrls: ['./photo-large-modal.component.scss']
})
export class PhotoLargeModalComponent {

    public constructor(
        public dialogRef: MatDialogRef<PhotoLargeModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {

    }

    public close(): void {
        this.dialogRef.close();
    }
}