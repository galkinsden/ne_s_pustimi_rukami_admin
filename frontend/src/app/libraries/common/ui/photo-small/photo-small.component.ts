import {Component, EventEmitter, Input, Output} from '@angular/core';
import {MatDialog} from "@angular/material";
import {PhotoLargeModalComponent} from "../photo-large-modal/photo-large-modal.component";

@Component({
    selector: 'photo-small',
    templateUrl: 'photo-small.component.html',
    styleUrls: ['./photo-small.component.scss']
})
export class PhotoSmallComponent {

    @Input() public src: string = '';
    @Input() public showDeleteButton: boolean = false;
    @Input() public openLarge: boolean = true;
    @Output() public deletePhoto: EventEmitter<string> = new EventEmitter();

    public constructor(
        private dialog: MatDialog
    ) { }

    public openPhotoDialog(path: string): void {
        if (this.openLarge) {
            let dialogRef = this.dialog.open(PhotoLargeModalComponent, {panelClass: 'un-panel-class', data: {path: path}});
        }
    }

    public deletePhotoEmitter(path: string): void {
        this.deletePhoto.emit(path);
    }
}


