import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {PhotoSmallComponent} from "./photo-small.component";
import {SecureModule} from "../img-secure/secure.module";
import {PhotoLargeModalComponent} from "../photo-large-modal/photo-large-modal.component";
import {PhotoLargeModalModule} from "../photo-large-modal/photo-large-modal.module";
import {MatButtonModule, MatIconModule, MatTooltipModule} from "@angular/material";

@NgModule({
    declarations: [PhotoSmallComponent],
    exports: [PhotoSmallComponent],
    imports: [CommonModule, FormsModule, SecureModule, PhotoLargeModalModule, MatIconModule, MatButtonModule, MatTooltipModule],
    providers: [],
    entryComponents: [PhotoLargeModalComponent],
})
export class PhotoSmallModule {}
