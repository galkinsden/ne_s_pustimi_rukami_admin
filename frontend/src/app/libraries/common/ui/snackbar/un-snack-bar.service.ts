import {Injectable} from '@angular/core';
import {MatSnackBar, MatSnackBarConfig} from '@angular/material';

const DURATION: number = 3000;

@Injectable()
export class UnSnackBarService {

    constructor(private matSnackBar: MatSnackBar) {}

    public openSucces(message: string, action?: string, config?: MatSnackBarConfig): void {
        const defaultConfig: MatSnackBarConfig = {
            duration: DURATION,
            panelClass: 'snackbar-success',
            verticalPosition: 'top'
        };
        this.matSnackBar.open(message, action, {...defaultConfig, ...config});
    }

    public openError(message: string, action?: string, config?: MatSnackBarConfig): void {
        const defaultConfig: MatSnackBarConfig = {
            duration: DURATION,
            panelClass: 'snackbar-error',
            verticalPosition: 'top'
        };
        this.matSnackBar.open(message, action, {...defaultConfig, ...config});
    }
}
