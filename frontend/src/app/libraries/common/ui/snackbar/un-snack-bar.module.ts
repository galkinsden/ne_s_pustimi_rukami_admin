import {NgModule, ModuleWithProviders} from '@angular/core';
import {UnSnackBarService} from './un-snack-bar.service';

@NgModule({
    declarations: [],
    exports     : [],
    imports     : [],
    providers: [UnSnackBarService]
})
export class UnSnackBarModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: UnSnackBarModule};
    }
}
