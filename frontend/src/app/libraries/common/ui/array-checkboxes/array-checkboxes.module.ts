import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {ArrayCheckboxesComponent} from './array-checkboxes.component';
import {MatCheckboxModule} from "@angular/material";

@NgModule({
	declarations: [
		ArrayCheckboxesComponent,
	],
	exports: [
		ArrayCheckboxesComponent,
	],
	imports: [
		CommonModule,
		ReactiveFormsModule,
		MatCheckboxModule
	],
})
export class ArrayCheckboxesModule {
}
