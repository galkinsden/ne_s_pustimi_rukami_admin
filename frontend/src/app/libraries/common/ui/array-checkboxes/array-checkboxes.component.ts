import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Subscription} from 'rxjs/Subscription';
import {AbstractValueAccessor, MakeProvider} from "../../utils/abstract-value-accessor";

export interface IBaseCheckbox {
	label: string;
	key: string;
	checked: boolean;
}

export interface ICheckboxMeta<T = any> extends IBaseCheckbox {
	type: T;
}

@Component({
	selector: 'array-checkboxes',
	templateUrl: 'array-checkboxes.component.html',
	styleUrls: ['array-checkboxes.component.scss'],
	providers: [MakeProvider(ArrayCheckboxesComponent)]
})
export class ArrayCheckboxesComponent extends AbstractValueAccessor implements OnChanges {

	@Input() public checkboxes: ICheckboxMeta[];
    @Output() public change: EventEmitter<any> = new EventEmitter<any>();

	public formGroup: FormGroup;
	private formSub: Subscription;

	public writeValue(value: any): void {
		super.writeValue(value);
		if (Array.isArray(value)) {
			this.formGroup.setValue(toKeyObject<boolean>(this.checkboxes, (cb: ICheckboxMeta) => value.includes(cb.type)));
		} else {
			throw new Error(value + 'is not Array type');
		}

	}

	public ngOnChanges(changes: SimpleChanges): void {
		if (changes.checkboxes) {
			if (this.formSub instanceof Subscription) {
				this.formSub.unsubscribe();
			}
			this.formGroup = new FormGroup(toKeyObject<FormControl>(this.checkboxes, (cb: ICheckboxMeta) => new FormControl(null)));
			// todo: use map for ICheckboxes
			this.formSub = this.formGroup.valueChanges.subscribe((value: any) => {
                this.value = Object
                    .entries(value)
                    .filter(([key, v]: [string, any]) => v)
                    .map(([key, v]: [string, any]) => this.checkboxes.find((cb: ICheckboxMeta) => key === cb.key));
				this.change.emit();
			});
		}
	}
}


interface IKeyObject<T> {
	[k: string]: T;
}


/**
 * Конвертирует [{key: '1'}, {key: '2'}] => {1: f(..), 2: f(...)}
 * @param {IBaseCheckbox[]} array
 * @param {Function} f
 * @return {IKeyObject<T>}
 */
function toKeyObject<T>(array: IBaseCheckbox[], f: Function): IKeyObject<T> {
	return array.reduce(
		(group: object, cb: IBaseCheckbox) => ({
			...group,
			[cb.key]: f(cb),
		}),
		{},
	);
}
