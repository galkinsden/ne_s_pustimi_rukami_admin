import {ModuleWithProviders, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {ImageUploaderComponent} from "./image-uploader.component";
import {MatButtonModule, MatIconModule, MatTooltipModule} from "@angular/material";
import {FileUploadModule} from "ng2-file-upload";
import {ImageUploaderService} from "./image-uploader.service";
import {AppHttpInterceptor} from "../../../../connectors/app-http.interceptor";

/**
 *  Модуль компонента замещающего сообщения
 */
@NgModule({
	declarations: [ImageUploaderComponent],
	exports: [ImageUploaderComponent],
	imports: [CommonModule, MatIconModule, FileUploadModule, MatButtonModule, MatTooltipModule],
	providers: [ImageUploaderService, AppHttpInterceptor],
	entryComponents: [],
})

export class ImageUploaderModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: ImageUploaderModule};
    }
}
