import {Component, Input} from '@angular/core';
import {FileUploader} from "ng2-file-upload";

@Component({
	selector: 'un-image-uploader',
	templateUrl: 'image-uploader.component.html',
	styleUrls: ['./image-uploader.component.scss']
})
export class ImageUploaderComponent {

	@Input() public uploader: FileUploader;

	constructor() {}
}


