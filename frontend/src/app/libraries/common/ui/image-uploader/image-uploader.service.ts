import {
    AppHttpInterceptor,
    IHttpHeadersIntercept
} from "../../../../connectors/app-http.interceptor";
import {FileUploader} from "ng2-file-upload";
import {appConfig} from "../../../../app.config";
import {AuthService} from "../../../auth/auth.service";
import {Injectable} from "@angular/core";
import {FileUploaderOptions} from "ng2-file-upload/file-upload/file-uploader.class";

@Injectable()
export class ImageUploaderService {

    public constructor(
        private appHttpInterceptor: AppHttpInterceptor
    ) {

    }

    public getNewUploader(fileUploaderOptions: FileUploaderOptions): FileUploader {
        const uploader: FileUploader = new FileUploader({
            url: fileUploaderOptions.url,
            headers: this.appHttpInterceptor.getHeadersIntercept().concat(fileUploaderOptions.headers)
        });
        uploader.onAfterAddingFile = (file)=> { file.withCredentials = false; };
        return uploader;
    }

}