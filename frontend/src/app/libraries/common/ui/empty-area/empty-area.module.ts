import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {EmptyAreaComponent} from './empty-area.component';
import {MatIconModule} from "@angular/material";

/**
 *  Модуль компонента замещающего сообщения
 */
@NgModule({
	declarations: [EmptyAreaComponent],
	exports: [EmptyAreaComponent],
	imports: [CommonModule, FormsModule, MatIconModule],
	providers: [],
	entryComponents: [],
})
export class EmptyAreaModule {}
