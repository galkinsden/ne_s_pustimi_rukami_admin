import {Component, Input} from '@angular/core';

/**
 * Компонент Замещающее сообщение
 */
@Component({
	selector: 'empty-area',
	templateUrl: 'empty-area.component.html',
	styleUrls: ['./empty-area.component.scss']
})
export class EmptyAreaComponent {

	@Input() public textOnly = false; //Только текст без иконки
	@Input() public relative = false; //Относительно позиционирование

	constructor() { }
}


