export class InterpolateService {

	private static PLACEHOLDER_REGEX: RegExp = /{{\s*(\w+)\s*}}/g;
	private static CUT_SPEC_SYMBOLS_REGEX: RegExp = /{{\s*|\s*}}/g;

	/**
	 * Функция интерполяции строк
	 *
	 * @param template
	 * @param data
	 * @returns {string}
	 */
	public static interpolate(template: string = '', data: Object = {}): string {
		const matches: RegExpMatchArray = template.match(InterpolateService.PLACEHOLDER_REGEX);
		return matches
			? matches.reduce((acc: string, val: string) => {
				const regex = new RegExp(val, 'g');
				const key: string = val.replace(InterpolateService.CUT_SPEC_SYMBOLS_REGEX, ''); // Обрезаем {{ и }}
				return acc.replace(regex, data.hasOwnProperty(key) && data[key] ? data[key] : '');
			}, template)
			: template;
	}
}