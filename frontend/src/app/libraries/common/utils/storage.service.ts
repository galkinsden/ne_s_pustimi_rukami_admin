﻿//TODO переделать
export class StorageService {

	public getItem<T>(key: string): Promise<T> {
		const obj: string = localStorage.getItem(key);
		return new Promise<T>((resolve: Function, reject: Function) => {
			resolve(JSON.parse(obj));
		});
	}

	public setItem<T>(key: string, value: T): Promise<T> {
		localStorage.setItem(key, JSON.stringify(value));
		return new Promise<T>((resolve: Function, reject: Function) => {
			resolve(null);
		});
	}


}

