import { forwardRef, InjectionToken } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

export abstract class AbstractValueAccessor implements ControlValueAccessor {
    public _value: any = '';
    public get value(): any { return this._value; }
    public set value(value: any) {
        if (value !== this._value) {
            this._value = value;
            this.onChange(value);
        }
    }

    public writeValue(value: any): void {
        this._value = value;
        // warning: uncomment below if want to emit not only on user intervention
        // this.onChange(value);
    }

    public onChange = (_: any): void => { };
    public onTouched = (): void => { };
    public registerOnChange(fn: (_: any) => void): void { this.onChange = fn; }
    public registerOnTouched(fn: () => void): void { this.onTouched = fn; }
}

export function MakeProvider(type: any): {
    provide: InjectionToken<ControlValueAccessor>;
    useExisting: any;
    multi: boolean;
} {
    return {
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => type),
        multi: true
    };
}
