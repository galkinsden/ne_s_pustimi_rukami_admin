export enum EStateModel {
    NONE,
    LOADING,
    LOADED,
    ERROR,
    CLOSED,
    WAIT,
}
