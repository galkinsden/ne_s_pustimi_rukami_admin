import {Directive, ElementRef, AfterViewChecked, OnInit, Renderer2, OnDestroy} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';

/**
 * Контейнер для прилипающих элементов
 *
 * Директиву нужно повесить на элемент-контейнер, а
 * элементам необходимо добавить data аттрибут sticky-item=true
 */
@Directive({
    selector: '[kpiStickyContainer]',
    /* tslint:disable:use-host-property-decorator*/
    host: {
        '(scroll)': 'recalculateElements()',
        '(click)': 'recalculateElements($event)',
    }
    /* tslint:enable */
})
export class StickyDirective implements AfterViewChecked, OnInit, OnDestroy {
    private static readonly scrollTime: number = 300;
    private static readonly scrollInterval: number = 15;

    private stickyElements: HTMLElement[];
    private iframe: HTMLIFrameElement;
    private unsubscribeScroll: Subscription = null;

    constructor(private ref: ElementRef, private renderer: Renderer2) {
    }

    public ngOnInit(): void {
        this.ref.nativeElement.style.position = 'relative';
        this.updateStickyElements();
        this.iframe = this.initIframe();

        this.renderer.listen(this.iframe, 'load', () => {

            this.renderer.listen(
                this.iframe.contentWindow && this.iframe.contentWindow['content'] ? this.iframe.contentWindow['content'] : this.iframe.contentWindow,
                'resize',
                () => this.recalculateElements()
            );
        });

    }

    public ngOnDestroy(): void {
        this.renderer.removeChild(this.ref.nativeElement, this.iframe);
        if (this.unsubscribeScroll) {
            this.unsubscribeScroll.unsubscribe();
        }
    }

    public ngAfterViewChecked(): void {
        this.updateStickyElements();
        this.recalculateElements();
    }

    /**
     * Создает iframe для поимки resize события
     *
     * @returns {HTMLIFrameElement}
     */
    public initIframe(): HTMLIFrameElement {
        const iframe = this.renderer.createElement('iframe');
        const iframeStyles = {
            visibility: 'hidden',
            display: 'none',
            position: 'absolute',
            height: '100%',
            width: '100%',
            top: '0',
        };

        this.renderer.appendChild(this.ref.nativeElement, iframe);
        Object.entries(iframeStyles).forEach(([style, value]) => this.renderer.setStyle(iframe, style, value));
        return iframe;
    }

    /**
     * Обновляет список Прикрепленных элементов
     *
     * @returns {void}
     */
    public updateStickyElements(): void {
        this.stickyElements = Array.from(this.ref.nativeElement.querySelectorAll('[sticky-item=true]'));
    }

    /**
     * Считает сумму высот всех елементов в sticky списке находящихся ВЫШЕ целевого
     *
     * @param targetElement: HTMLElement
     * @returns {number}
     */
    private getHeightOfPreviousFixedElementsSum(targetElement: HTMLElement): number {
        return this.stickyElements
            .slice(0, this.stickyElements.indexOf(targetElement))
            .reduce((sum, stickyElement) => sum + stickyElement.offsetHeight, 0);
    }

    /**
     * Считает сумму высот всех елементов в sticky списке находящихся НИЖЕ целевого
     *
     * @param targetElement: HTMLElement
     * @returns {number}
     */
    private getHeightOfAfterFixedElementsSum(targetElement: HTMLElement): number {
        return this.stickyElements
            .slice(this.stickyElements.indexOf(targetElement) + 1)
            .reduce((sum, stickyElement) => sum + stickyElement.offsetHeight, 0);
    }

    /**
     * Определяет нужно ли прилеплять целевой элемент к ВЕРХУ контейнера
     *
     * @param container: HTMLElement,
     * @param stickyItem: HTMLElement
     * @returns {boolean}
     */
    private shouldStickToTop(container: HTMLElement, stickyItem: HTMLElement): boolean {
        return stickyItem.offsetTop - this.getHeightOfPreviousFixedElementsSum(stickyItem) < container.scrollTop;
    }

    /**
     * Определяет нужно ли прилеплять целевой элемент к НИЗУ контейнера
     *
     * @param container: HTMLElement,
     * @param stickyItem: HTMLElement
     * @returns {boolean}
     */
    private shouldStickToBottom(container: HTMLElement, stickyItem: HTMLElement): boolean {
        return stickyItem.offsetTop + stickyItem.offsetHeight + this.getHeightOfAfterFixedElementsSum(stickyItem) > container.scrollTop + container.offsetHeight;
    }

    /**
     * Чистит смещения всех элементов
     *
     * @returns {void}
     */
    public clearAll() {
        Array.from(this.ref.nativeElement.querySelectorAll('[sticky-item]') as HTMLElement[])
            .forEach(el => {
                el.style.transform = 'translateY(0px)';
                el.style.zIndex = 'inherit';
                el.style.position = 'relative';
            });
    }

    /**
     * Пересчитывает значения смещений элементов
     *
     * @returns {void}
     */
    // TODO: called 4 times on click (3 of which is ngAfterViewChecked)
    public recalculateElements(event = null): void {
        if (event) {
            const clickableItem = this.getParentClickableItem(event.target);
            if (clickableItem) {
                this.processGroupHeaderClick(clickableItem);
                return;
            }
        }

        const container = this.ref.nativeElement;
        if (!Array.isArray(this.stickyElements)) return;

       // this.clearAll();

        this.stickyElements.forEach(stickyItem => {
            let newTransformValue = 0;
            if (this.shouldStickToTop(container, stickyItem)) {
                newTransformValue = container.scrollTop - stickyItem.offsetTop + this.getHeightOfPreviousFixedElementsSum(stickyItem);
                stickyItem.style.zIndex = '5';

            } else if (this.shouldStickToBottom(container, stickyItem)) {
                newTransformValue = container.scrollTop + container.offsetHeight - stickyItem.offsetTop - stickyItem.offsetHeight - this.getHeightOfAfterFixedElementsSum(stickyItem);
                stickyItem.style.zIndex = '5';
            }
            stickyItem.style.transform = `translateY(${newTransformValue}px)`;

        });
    }

    public getParentClickableItem(stickyItem): void {
        if (stickyItem.hasAttribute('sticky-item-clickable')) {
            return stickyItem;
        }

        if (stickyItem.hasAttribute('kpiStickyContainer')) {
            return null;
        }

        if (!stickyItem.parentNode) {
            return null;
        }

        return this.getParentClickableItem(stickyItem.parentNode);
    }


    public processGroupHeaderClick(stickyItemHeader): void {
        if (this.unsubscribeScroll !== null) {
            this.unsubscribeScroll.unsubscribe();
        }

        const startPosition: number = this.ref.nativeElement.scrollTop;
        const stopPosition: number = stickyItemHeader.offsetTop - this.getHeightOfPreviousFixedElementsSum(stickyItemHeader);

        const scrollSpeed = (stopPosition - startPosition) / StickyDirective.scrollTime;

        let previousPosition = startPosition;

        this.unsubscribeScroll = Observable
            .interval(StickyDirective.scrollInterval)
            .subscribe(() => {
                const nextDelta = Math.floor(StickyDirective.scrollInterval * scrollSpeed);
                const nextPosition = this.ref.nativeElement.scrollTop + nextDelta;
                if ((nextDelta < 0 && nextPosition < stopPosition) ||
                    (nextDelta > 0 && nextPosition > stopPosition) ||
                    previousPosition === nextPosition // end of container already reached
                ) {
                    this.ref.nativeElement.scrollTop = stopPosition;
                    this.unsubscribeScroll.unsubscribe();
                    return;
                }

                this.ref.nativeElement.scrollTop = nextPosition;
                previousPosition = nextPosition;
            });
    }
}
