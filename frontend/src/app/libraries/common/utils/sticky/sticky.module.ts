import {NgModule, ModuleWithProviders} from '@angular/core';
import {StickyDirective} from "./sticky-container.directive";

@NgModule({
    declarations: [StickyDirective],
    exports     : [StickyDirective],
    imports     : [],
    providers: []
})
export class StickyModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: StickyModule};
    }
}
