import {NgModule, ModuleWithProviders} from '@angular/core';
import {DisableControlDirective} from "./disable-control.directive";

@NgModule({
    declarations: [DisableControlDirective],
    exports     : [DisableControlDirective],
    imports     : [],
    providers: []
})
export class DisableControlModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: DisableControlModule};
    }
}
