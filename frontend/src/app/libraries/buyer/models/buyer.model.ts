import {EStatus} from "../enum/status.enum";
import {IOrder, OrderModel} from "../order/models/order.model";
import {EDeliveryTypes} from "../enum/delivery-types.enum";

export interface IBuyer {
    id: string;
    name: string;
    comment: string;
    phone: string;
    email: string;
    address: string;
    dateOrder: string;
    dateDelivery: string[];
    status: EStatus;
    orders: IOrder[];
    deliveryType: EDeliveryTypes;
}

export class BuyerModel implements IBuyer {

    public readonly id: string;
    public readonly name: string;
    public readonly comment: string;
    public readonly phone: string;
    public readonly email: string;
    public readonly address: string;
    public readonly dateOrder: string;
    public readonly dateDelivery: string[]; //с по
    public readonly status: EStatus;
    public readonly orders: OrderModel[];
    public readonly deliveryType: EDeliveryTypes;

    public constructor(src: IBuyer) {
        this.id = src && src.id || '';
        this.name = src && src.name || '';
        this.comment = src && src.comment || '';
        this.phone = src && src.phone || '';
        this.email = src && src.email || '';
        this.address = src && src.address || '';
        this.dateOrder = src && src.dateOrder || '';
        this.dateDelivery = src && Array.isArray(src.dateDelivery) ? src.dateDelivery : [];
        this.status = src && src.status || null;
        this.deliveryType = src && src.deliveryType || null;
        this.orders = src && src.orders ? src.orders.map((order: IOrder) => new OrderModel(order)) : null;
    }
}
