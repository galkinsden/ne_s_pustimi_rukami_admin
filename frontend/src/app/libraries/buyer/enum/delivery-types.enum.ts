export enum EDeliveryTypes {
    PICKUP = 'PICKUP',
    DELIVERY = 'DELIVERY',
}