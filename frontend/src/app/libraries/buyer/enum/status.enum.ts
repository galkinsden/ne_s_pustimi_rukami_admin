export enum EStatus {
    NEWEST = 'NEWEST',
    WAIT = 'WAIT',
    READY = 'READY',
    SENT = 'SENT',
    PREORDER = 'PREORDER',
    FINISHED = 'FINISHED',
    CANCELED = 'CANCELED',
    OUTSIDE = 'OUTSIDE'
}