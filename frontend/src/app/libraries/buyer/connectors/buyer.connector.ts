import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {IUser} from '../../user/user.interface';
import {ConnectorErrorModel, IError} from '../../common/utils/connector-error.model';
import {appConfig} from '../../../app.config';
import {UserModel} from '../../user/user.model';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {IBuyer} from "../models/buyer.model";
import {IGetBuyersByDateResponse} from "./models/response/get-buyers-by-date-response.interface";
import {IGetBuyerByIdResponse} from "./models/response/get-buyer-by-id-response.interface";
import {ISetBuyerResponse} from "./models/response/set-buyer-response.interface";
import {ISetRemoveBuyerResponse} from "./models/response/set-remove-buyer-response.interface";
import {IGetBuyersResponse} from "./models/response/get-buyers-response.interface";
import {IUpdateBuyerResponse} from "./models/response/update-buyer-response.interface";
import {IGetBuyersByDateDeliveryResponse} from "./models/response/get-buyers-by-date-delivery-response.interface";
import {IGetBuyersByDateDeliveryOrStatusNew} from "./models/response/get-buyers-by-date-delivery-or-status-new-response.interface";

export class BuyerConnector {

    constructor(private http: HttpClient) {}

    public getBuyers(): Observable<IBuyer[] | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/buyer-api/getBuyers`, { })
            .map((res: IGetBuyersResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public getBuyersByDateOrder(dateOrder: string): Observable<IBuyer[] | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/buyer-api/getBuyersByDateOrder`, { dateOrder: dateOrder })
            .map((res: IGetBuyersByDateResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public getBuyersByDateDelivery(dateDelivery: string): Observable<IBuyer[] | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/buyer-api/getBuyersByDateDelivery`, { dateDelivery: dateDelivery })
            .map((res: IGetBuyersByDateDeliveryResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public getBuyersByDateDeliveryOrStatusNew(dateDelivery: string): Observable<IBuyer[] | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/buyer-api/getBuyersByDateDeliveryOrStatusNew`, { dateDelivery: dateDelivery })
            .map((res: IGetBuyersByDateDeliveryOrStatusNew) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public getBuyerById(id: string): Observable<IBuyer | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/buyer-api/getBuyerById`, { id: id })
            .map((res: IGetBuyerByIdResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public setBuyer(buyer: IBuyer): Observable<string | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/buyer-api/setBuyer`, buyer)
            .map((res: ISetBuyerResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public removeBuyer(id: string): Observable<boolean | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/buyer-api/setRemoveBuyer`, {id: id})
            .map((res: ISetRemoveBuyerResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public updateBuyer(buyer: IBuyer): Observable<string | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/buyer-api/setUpdateBuyer`, buyer)
            .map((res: IUpdateBuyerResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    private handleError(res: IError): Observable<ConnectorErrorModel> {
        return Observable.throw(new ConnectorErrorModel(res));
    }
}