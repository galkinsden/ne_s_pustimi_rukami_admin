import {IBuyer} from "../../../models/buyer.model";

export type IGetBuyersByStatusResponse = IBuyer[];