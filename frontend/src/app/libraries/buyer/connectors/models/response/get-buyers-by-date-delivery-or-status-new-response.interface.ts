import {IBuyer} from "../../../models/buyer.model";
import {IError} from "../../../../common/utils/connector-error.model";

export interface IGetBuyersByDateDeliveryOrStatusNew {
    data: IBuyer[];
    error: IError;
}