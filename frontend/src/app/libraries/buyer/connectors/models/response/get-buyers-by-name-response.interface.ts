import {IBuyer} from "../../../models/buyer.model";

export type IGetBuyersByNameResponse = IBuyer[];