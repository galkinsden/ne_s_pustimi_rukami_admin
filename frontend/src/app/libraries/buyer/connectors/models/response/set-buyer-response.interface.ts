import {IError} from "../../../../common/utils/connector-error.model";

export interface ISetBuyerResponse {
    data: string;
    error: IError;
}