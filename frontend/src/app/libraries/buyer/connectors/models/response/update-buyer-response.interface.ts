import {IError} from "../../../../common/utils/connector-error.model";

export interface IUpdateBuyerResponse {
    data: string;
    error: IError;
}