import {IBuyer} from "../../../models/buyer.model";
import {IError} from "../../../../common/utils/connector-error.model";

export interface  IGetBuyersResponse {
    data: IBuyer[];
    error: IError;
}