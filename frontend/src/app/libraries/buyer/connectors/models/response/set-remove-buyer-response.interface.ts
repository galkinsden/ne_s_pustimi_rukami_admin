import {IError} from "../../../../common/utils/connector-error.model";

export interface ISetRemoveBuyerResponse {
    data: boolean;
    error: IError
}