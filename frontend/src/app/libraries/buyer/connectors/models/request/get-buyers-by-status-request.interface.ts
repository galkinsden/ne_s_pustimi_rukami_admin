import {EStatus} from "../../../enum/status.enum";


export interface IGetBuyersByStatusRequest {
    status: EStatus
}