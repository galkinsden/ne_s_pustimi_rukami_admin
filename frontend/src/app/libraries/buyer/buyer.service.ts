import {Observable} from 'rxjs/Observable';
﻿﻿import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BuyerConnector} from "./connectors/buyer.connector";
import {BuyerModel, IBuyer} from "./models/buyer.model";
import {ConnectorErrorModel} from "../common/utils/connector-error.model";


@Injectable()
export class BuyerService {

    private buyerConnector: BuyerConnector;

    constructor(http: HttpClient) {
        this.buyerConnector = new BuyerConnector(http);
    }

    public getBuyers(): Observable<BuyerModel[] | ConnectorErrorModel> {
        return this.buyerConnector.getBuyers()
            .map((res: IBuyer[]) => res.map((r: IBuyer) => new BuyerModel(r)));
    }

    public getBuyersByDateOrder(date: string): Observable<BuyerModel[] | ConnectorErrorModel> {
        return this.buyerConnector.getBuyersByDateOrder(date)
            .map((res: IBuyer[]) => res.map((r: IBuyer) => new BuyerModel(r)));
    }

    public getBuyersByDateDelivery(date: string): Observable<BuyerModel[] | ConnectorErrorModel> {
        return this.buyerConnector.getBuyersByDateDelivery(date)
            .map((res: IBuyer[]) => res.map((r: IBuyer) => new BuyerModel(r)));
    }

    public getBuyersByDateDeliveryOrStatusNew(date: string): Observable<BuyerModel[] | ConnectorErrorModel> {
        return this.buyerConnector.getBuyersByDateDeliveryOrStatusNew(date)
            .map((res: IBuyer[]) => res.map((r: IBuyer) => new BuyerModel(r)));
    }

    public setBuyer(buyer: IBuyer): Observable<string | ConnectorErrorModel> {
        return this.buyerConnector.setBuyer(buyer);
    }

    public getBuyerById(id: string): Observable<BuyerModel | ConnectorErrorModel> {
        return this.buyerConnector.getBuyerById(id)
            .map((buyer: IBuyer) => new BuyerModel(buyer));
    }

    public updateBuyer(buyer: IBuyer): Observable<string | ConnectorErrorModel> {
        return this.buyerConnector.updateBuyer(buyer);
    }

    public removeBuyer(id: string): Observable<boolean | ConnectorErrorModel> {
        return this.buyerConnector.removeBuyer(id);
    }

}