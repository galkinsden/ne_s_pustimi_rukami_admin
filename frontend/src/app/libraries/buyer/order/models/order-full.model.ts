import {IProduct, ProductModel} from "../../../../modules/products/models/products.model";
import {
    IProductIngredientFull,
    ProductIngredientFullModel
} from "../../../../modules/products/models/product-ingredient-full.model";
import {IProductFull, ProductFullModel} from "../../../../modules/products/models/product-full-model.model";

export interface IOrderFull {
    product: IProductFull;
    count: number;
    salePercent: number;
    preOrder: boolean;
    additional: IProductIngredientFull[]
}

export class OrderFullModel implements IOrderFull {

    public readonly product: ProductFullModel;
    public readonly salePercent: number;
    public readonly count: number;
    public readonly preOrder: boolean;
    public readonly additional: ProductIngredientFullModel[];

    public constructor(src: IOrderFull) {
        this.product = src && src.product ? new ProductFullModel(src.product) : new ProductFullModel(null);
        this.salePercent = src && src.salePercent || null;
        this.count = src && src.count || null;
        this.preOrder = src && (typeof src.preOrder === 'boolean') ? src.preOrder : false;
        this.additional = src && Array.isArray(src.additional) ? src.additional.map((additional: IProductIngredientFull) => new ProductIngredientFullModel(additional)) : [];
    }

}