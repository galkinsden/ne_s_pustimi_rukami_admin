import {IProductIngredient, ProductIngredientModel} from "../../../../modules/products/models/product-ingredient.model";

export interface IOrder {
    count: number;
    id: string;
    salePercent: number;
    additional: IProductIngredient[],
    preOrder: boolean;
}

export class OrderModel implements IOrder {

    public readonly id: string;
    public readonly additional: ProductIngredientModel[];
    public readonly count: number;
    public readonly salePercent: number;
    public readonly preOrder: boolean;

    public constructor(src: IOrder) {
        this.id = src && src.id || '';
        this.count = src && src.count || null;
        this.salePercent = src && src.salePercent || null;
        this.preOrder = src && (typeof src.preOrder === 'boolean') ? src.preOrder : false;
        this.additional = src && src.additional ?  src.additional.map((additional: IProductIngredient) => new ProductIngredientModel(additional)) : null;
    }
}
