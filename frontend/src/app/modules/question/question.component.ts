import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {EAbstractProductMode} from "../products/abstract-product/enum/abstract-product-mode.enum";
import {Observable} from "rxjs/Observable";
import {EStateModel} from "../../libraries/common/utils/state/state-model.enum";
import {ConnectorErrorModel} from "../../libraries/common/utils/connector-error.model";
import {UnSnackBarService} from "../../libraries/common/ui/snackbar/un-snack-bar.service";
import * as uuidv1 from 'uuid/v1';
import {EGender} from "./enum/gender.enum";
import {AbstractQuestionWithAnswerModel} from "./models/abstract-question-with-answers.model";
import {QuestionService} from "./question.service";
import {QuestionCacheService} from "./cache/question-cache.service";
import {QuestionWithAnswerModel} from "./models/question-with-answers.model";
import {IQuestion, QuestionModel} from "./models/question.model";
import {AnswerModel, IAnswer} from "./models/answer.model";

@Component({
    selector: 'un-question',
    templateUrl: 'question.component.html',
    styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnChanges {

    @Input() public formQuestion: FormArray = new FormArray([]);
    @Input() public mode: EAbstractProductMode;
    @Input() public productId: string;

    public abstractQuestionWithAnswerModel$: Observable<AbstractQuestionWithAnswerModel>;

    public constructor(
        private questionService: QuestionService,
        private unSnackBarService: UnSnackBarService,
        private questionCacheService: QuestionCacheService
    ) {

    }

    public ngOnChanges(simpleChanges: SimpleChanges) {
        this.abstractQuestionWithAnswerModel$ = this.getQuestionsWithAnswers(this.productId)
            .startWith(new AbstractQuestionWithAnswerModel(null, EStateModel.LOADING))
            .do((abstractQuestionWithAnswerModel: AbstractQuestionWithAnswerModel) => {
                this.initQuestion(abstractQuestionWithAnswerModel && abstractQuestionWithAnswerModel.questionWithAnswersModel || []);
                this.questionCacheService.setCacheOld(abstractQuestionWithAnswerModel && abstractQuestionWithAnswerModel.questionWithAnswersModel || []);
            });
    }

    public isModeCreate(): boolean {
        return this.mode === EAbstractProductMode.CREATE;
    }

    public isModeEdit(): boolean {
        return this.mode === EAbstractProductMode.EDIT;
    }

    public isModeView(): boolean {
        return this.mode === EAbstractProductMode.VIEW;
    }

    public getQuestionsWithAnswers(prodId: string): Observable<AbstractQuestionWithAnswerModel> {
        //TODO разобраться
        this.isModeView() && this.resetQuestions();

        return this.questionService.getQuestionsWithAnswersByProductId(prodId)
            .map((questionWithAnswersModels: QuestionWithAnswerModel[]) => {
                return new AbstractQuestionWithAnswerModel(questionWithAnswersModels, EStateModel.LOADED)
            })
            .catch((error: ConnectorErrorModel) => {
                this.unSnackBarService.openError(error && error.description ? error.description : 'Ошибка получения вопросов и ответов к товару');
                return Observable.of(new AbstractQuestionWithAnswerModel([], EStateModel.ERROR));
            })
    }

    private createQuestionWithAnswers(questionWithAnswersModel: QuestionWithAnswerModel, productId?: string): FormGroup {
        if (questionWithAnswersModel && questionWithAnswersModel.answer) {
            const _questionId: string = uuidv1();
            return new FormGroup({
                question: this.questionService.createQuestionFromGroup(questionWithAnswersModel && questionWithAnswersModel.question || null, productId),
                _questionId: new FormControl(questionWithAnswersModel && questionWithAnswersModel.question && questionWithAnswersModel.question.id || _questionId),
                answer: this.questionService.createAnswerFormGroup(questionWithAnswersModel && questionWithAnswersModel.answer || null, questionWithAnswersModel && questionWithAnswersModel.question && questionWithAnswersModel.question.id || _questionId),
                _answerId: new FormControl(questionWithAnswersModel && questionWithAnswersModel.answer && questionWithAnswersModel.answer.id || uuidv1()),
            });
        } else {
            return new FormGroup({
                question: this.questionService.createQuestionFromGroup(questionWithAnswersModel && questionWithAnswersModel.question || null, productId),
                _questionId: new FormControl(questionWithAnswersModel && questionWithAnswersModel.question && questionWithAnswersModel.question.id || uuidv1()),
                _answerId: new FormControl('')
            });
        }

    }

    private initQuestion(questionsWithAnswers: QuestionWithAnswerModel[]): void {
        questionsWithAnswers.forEach((questionWithAnswersModel: QuestionWithAnswerModel) => {
            this.formQuestion.push(this.createQuestionWithAnswers(questionWithAnswersModel))
        });
    }

    public addNewQuestionWithAnswers(): void {
        const newQuestionWithAnswersForm: FormGroup = this.createQuestionWithAnswers(null, this.productId);
        this.formQuestion.push(newQuestionWithAnswersForm);
        if (newQuestionWithAnswersForm
            && newQuestionWithAnswersForm.get('_questionId')
            && newQuestionWithAnswersForm.get('_questionId').value
        ) {
            this.questionCacheService.addNewQuestion(newQuestionWithAnswersForm.get('_questionId').value);
        }
    }

    public deleteQuestionWithAnswersByIndex(index: number): void {
        const id: string = this.formQuestion.controls[index]
            && this.formQuestion.controls[index]
            && this.formQuestion.controls[index].get('_questionId')
            && this.formQuestion.controls[index].get('_questionId').value
            ? this.formQuestion.controls[index].get('_questionId').value
            : null;
        this.formQuestion.removeAt(index);
        if  (id) {
            this.questionCacheService.removeOneQuestion(id);
        }
    }

    public resetQuestions(): void {
        this.formQuestion = new FormArray([]);
    }
}