import {Injectable} from "@angular/core";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {ConnectorErrorModel} from "../../libraries/common/utils/connector-error.model";
import {QuestionConnector} from "./connectors/question.connector";
import {IQuestionWithAnswer, QuestionWithAnswerModel} from "./models/question-with-answers.model";
import {IQuestion} from "./models/question.model";
import {AnswerModel, IAnswer} from "./models/answer.model";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import * as uuidv1 from 'uuid/v1';

@Injectable()
export class QuestionService {

    private questionConnector: QuestionConnector;

    public constructor(http: HttpClient) {
        this.questionConnector = new QuestionConnector(http);
    }

    public getQuestionsWithAnswersByProductId(productId: string): Observable<QuestionWithAnswerModel[] | ConnectorErrorModel> {
        return this.questionConnector.getQuestionsWithAnswersByProductId(productId)
            .map((res: IQuestionWithAnswer[]) => res.map((q: IQuestionWithAnswer) => new QuestionWithAnswerModel(q)))
    }

    public setQuestionByProductId(question: IQuestion): Observable<string> {
        return this.questionConnector.setQuestionByProductId(question)
            .map((res: string) => res)
            .catch(() => Observable.of(''))
    }

    public removeQuestionById(id: string): Observable<boolean | ConnectorErrorModel> {
        return this.questionConnector.removeQuestionById(id);
    }

    public updateQuestionById(question: IQuestion): Observable<string | ConnectorErrorModel> {
        return this.questionConnector.updateQuestionById(question);
    }

    public setAnswerByQuestionId(answer: IAnswer): Observable<string> {
        return this.questionConnector.setAnswerByQuestionId(answer)
            .map((res: string) => res)
            .catch(() => Observable.of(''))
    }

    public removeAnswerById(id: string): Observable<boolean | ConnectorErrorModel> {
        return this.questionConnector.removeAnswerById(id);
    }

    public setUpdateAnswerById(answer: IAnswer): Observable<string | ConnectorErrorModel> {
        return this.questionConnector.updateAnswerById(answer);
    }

    public createAnswerFormGroup(answer: AnswerModel, questionId?: string): FormGroup {
        return new FormGroup({
            id: new FormControl(answer && answer.id || ''),
            questionId: new FormControl(questionId ? questionId : (answer && answer.questionId || '')),
            name: new FormControl(answer && answer.name || '', Validators.required),
            description: new FormControl(answer && answer.description || '', Validators.required)
        });
    }

    public createQuestionFromGroup(question: IQuestion, productId?: string): FormGroup {
        return new FormGroup({
            id: new FormControl(question && question.id || ''),
            productId: new FormControl(productId ? productId : (question && question.productId || ''), Validators.required),
            name: new FormControl(question && question.name || '', Validators.required),
            description: new FormControl(question && question.description || '', Validators.required)
        })
    }

}