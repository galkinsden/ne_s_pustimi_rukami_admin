import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatButtonModule, MatFormFieldModule, MatIconModule, MatInputModule, MatTooltipModule} from "@angular/material";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DisableControlModule} from "../../libraries/common/utils/disable-control/disable-control.module";
import {PreloaderModule} from "../../libraries/common/ui/preloader/preloader.module";
import {EmptyAreaModule} from "../../libraries/common/ui/empty-area/empty-area.module";
import {QuestionComponent} from "./question.component";
import {QuestionService} from "./question.service";
import {QuestionCacheService} from "./cache/question-cache.service";
import {QuestionItemComponent} from "./question-item/question-item.component";
import {AnswerItemComponent} from "./question-item/answer-item/answer-item.component";


@NgModule({
    declarations: [QuestionComponent, QuestionItemComponent, AnswerItemComponent],
    exports     : [QuestionComponent, QuestionItemComponent, AnswerItemComponent],
    imports     : [
        MatInputModule,
        MatFormFieldModule,
        FormsModule,
        ReactiveFormsModule,
        DisableControlModule,
        PreloaderModule,
        EmptyAreaModule,
        MatIconModule,
        MatButtonModule,
        CommonModule,
        MatTooltipModule
    ],
    providers   : [QuestionService, QuestionCacheService]
})

export class QuestionModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: QuestionModule};
    }
}
