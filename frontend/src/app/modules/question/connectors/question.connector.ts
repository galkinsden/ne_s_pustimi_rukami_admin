import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {ConnectorErrorModel, IError} from '../../../libraries/common/utils/connector-error.model';
import {appConfig} from '../../../app.config';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {IQuestionWithAnswer} from "../models/question-with-answers.model";
import {IQuestion} from "../models/question.model";
import {IAnswer} from "../models/answer.model";

export interface IQuestionConnectorGetQuestionsWithAnswersByProductIdResponse {
    data: IQuestionWithAnswer[];
    error: IError;
}

export interface IQuestionConnectorSetQuestionResponse {
    data: string;
    error: IError;
}

export interface IQuestionConnectorRemoveQuestionByIdResponse {
    data: boolean;
    error: IError;
}

export interface IQuestionConnectorUpdateQuestionByIdResponse {
    data: string;
    error: IError;
}

export interface IQuestionConnectorSetAnswerByQuestionIdResponse {
    data: string;
    error: IError;
}

export interface IQuestionConnectorRemoveAnswerByIdResponse {
    data: boolean;
    error: IError;
}

export interface IQuestionConnectorSetAnswerByIdResponse {
    data: string;
    error: IError;
}

export class QuestionConnector {

    constructor(private http: HttpClient) {}

    public getQuestionsWithAnswersByProductId(productId: string): Observable<IQuestionWithAnswer[] | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/question-api/getQuestionsWithAnswersByProductId`, { productId: productId })
            .map((res: IQuestionConnectorGetQuestionsWithAnswersByProductIdResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public setQuestionByProductId(question: IQuestion): Observable<string | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/question-api/setQuestionByProductId`, question)
            .map((res: IQuestionConnectorSetQuestionResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public removeQuestionById(id: string): Observable<boolean | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/question-api/setRemoveQuestionById`, {id: id})
            .map((res: IQuestionConnectorRemoveQuestionByIdResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public updateQuestionById(question: IQuestion): Observable<string | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/question-api/setUpdateQuestionById`, question)
            .map((res: IQuestionConnectorUpdateQuestionByIdResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public setAnswerByQuestionId(answer: IAnswer): Observable<string | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/answer-api/setAnswerByQuestionId`, answer)
            .map((res: IQuestionConnectorSetAnswerByQuestionIdResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public removeAnswerById(id: string): Observable<boolean | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/answer-api/setRemoveAnswerById`, {id: id})
            .map((res: IQuestionConnectorRemoveAnswerByIdResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public updateAnswerById(answer: IAnswer): Observable<string | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/answer-api/setUpdateAnswerById`, answer)
            .map((res: IQuestionConnectorSetAnswerByIdResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    private handleError(res: IError): Observable<ConnectorErrorModel> {
        return Observable.throw(new ConnectorErrorModel(res));
    }
}
