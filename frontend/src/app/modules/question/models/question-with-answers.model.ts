import {IQuestion, QuestionModel} from "./question.model";
import {AnswerModel, IAnswer} from "./answer.model";
import * as uuidv1 from 'uuid/v1';

export interface IQuestionWithAnswer {
    question: IQuestion;
    answer: IAnswer
    _questionId: string;
}

export class QuestionWithAnswerModel {

    public readonly question: QuestionModel;
    public readonly answer: AnswerModel;
    public readonly _questionId: string;

    public constructor(src: IQuestionWithAnswer) {
        this.question = src && src.question ? new QuestionModel(src.question) : null;
        this.answer = src && src.answer ? new AnswerModel(src.answer) : null;
        this._questionId = src && src.question && src.question.id ? src.question.id : uuidv1();
    }

}