import {EStateModel} from "../../../libraries/common/utils/state/state-model.enum";
import {StateModel} from "../../../libraries/common/utils/state/state.model";
import {QuestionWithAnswerModel} from "./question-with-answers.model";

export interface IAbstractQuestionWithAnswers {
    questionWithAnswersModel: QuestionWithAnswerModel[];
    state: EStateModel;
}

export class AbstractQuestionWithAnswerModel extends StateModel implements IAbstractQuestionWithAnswers {

    public constructor(
        public readonly questionWithAnswersModel: QuestionWithAnswerModel[] = [],
        public readonly state: EStateModel = EStateModel.NONE
    ) {
        super(state);
    }
}