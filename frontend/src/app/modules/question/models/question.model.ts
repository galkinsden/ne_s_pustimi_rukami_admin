export interface IQuestion {
    id?: string // id вопроса
    productId?: string; // id набора
    name: string; // Имя отправителя
    description: string; // Вопрос
}

export class QuestionModel {

    public readonly id: string;
    public readonly productId: string;
    public readonly name: string;
    public readonly description: string;

    public constructor(src: IQuestion) {
        this.id = src && src.id || '';
        this.productId = src && src.productId || '';
        this.name = src && src.name || '';
        this.description = src && src.description || '';
    }
}