export interface IAnswer {
    id?: string; // id ответа
    questionId?: string; // id вопроса
    name: string; // имя ответившего
    description: string; // описание
}

export class AnswerModel {

    public readonly id: string;
    public readonly questionId: string;
    public readonly name: string;
    public readonly description: string;

    public constructor(src: IAnswer) {
        this.id = src && src.id || '';
        this.questionId = src && src.questionId || '';
        this.name = src && src.name || '';
        this.description = src && src.description || '';
    }
}