import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {EAbstractProductMode} from "../../products/abstract-product/enum/abstract-product-mode.enum";
import {IAnswer} from "../models/answer.model";
import * as uuidv1 from 'uuid/v1';
import {QuestionCacheService} from "../cache/question-cache.service";
import {QuestionService} from "../question.service";

@Component({
    selector: 'un-question-item',
    templateUrl: 'question-item.component.html',
    styleUrls: ['./question-item.component.scss']
})
export class QuestionItemComponent implements OnInit {

    @Input() public formQuestionItem: FormGroup = new FormGroup({
        question: this.questionService.createQuestionFromGroup(null),
        _questionId: new FormControl(''),
        _answerId: new FormControl('')
    });
    @Input() public mode: EAbstractProductMode;
    @Input() public index: number;
    @Output() public deleteQuestion: EventEmitter<number> = new EventEmitter<number>();

    public constructor(
        private questionCacheService: QuestionCacheService,
        private questionService: QuestionService
    ) {

    }

    public ngOnInit(): void {

    }

    public isModeCreate(): boolean {
        return this.mode === EAbstractProductMode.CREATE;
    }

    public isModeEdit(): boolean {
        return this.mode === EAbstractProductMode.EDIT;
    }

    public isModeView(): boolean {
        return this.mode === EAbstractProductMode.VIEW;
    }

    public deleteQuestionByIndex(index: number): void {
        this.deleteQuestion.emit(index);
    }

    public addAnswer(): void {
        const newAnswer: FormGroup = this.questionService.createAnswerFormGroup(null);
        this.formQuestionItem.setControl('answer', newAnswer);
        this.formQuestionItem.get('_answerId').setValue(uuidv1());
        this.questionCacheService.addNewAnswer(this.formQuestionItem.get('_questionId').value, this.formQuestionItem.get('_answerId').value);
    }

    public deleteAnswer(): void {
        this.formQuestionItem.removeControl('answer');
        this.questionCacheService.removeOneAnswer(this.formQuestionItem.get('_questionId').value, this.formQuestionItem.get('_answerId').value);
    }
}

//var obj = {"id":"6af0ef80-0126-11e8-817d-a55607ce49db", "username":"admin", "firstName":"", "lastName":"", "token":"", "permissions":{"plan":{"read":true,"write":true}, "charts":{"read":true, "write":true}, "register":{"read":true, "write":true}, "settings":{"read":true, "write":true}, "ingredients":{"read":true, "write":true}, "products":{"read":true, "write":true}}, "hash":"$2a$10$91wB2d9oPfpBwgeaNFjSg.q4Pgm5tBrUDXi8W6fSYWpkMb4hgwbSO"}

//mongo ds249025.mlab.com:49025/nespusti -u nespusti -p Dtr2ex9aWX