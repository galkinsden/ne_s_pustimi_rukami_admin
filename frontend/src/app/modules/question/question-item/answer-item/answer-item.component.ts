import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {EAbstractProductMode} from "../../../products/abstract-product/enum/abstract-product-mode.enum";
import {QuestionCacheService} from "../../cache/question-cache.service";
import {QuestionService} from "../../question.service";

@Component({
    selector: 'un-answer-item',
    templateUrl: 'answer-item.component.html',
    styleUrls: ['./answer-item.component.scss']
})
export class AnswerItemComponent {

    @Input() public formAnswerItem: FormGroup = this.questionService.createAnswerFormGroup(null);
    @Input() public _questionId: string = '';
    @Input() public mode: EAbstractProductMode;
    @Input() public index: number;
    @Output() public deleteAnswer: EventEmitter<number> = new EventEmitter<number>();

    public constructor(
        private questionCacheService: QuestionCacheService,
        private questionService: QuestionService
    ) {

    }

    public isModeCreate(): boolean {
        return this.mode === EAbstractProductMode.CREATE;
    }

    public isModeEdit(): boolean {
        return this.mode === EAbstractProductMode.EDIT;
    }

    public isModeView(): boolean {
        return this.mode === EAbstractProductMode.VIEW;
    }

    public deleteAnswerByIndex(index: number): void {
        this.deleteAnswer.emit(index);
        //this.questionCacheService.removeOneAnswer(this.formAnswerItem.get('id').value, this._questionId);
    }
}