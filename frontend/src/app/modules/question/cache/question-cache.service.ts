import {Injectable} from "@angular/core";
import {QuestionService} from "../question.service";
import {IQuestionsCache, QuestionsCacheModel} from "./model/questions-cache.model";
import {AnswersCacheModel, IAnswersCache} from "./model/answers-cache.model";
import {QuestionWithAnswerModel} from "../models/question-with-answers.model";
import {IQuestion, QuestionModel} from "../models/question.model";
import {AnswerModel, IAnswer} from "../models/answer.model";
import 'rxjs/add/observable/combineLatest';
import {Observable} from "rxjs/Observable";
import {Subscription} from "rxjs/Subscription";

interface ICacheOldEnter {
    question: QuestionModel;
    _questionId: string;
    answer: AnswerModel;
    _answerId: string;
}

interface ISubmitDataQuestions {
    created: IQuestionsCache[]
    updated: IQuestionsCache[];
    removed: IQuestionsCache[];
}

interface ISubmitDataAnswers {
    created: IAnswersCache[]
    updated: IAnswersCache[];
    removed: IAnswersCache[];
}

interface ISubmitData {
    questions: ISubmitDataQuestions;
    answers: ISubmitDataAnswers;
}

@Injectable()
export class QuestionCacheService {

    private questions: QuestionsCacheModel = new QuestionsCacheModel();
    private answers: AnswersCacheModel = new AnswersCacheModel();

    public constructor(
        private questionService: QuestionService
    ) {

    }

    public setCacheOld(questionWithAnswerModels: QuestionWithAnswerModel[]): void {
        this.questions.setCacheOld(questionWithAnswerModels);
        this.answers.setCacheOld(questionWithAnswerModels);
    }

    public addNewQuestion(_questionId: string): void {
        this.questions.created.setItem(_questionId);
    }

    public addNewAnswer(_questionId: string, _answerId: string): void {
        this.answers.created.setItem({
            _questionId: _questionId,
            _answerId: _answerId
        });
    }

    public removeOneQuestion(id: string): void {
        if (!!~this.questions.created.getIndexFromItems(id)) {
            this.questions.created.removeItem(id);
            this.answers.created.removeItemsByQuestionId(id);
        } else if (!!~this.questions.updated.getIndexFromItems(id)) {
            this.questions.updated.removeItem(id);
            this.questions.removed.setItem(id);
        } else if (!!~this.questions.updated.getIndexFromItems(id)) {

        } else {
            this.questions.removed.setItem(id);
            this.answers.removed.removeItemsByQuestionId(id);
            this.answers.created.removeItemsByQuestionId(id);
            this.answers.updated.removeItemsByQuestionId(id);
        }
    }

    public removeOneAnswer(_questionId: string, _answerId: string): void {
        if (!!~this.answers.created.getIndexFromItems(_answerId)) {
            this.answers.created.removeItem(_answerId);
        } else if (!!~this.answers.updated.getIndexFromItems(_answerId)) {
            this.answers.updated.removeItem(_answerId);
            this.answers.removed.setItem({
                _questionId: _questionId,
                _answerId: _answerId
            });
        } else {
            this.answers.removed.setItem({
                _questionId: _questionId,
                _answerId: _answerId
            });
        }
    }

    public getSubmitData(cacheOldEnters: ICacheOldEnter[]): ISubmitData {
        return {
            questions: this.getSubmitDataQuestion(cacheOldEnters.map((cacheOldEnter: ICacheOldEnter) => {
                return {
                    question: cacheOldEnter.question,
                    _questionId: cacheOldEnter._questionId,
                }
            })),
            answers: this.getSubmitDataAnswers(cacheOldEnters.map((cacheOldEnter: ICacheOldEnter) => {
                return {
                    answer: cacheOldEnter.answer,
                    _answerId: cacheOldEnter._answerId,
                    _questionId: cacheOldEnter._questionId
                }
            }))
        }
    }

    private getSubmitDataQuestion(questionsCache: IQuestionsCache[]): ISubmitDataQuestions {

        const createdElements: IQuestionsCache[] = questionsCache.filter((q: IQuestionsCache) => {
            return !!~this.questions.created.getIndexFromItems(q._questionId)
        });

        const removedElements: IQuestionsCache[] = this.questions.getCacheOld().filter((q: IQuestionsCache) => {
            return !!~this.questions.removed.getIndexFromItems(q._questionId)
        });

        const noRemovedAndCreatedCacheOld: IQuestionsCache[] = this.questions.getCacheOld().filter((q: IQuestionsCache) => {
            return !~this.questions.created.getIndexFromItems(q._questionId) && !~this.questions.removed.getIndexFromItems(q._questionId)
        });

        const noRemovedAndCreatedNewFormValue: IQuestionsCache[] = questionsCache.filter((q: IQuestionsCache) => {
            return !~this.questions.created.getIndexFromItems(q._questionId) && !~this.questions.removed.getIndexFromItems(q._questionId)
        });

        const updatedElements: IQuestionsCache[] = noRemovedAndCreatedNewFormValue.filter((q: IQuestionsCache) => {
            const index: number = noRemovedAndCreatedCacheOld.map((r: IQuestionsCache) => r._questionId).indexOf(q._questionId);
            if (!!~index) {
                const elem: IQuestionsCache = noRemovedAndCreatedCacheOld[index];
                return !!(elem && elem.question && (elem.question.id !== q.question.id
                    || elem.question.name !== q.question.name
                    || elem.question.description !== q.question.description))
            } else {
                return false;
            }
        });

        return {
            removed: removedElements,
            created: createdElements,
            updated: updatedElements,
        }
    }

    private getSubmitDataAnswers(answersCache: IAnswersCache[]): ISubmitDataAnswers {

        const createdElements: IAnswersCache[] = answersCache.filter((q: IAnswersCache) => {
            return !!~this.answers.created.getIndexFromItems(q._answerId)
        });

        const removedElements: IAnswersCache[] = this.answers.getCacheOld().filter((q: IAnswersCache) => {
            return !!~this.answers.removed.getIndexFromItems(q._answerId)
        });

        const noRemovedAndCreatedCacheOld: IAnswersCache[] = this.answers.getCacheOld().filter((q: IAnswersCache) => {
            return !~this.answers.created.getIndexFromItems(q._answerId) && !~this.answers.removed.getIndexFromItems(q._answerId)
        });

        const noRemovedAndCreatedNewFormValue: IAnswersCache[] = answersCache.filter((q: IAnswersCache) => {
            return !~this.answers.created.getIndexFromItems(q._answerId) && !~this.answers.removed.getIndexFromItems(q._answerId)
        });

        const updatedElements: IAnswersCache[] = noRemovedAndCreatedNewFormValue.filter((q: IAnswersCache) => {
            const index: number = noRemovedAndCreatedCacheOld.map((r: IAnswersCache) => r._answerId).indexOf(q._answerId);
            if (!!~index) {
                const elem: IAnswersCache = noRemovedAndCreatedCacheOld[index];
                return !!(elem && elem.answer && (elem.answer.id !== q.answer.id
                    || elem.answer.name !== q.answer.name
                    || elem.answer.description !== q.answer.description))
            } else {
                return false;
            }
        });

        return {
            removed: removedElements,
            created: createdElements,
            updated: updatedElements,
        }
    }

    public changeQuestionsWithAnswers(cacheOldEnters: ICacheOldEnter[], productId: string): void {
        const submitData: ISubmitData = this.getSubmitData(cacheOldEnters);

        if (submitData && submitData.questions && submitData.questions.created && submitData.questions.created.length) {
            this.setCreatedQuestions(submitData.questions.created, submitData.answers.created, productId);
        }

        if (submitData && submitData.questions && submitData.questions.removed && submitData.questions.removed.length) {
            this.removeQuestions(submitData.questions.removed);
        }

        if (submitData && submitData.questions && submitData.questions.updated && submitData.questions.updated.length) {
            this.updateQuestions(submitData.questions.updated, productId);
        }

        if (submitData && submitData.answers && submitData.answers.created && submitData.answers.created.length) {
            this.setCreatedAnswers(submitData.answers.created);
        }

        if (submitData && submitData.answers && submitData.answers.updated && submitData.answers.updated.length) {
            this.updateAnswers(submitData.answers.updated);
        }

        if (submitData && submitData.answers && submitData.answers.removed && submitData.answers.removed.length) {
            this.removeUnswers(submitData.answers.removed);
        }
    }

    private setCreatedQuestions(createdQuestions: IQuestionsCache[], createdAnswers: IAnswersCache[], productId: string): void {
        const sub: Subscription = Observable
            .combineLatest(createdQuestions.map((q: IQuestionsCache) => this.questionService.setQuestionByProductId({...q.question, ...{productId: productId}})))
            .switchMap((ids: string[]) => {
                return Observable
                    .combineLatest(
                        ids.map((id: string, index: number) => {
                            const findedCreatedElem: IAnswersCache = createdAnswers
                                .find((answersCache: IAnswersCache) => createdQuestions && createdQuestions[index] && answersCache._questionId === createdQuestions[index]._questionId);
                            if (findedCreatedElem) {
                                const elemWithQuestionId: IAnswer = {...findedCreatedElem.answer, ...{questionId: id}};
                                return this.questionService.setAnswerByQuestionId(elemWithQuestionId);
                            } else {
                                return Observable.of('');
                            }
                        })
                    )
            })
            .finally(() => sub.unsubscribe())
            .subscribe()
    }

    private removeQuestions(questions: IQuestionsCache[]): void {
        const sub: Subscription = Observable
            .combineLatest(questions.map((questionsCache: IQuestionsCache) => this.questionService.removeQuestionById(questionsCache.question && questionsCache.question.id)))
            .map(() => true)
            .catch(() => Observable.of(false))
            .finally(() => sub.unsubscribe())
            .subscribe()
    }

    private updateQuestions(questions: IQuestionsCache[], productId: string): void {
        const sub: Subscription = Observable
            .combineLatest(questions.map((questionsCache: IQuestionsCache) => this.questionService.updateQuestionById({...questionsCache.question, ...{productId: productId}})))
            .map(() => true)
            .catch(() => Observable.of(false))
            .finally(() => sub.unsubscribe())
            .subscribe()
    }

    private updateAnswers(unswers: IAnswersCache[]): void {
        const sub: Subscription = Observable
            .combineLatest(unswers.map((answersCache: IAnswersCache) => this.questionService.setUpdateAnswerById(answersCache.answer)))
            .map(() => true)
            .catch(() => Observable.of(false))
            .finally(() => sub.unsubscribe())
            .subscribe()
    }

    private setCreatedAnswers(unswers: IAnswersCache[]): void {
        const sub: Subscription = Observable
            .combineLatest(unswers.map((answersCache: IAnswersCache) => this.questionService.setAnswerByQuestionId({...answersCache.answer,...{questionId: answersCache._questionId}})))
            .map(() => true)
            .catch(() => Observable.of(false))
            .finally(() => sub.unsubscribe())
            .subscribe()
    }

    private removeUnswers(unswers: IAnswersCache[]): void {
        const sub: Subscription = Observable
            .combineLatest(unswers.map((answersCache: IAnswersCache) => this.questionService.removeAnswerById(answersCache.answer && answersCache.answer.id)))
            .map(() => true)
            .catch(() => Observable.of(false))
            .finally(() => sub.unsubscribe())
            .subscribe()
    }

}