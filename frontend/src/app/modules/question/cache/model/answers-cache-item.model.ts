export interface IAnswersCacheItem {
    _questionId: string;
    _answerId: string;
}

export class AnswersCacheItemModel {

    public constructor(
        private items: IAnswersCacheItem[] = []
    ) {}

    public setItem(answersCacheItem: IAnswersCacheItem): AnswersCacheItemModel {
        this.items.push(answersCacheItem);
        return this;
    }

    public removeItem(_answerId: string): AnswersCacheItemModel {
        const index: number = this.getIndexFromItems(_answerId);
        if (!!~index) {
            this.items.splice(index, 1);
        }
        return this;
    }

    public getItemById(_answerId: string): string {
        const index: number = this.getIndexFromItems(_answerId);
        return !!~index ? this.items.map((answersCacheItem: IAnswersCacheItem) => answersCacheItem._answerId)[index] : null;
    }

    public getItemsByQuestionId(_answerId: string): IAnswersCacheItem[] {
        return this.items.filter((answersCacheItem: IAnswersCacheItem) => answersCacheItem._questionId);
    }

    public removeItemsByQuestionId(_questionId: string): AnswersCacheItemModel {
        this.items.filter((answersCacheItem: IAnswersCacheItem) => answersCacheItem._questionId !== _questionId);
        return this;
    }

    public getIndexFromItems(id: string): number {
        return this.items.map((answersCacheItem: IAnswersCacheItem) => answersCacheItem._answerId).indexOf(id);
    }

    public clear(): AnswersCacheItemModel {
        this.items = [];
        return this;
    }

}