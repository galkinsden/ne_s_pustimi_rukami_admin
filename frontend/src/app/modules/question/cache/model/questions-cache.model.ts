import {QuestionsCacheItemModel} from "./questions-cache-item.model";
import {QuestionModel} from "../../models/question.model";
import {QuestionWithAnswerModel} from "../../models/question-with-answers.model";

export interface IQuestionsCache {
    question: QuestionModel;
    _questionId: string;
}

export class QuestionsCacheModel {

    public readonly created: QuestionsCacheItemModel = new QuestionsCacheItemModel([]);
    public readonly updated: QuestionsCacheItemModel = new QuestionsCacheItemModel([]);
    public readonly removed: QuestionsCacheItemModel = new QuestionsCacheItemModel([]);

    private cacheOld: IQuestionsCache[] = [];

    public constructor(

    ) {
        this.reset();
    }

    public setCacheOld(questionWithAnswerModels: QuestionWithAnswerModel[]): QuestionsCacheModel {
        this.cacheOld = questionWithAnswerModels.map((questionWithAnswerModel: QuestionWithAnswerModel) => {
            return {
                question: questionWithAnswerModel && questionWithAnswerModel.question  && questionWithAnswerModel.question || null,
                _questionId: questionWithAnswerModel && questionWithAnswerModel.question && questionWithAnswerModel.question.id || ''
            }
        });
        return this;
    }

    public getCacheOld(): IQuestionsCache[] {
        return this.cacheOld;
    }

    public reset(): QuestionsCacheModel {
        this.created.clear();
        this.updated.clear();
        this.removed.clear();
        this.cacheOld = [];
        return this;
    }

}
