import {AnswersCacheItemModel} from "./answers-cache-item.model";
import {AnswerModel} from "../../models/answer.model";
import {QuestionWithAnswerModel} from "../../models/question-with-answers.model";
import * as uuidv1 from 'uuid/v1';

export interface IAnswersCache {
    answer: AnswerModel;
    _questionId: string;
    _answerId: string;
}

export class AnswersCacheModel {

    public readonly created: AnswersCacheItemModel = new AnswersCacheItemModel([]);
    public readonly updated: AnswersCacheItemModel = new AnswersCacheItemModel([]);
    public readonly removed: AnswersCacheItemModel = new AnswersCacheItemModel([]);

    private cacheOld: IAnswersCache[] = [];

    public constructor(

    ) {
        this.reset();
    }

    public setCacheOld(questionWithAnswerModels: QuestionWithAnswerModel[]): AnswersCacheModel {
        this.cacheOld = questionWithAnswerModels.map((questionWithAnswerModel: QuestionWithAnswerModel) => {
            return {
                answer: questionWithAnswerModel && questionWithAnswerModel.answer || null,
                _questionId: questionWithAnswerModel && questionWithAnswerModel.question && questionWithAnswerModel.question.id || '',
                _answerId: questionWithAnswerModel && questionWithAnswerModel.answer && questionWithAnswerModel.answer.id || ''
            }
        });
        return this;
    }

    public getCacheOld(): IAnswersCache[] {
        return this.cacheOld;
    }

    public reset(): AnswersCacheModel {
        this.created.clear();
        this.updated.clear();
        this.removed.clear();
        this.cacheOld = [];
        return this;
    }

}
