export class QuestionsCacheItemModel {

    public constructor(
        private items: string[] = []
    ) {}

    public setItem(id: string): QuestionsCacheItemModel {
        this.items.push(id);
        return this;
    }

    public removeItem(id: string): QuestionsCacheItemModel {
        const index: number = this.getIndexFromItems(id);
        if (!!~index) {
            this.items.splice(index, 1);
        }
        return this;
    }

    public getItemById(id: string): string {
        const index: number = this.getIndexFromItems(id);
        return !!~index ? this.items[index] : null;
    }

    public getIndexFromItems(id: string): number {
        return this.items.indexOf(id);
    }

    public clear(): QuestionsCacheItemModel {
        this.items = [];
        return this;
    }

}