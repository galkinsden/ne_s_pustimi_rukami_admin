export interface ICategory {
    id: string; //id коллекции
    name: string; // название
    description?: string; // описание
}

export class CategoryModel {

    public readonly id: string;
    public readonly name: string;
    public readonly description: string;

    public constructor(src: ICategory) {
        this.id = src && src.id;
        this.name = src && src.name;
        this.description = src && src.description;
    }
}