import {EStateModel} from "../../../libraries/common/utils/state/state-model.enum";
import {StateModel} from "../../../libraries/common/utils/state/state.model";
import {CategoryModel} from "./category.model";

export interface ICategories {
    categories: CategoryModel[];
    state: EStateModel;
}

export class CategoriesModel extends StateModel implements ICategories {

    public constructor(
        public readonly categories: CategoryModel[] = [],
        public readonly state: EStateModel = EStateModel.NONE
    ) {
        super(state);
    }
}