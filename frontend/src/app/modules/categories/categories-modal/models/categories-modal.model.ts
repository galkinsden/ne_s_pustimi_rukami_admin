import {StateModel} from "../../../../libraries/common/utils/state/state.model";
import {EStateModel} from "../../../../libraries/common/utils/state/state-model.enum";
import {CategoryModel} from "../../models/category.model";
export interface ICategoriesModal {
    categories: CategoryModel[];
    state: EStateModel;
}

export class CategoriesModalModel extends StateModel implements ICategoriesModal {

    public constructor(
        public readonly categories: CategoryModel[] = [],
        public readonly state: EStateModel = EStateModel.NONE
    ) {
        super(state);
    }
}