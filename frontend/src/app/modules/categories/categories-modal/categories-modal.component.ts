import {Component, Inject, Input, OnDestroy, OnInit} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material";
import 'rxjs/add/operator/finally';
import 'rxjs/add/observable/throw';
import {EAbstractProductMode} from "../../products/abstract-product/enum/abstract-product-mode.enum";
import {CategoriesService} from "../categories.service";
import {Observable} from "rxjs/Observable";
import {CategoriesModalModel} from "./models/categories-modal.model";
import {CategoryModel, ICategory} from "../models/category.model";
import {EStateModel} from "../../../libraries/common/utils/state/state-model.enum";
import {FormArray, FormControl, FormGroup} from "@angular/forms";
import {Subscription} from "rxjs/Subscription";
import {UnSnackBarService} from "../../../libraries/common/ui/snackbar/un-snack-bar.service";
import * as uuidv1 from 'uuid/v1';

@Component({
    selector: 'categories-modal',
    templateUrl: './categories-modal.component.html',
    styleUrls: ['./categories-modal.component.scss']
})
export class CategoriesModalComponent implements OnInit {

    public categoriesModal$: Observable<CategoriesModalModel>;
    public modalForm: FormGroup;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<CategoriesModalComponent>,
        private categoriesService: CategoriesService,
        private unSnackBarService: UnSnackBarService
    ) {

    }

    public ngOnInit() {
        this.modalForm = new FormGroup({
            categories: new FormArray([])
        });
        this.categoriesModal$ = this.getAllCategories$();
    }

    private getAllCategories$(): Observable<CategoriesModalModel> {
        return this.categoriesService.getAll()
            .do((res: CategoryModel[]) => {
                this.initCategories(res);
            })
            .map((res: CategoryModel[]) => {
                return new CategoriesModalModel(
                    res,
                    EStateModel.LOADED
                )
            })
            .catch(() => {
                return Observable.of(new CategoriesModalModel(
                    [],
                    EStateModel.ERROR
                ))
            })
            .startWith(new CategoriesModalModel([], EStateModel.LOADING));
    }

    public createCategory(collection: ICategory): FormGroup {
        return new FormGroup({
            id: new FormControl(collection && collection.id || uuidv1()),
            name: new FormControl(collection && collection.name || ''),
            description: new FormControl(collection && collection.description || '')
        })
    }

    private initCategories(categories: ICategory[] = []): void {
        categories.forEach((collection: ICategory) => {
            this.categories.push(this.createCategory(collection))
        });
    }

    public get categories(): FormArray {
        return <FormArray>this.modalForm.get('categories');
    }

    public close(): void {
       this.dialogRef.close();
   }

    public isModeCreate(): boolean {
        return this.data.mode === EAbstractProductMode.CREATE;
    }

    public isModeEdit(): boolean {
        return this.data.mode === EAbstractProductMode.EDIT;
    }

    public isModeView(): boolean {
        return this.data.mode === EAbstractProductMode.VIEW;
    }

    public addNewCategory(): void {
        this.categories.push(this.createCategory(null));
    }

    public deleteCategoryByIndex(index: number): void {
        this.categories.removeAt(index);
    }

    public onSubmit(): void {
        this.categoriesModal$ = this.setCategories(this.modalForm.get('categories').value);
    }

    private setCategories(categories: ICategory[]): Observable<CategoriesModalModel> {
        return this.categoriesService.setCategoriesByIds(categories)
            .map((res: boolean) => {
                this.unSnackBarService.openSucces('Список категорий успешно обновлен');
                this.close();
                return new CategoriesModalModel([], EStateModel.LOADED);
            })
            .catch(() => {
                this.unSnackBarService.openError('Ошибка обновления списка категорий');
                this.close();
                return Observable.of(new CategoriesModalModel([], EStateModel.ERROR))
            })
            .startWith(new CategoriesModalModel([], EStateModel.LOADING));
    }
}