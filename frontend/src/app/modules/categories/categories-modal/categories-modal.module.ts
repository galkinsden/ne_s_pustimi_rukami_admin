import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {CategoriesModalComponent} from "./categories-modal.component";
import {
    MatButtonModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule,
    MatTooltipModule
} from "@angular/material";
import {CategoriesItemComponent} from "./categories-item/categories-item.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {PreloaderModule} from "../../../libraries/common/ui/preloader/preloader.module";


@NgModule({
    declarations: [CategoriesModalComponent, CategoriesItemComponent],
    exports: [CategoriesModalComponent, CategoriesItemComponent],
    imports: [
        CommonModule,
        RouterModule,
        MatDialogModule,
        MatIconModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        FormsModule,
        ReactiveFormsModule,
        MatTooltipModule,
        PreloaderModule
    ],
    providers: []
})

export class CategoriesModalModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: CategoriesModalModule};
    }
}
