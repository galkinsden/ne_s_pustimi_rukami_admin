import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
    MatButtonModule, MatFormFieldModule, MatIconModule, MatInputModule, MatRadioModule,
    MatSelectModule, MatTooltipModule
} from "@angular/material";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DisableControlModule} from "../../libraries/common/utils/disable-control/disable-control.module";
import {PreloaderModule} from "../../libraries/common/ui/preloader/preloader.module";
import {EmptyAreaModule} from "../../libraries/common/ui/empty-area/empty-area.module";
import {CategoriesComponent} from "./categories.component";
import {CategoriesModalModule} from "./categories-modal/categories-modal.module";
import {CategoriesModalComponent} from "./categories-modal/categories-modal.component";
import {CategoriesService} from "./categories.service";


@NgModule({
    declarations: [CategoriesComponent],
    exports     : [CategoriesComponent],
    imports     : [
        MatInputModule,
        MatFormFieldModule,
        FormsModule,
        ReactiveFormsModule,
        DisableControlModule,
        PreloaderModule,
        EmptyAreaModule,
        MatIconModule,
        MatButtonModule,
        CommonModule,
        MatSelectModule,
        MatTooltipModule,
        CategoriesModalModule
    ],
    providers   : [CategoriesService],
    entryComponents: [CategoriesModalComponent]
})

export class CategoriesModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: CategoriesModule};
    }
}
