import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {ConnectorErrorModel, IError} from '../../../libraries/common/utils/connector-error.model';
import {appConfig} from '../../../app.config';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {ICategory} from "../models/category.model";

export interface ICategoriesConnectorGetAllResponse {
    data: ICategory[];
    error: IError;
}

export interface ICategoriesConnectorSetCategoriesByIdsResponse {
    data: boolean;
    error: IError;
}


export class CategoriesConnector {

    constructor(private http: HttpClient) {}

    public getAll(): Observable<ICategory[] | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/categories-api/getCategories`, {})
            .map((res: ICategoriesConnectorGetAllResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public setCategoriesByIds(categories: ICategory[]): Observable<boolean | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/categories-api/setCategoriesByIds`, categories)
            .map((res: ICategoriesConnectorSetCategoriesByIdsResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    private handleError(res: IError): Observable<ConnectorErrorModel> {
        return Observable.throw(new ConnectorErrorModel(res));
    }
}