import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {ConnectorErrorModel} from "../../libraries/common/utils/connector-error.model";
import {CategoriesConnector} from "./connectors/categories.connector";
import {CategoryModel, ICategory} from "./models/category.model";

@Injectable()
export class CategoriesService {

    private categoriesConnector: CategoriesConnector;

    public constructor(http: HttpClient) {
        this.categoriesConnector = new CategoriesConnector(http);
    }

    public getAll(): Observable<ICategory[] | ConnectorErrorModel> {
        return this.categoriesConnector.getAll()
            .map((res: ICategory[]) => res.map((r: ICategory) => new CategoryModel(r)));
    }

    public setCategoriesByIds(categories: ICategory[]): Observable<boolean | ConnectorErrorModel> {
        return this.categoriesConnector.setCategoriesByIds(categories);
    }

}