import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import * as uuidv1 from 'uuid/v1';
import {EAbstractProductMode} from "../products/abstract-product/enum/abstract-product-mode.enum";
import {CategoryModel, ICategory} from "./models/category.model";
import {CategoriesModalComponent} from "./categories-modal/categories-modal.component";
import {MatDialog, MatSelectChange} from "@angular/material";
import {EStateModel} from "../../libraries/common/utils/state/state-model.enum";
import {CategoriesModalModel, ICategoriesModal} from "./categories-modal/models/categories-modal.model";
import {Observable} from "rxjs/Observable";
import {CategoriesService} from "./categories.service";
import {CategoriesModel} from "./models/categories.model";
import {Subscription} from "rxjs/Subscription";
import {UnSnackBarService} from "../../libraries/common/ui/snackbar/un-snack-bar.service";

@Component({
    selector: 'un-categories',
    templateUrl: 'categories.component.html',
    styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnChanges {

    @Input() public formCategories: FormArray = new FormArray([]);
    @Input() public mode: EAbstractProductMode;
    @Input() public categoriesInput: string[] = [];

    public categoriesModal$: Observable<CategoriesModel>;
    public formSelectControl: FormControl = new FormControl();

    public constructor(
        private dialog: MatDialog,
        private categoriesService: CategoriesService,
        private unSnackBarService: UnSnackBarService
    ) {

    }

    public ngOnChanges() {
        this.init();
    }

    private init(): void {
        this.clearFormArray(this.formCategories);
        this.categoriesModal$ = this.getAllCategories$();
        this.formSelectControl = new FormControl(this.categoriesInput);
    }

    public isModeCreate(): boolean {
        return this.mode === EAbstractProductMode.CREATE;
    }

    public isModeEdit(): boolean {
        return this.mode === EAbstractProductMode.EDIT;
    }

    public isModeView(): boolean {
        return this.mode === EAbstractProductMode.VIEW;
    }

    private getAllCategories$(): Observable<CategoriesModel> {
        return this.categoriesService.getAll()
            .do((res: CategoryModel[]) => {
                this.initCategories(res, this.categoriesInput);
            })
            .map((res: CategoryModel[]) => {
                return new CategoriesModel(
                    res,
                    EStateModel.LOADED
                )
            })
            .catch(() => {
                this.unSnackBarService.openError('Ошибка загрузки списка категорий');
                return Observable.of(new CategoriesModel(
                    [],
                    EStateModel.ERROR
                ))
            })
            .startWith(new CategoriesModalModel([], EStateModel.LOADING));
    }

    public createCategory(id: string): FormControl {
        return new FormControl(id || '')
    }

    private initCategories(categories: CategoryModel[] = [], ids: string[]): void {
        categories.filter((collection: CategoryModel) => !!~ids.indexOf(collection.id)).forEach((collection: CategoryModel) => {
            this.categories.push(this.createCategory(collection && collection.id || ''))
        });
    }

    public get categories(): FormArray {
        return <FormArray>this.formCategories;
    }

    public editCategories() {
        const sub: Subscription = this.dialog.open(CategoriesModalComponent, {panelClass: 'un-large-panel-class', data: {mode: this.mode}})
            .afterClosed()
            .finally(() => {
                sub.unsubscribe();
            })
            .subscribe(() => {
                this.init();
            })
    }

    public selectionChange($event: MatSelectChange): void {
        this.clearFormArray(this.formCategories);
        $event && $event.value && $event.value.forEach((id: string) => {
            this.formCategories.push(this.createCategory(id))
        })
    }

    public clearFormArray(formArray: FormArray): void {
        while (formArray.length !== 0) {
            formArray.removeAt(0)
        }
    }
}