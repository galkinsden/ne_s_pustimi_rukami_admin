import {Component, EventEmitter, Input, Output} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {ConnectorErrorModel} from "../../libraries/common/utils/connector-error.model";
import {EPermissionsCode} from "../../libraries/user/permissions/permissions-code.enum";
import {AuthService} from "../../libraries/auth/auth.service";
import {UserService} from "../../libraries/user/user.service";
import {UnSnackBarService} from "../../libraries/common/ui/snackbar/un-snack-bar.service";
import {IAbstractPermissions} from "../../libraries/user/permissions/abstract-permissions.interface";
import {merge} from "rxjs/observable/merge";
import {forkJoin} from "rxjs/observable/forkJoin";
import {IIngredient} from "./models/ingredients.model";

@Component({
    selector: 'ingredients',
    templateUrl: './ingredients.component.html',
    styleUrls: ['./ingredients.component.scss']
})
export class IngredientsComponent {

    @Input() mini: boolean = false;
    @Input() isAdditional: boolean = null;
    @Output() public selectIngredientEE: EventEmitter<IIngredient> = new EventEmitter();

    public static PATH: string = 'ingredients';
    public model$: Observable<IAbstractPermissions>;

    public constructor(
        private userService: UserService,
        private authService: AuthService,
        private unSnackBarService: UnSnackBarService
    ) {

    }

    public ngOnInit(): void {
        const userId: string = this.authService.getAuthUser().id;
        this.model$ = Observable.of({
            read: true,
            write: true
        });

    }

    public selectIngredient(ingredient: IIngredient): void {
        this.selectIngredientEE.emit(ingredient);
    }
}