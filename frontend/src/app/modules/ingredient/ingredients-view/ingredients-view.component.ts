import {Component, EventEmitter, forwardRef, Input, OnDestroy, OnInit, Output} from "@angular/core";
import {IngredientsService} from "../ingredients.service";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/last';
import 'rxjs/add/observable/of';
import {EStateModel} from "../../../libraries/common/utils/state/state-model.enum";
import {MatDialog} from "@angular/material";
import {IIngredientView, IngredientsViewModel} from "../models/ingredient-view.model";
import {AuthService} from "../../../libraries/auth/auth.service";
import {IngredientsListItemModel} from "../models/ingredient-list-item.model";
import {IIngredientFull, IngredientFullModel} from "../models/ingredient-full-model.model";
import {appConfig} from "../../../app.config";
import {AbstractIngredientModalComponent} from "../abstract-ingredient-modal/abstract-ingredient-modal.component";
import {EAbstractProductMode} from "../../products/abstract-product/enum/abstract-product-mode.enum";
import {IIngredientFullResponse} from "../connectors/models/response/ingredient-full-response.model";


@Component({
    selector: 'ingredients-view',
    templateUrl: './ingredients-view.component.html',
    styleUrls: ['./ingredients-view.component.scss']
})
export class IngredientsViewComponent/*implements OnInit, OnDestroy*/ {

    @Input() write: boolean = false;
    @Input() read: boolean = false;
    @Input() mini: boolean = false;
    @Output() public selectIngredientEE: EventEmitter<IIngredientFull> = new EventEmitter();

    public $model: Observable<IngredientsViewModel>;
    public disable: boolean = true;
    public apiUrl: string = appConfig.apiUrl;

    constructor(
        private ingredientsService: IngredientsService
    ) {

    }

    public ngOnInit(): void {
        this.$model = this.changeIngredientStream$();
    }

    public changeCountIngredientStream$(): Observable<EStateModel> {
        return this.ingredientsService.countIngredients
            .map((count: number) => count > 0 ? EStateModel.WAIT : EStateModel.NONE);
    }

    public changeIngredientStream$(): Observable<IngredientsViewModel> {
        return this.ingredientsService.selectedIngredient
            .combineLatest(this.changeCountIngredientStream$())
            .switchMap(([ingredientsListItemModel, state]: [IngredientsListItemModel, EStateModel]) => {
                return Observable.of({state: state, ingredientId: ingredientsListItemModel && ingredientsListItemModel.id ? ingredientsListItemModel.id : ''})
            })
            .map((ingredientViewResponse: IIngredientView) => {
                return new IngredientsViewModel(
                    ingredientViewResponse.ingredientId,
                    ingredientViewResponse.state
                )
            })
    }

    public selectIngredient(ingredientFull: IngredientFullModel): void {
        this.selectIngredientEE.emit(ingredientFull)
    }



}