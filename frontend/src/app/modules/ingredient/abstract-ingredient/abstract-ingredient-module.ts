import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {AbstractIngredientComponent} from "./abstract-ingredient.component";
import {
    MatButtonModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule,
    MatRadioModule, MatSelectModule
} from "@angular/material";
import {UnSnackBarModule} from "../../../libraries/common/ui/snackbar/un-snack-bar.module";
import {UnProgressBarModule} from "../../../libraries/common/ui/progress/un-progress-bar.module";
import {PhotoLargeModalModule} from "../../../libraries/common/ui/photo-large-modal/photo-large-modal.module";
import {PhotoSmallModule} from "../../../libraries/common/ui/photo-small/photo-small.module";
import {ImageUploaderModule} from "../../../libraries/common/ui/image-uploader/image-uploader.module";
import {PreloaderModule} from "../../../libraries/common/ui/preloader/preloader.module";
import {SecureModule} from "../../../libraries/common/ui/img-secure/secure.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DisableControlModule} from "../../../libraries/common/utils/disable-control/disable-control.module";
import {EmptyAreaModule} from "../../../libraries/common/ui/empty-area/empty-area.module";


@NgModule({
    declarations: [AbstractIngredientComponent],
    exports: [AbstractIngredientComponent],
    imports: [
        CommonModule,
        RouterModule,
        MatDialogModule,
        UnSnackBarModule,
        MatIconModule,
        UnProgressBarModule,
        MatFormFieldModule,
        PhotoSmallModule,
        PhotoLargeModalModule,
        ImageUploaderModule,
        PreloaderModule,
        SecureModule,
        FormsModule,
        ReactiveFormsModule,
        DisableControlModule,
        MatInputModule,
        MatButtonModule,
        MatRadioModule,
        EmptyAreaModule,
        MatSelectModule
    ],
    providers: []
})

export class AbstractIngredientModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: AbstractIngredientModule};
    }
}
