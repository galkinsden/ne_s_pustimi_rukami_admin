export enum EAbstractIngredientMode {
    CREATE = 'CREATE',
    EDIT = 'EDIT',
    VIEW = 'VIEW'
}