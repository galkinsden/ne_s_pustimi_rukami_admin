import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {IngredientFullModel} from "../models/ingredient-full-model.model";

@Injectable()
export class AbstractIngredientService {
    /**
     * Модель для хранения текущего состояний
     * @type {BehaviorSubject<IngredientFullModel>}
     */
    private readonly model: BehaviorSubject<string> = new BehaviorSubject<string>('');

    public setIngredientId(id: string): AbstractIngredientService {
        return this.setModel(id);
    }

    /**
     * Получить наблюдаемую модель
     * @returns {Observable<IngredientFullModel>}
     */
    public getObservableModel(): Observable<string> {
        return this.model.asObservable();
    }

    /**
     * Получить актуальную модель
     * @returns {IngredientFullModel}
     */
    public getModel(): string {
        return this.model.getValue();
    }

    private setModel(model: string): AbstractIngredientService {
        this.model.next(model);
        return this;
    }

}
