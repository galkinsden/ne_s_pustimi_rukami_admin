import {StateModel} from "../../../../libraries/common/utils/state/state.model";
import {EStateModel} from "../../../../libraries/common/utils/state/state-model.enum";
import {IngredientFullModel} from "../../models/ingredient-full-model.model";
export interface IAbstractIngredientFull {
    ingredient: IngredientFullModel;
    state: EStateModel;
}

export class AbstractIngredientFullModel extends StateModel implements IAbstractIngredientFull {

    public constructor(
        public readonly ingredient: IngredientFullModel = null,
        public readonly state: EStateModel = EStateModel.NONE
    ) {
        super(state);
    }
}