import {
    Component, EventEmitter, Input, OnChanges, OnInit, Output,
    SimpleChanges
} from "@angular/core";
import {MatDialog, MatDialogRef} from "@angular/material";
import {FormControl, FormGroup, Validators, FormArray} from "@angular/forms";
import {Subscription} from "rxjs/Subscription";
import {FileUploader} from "ng2-file-upload";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/finally';
import 'rxjs/add/observable/throw';
import {StateModel} from "../../../libraries/common/utils/state/state.model";
import {IngredientsService} from "../ingredients.service";
import {UnSnackBarService} from "../../../libraries/common/ui/snackbar/un-snack-bar.service";
import {AbstractIngredientService} from "./abstract-ingredient.service";
import {AuthService} from "../../../libraries/auth/auth.service";
import {EStateModel} from "../../../libraries/common/utils/state/state-model.enum";
import {appConfig} from "../../../app.config";
import {IIngredientFull, IngredientFullModel} from "../models/ingredient-full-model.model";
import {IIngredientFullRequest} from "../connectors/models/request/ingredient-full-request.model";
import {ConnectorErrorModel} from "../../../libraries/common/utils/connector-error.model";
import {EAbstractIngredientMode} from "./enum/abstract-ingredient-mode.enum";
import {AbstractIngredientModalComponent} from "../abstract-ingredient-modal/abstract-ingredient-modal.component";
import {Router} from "@angular/router";
import {ImageUploaderService} from "../../../libraries/common/ui/image-uploader/image-uploader.service";
import {EAbstractProductMode} from "../../products/abstract-product/enum/abstract-product-mode.enum";
import {IIngredientFullResponse} from "../connectors/models/response/ingredient-full-response.model";
import {AbstractIngredientFullPhotosModel} from "./models/abstract-ingredient-full-photos.model";
import {AbstractIngredientFullModel} from "./models/abstract-ingredient-full.model";
import {AppHttpInterceptor} from "../../../connectors/app-http.interceptor";
import {Subject} from "rxjs/Subject";
import {ECountUnit, EType} from "../models/ingredients.model";

@Component({
    selector: 'abstract-ingredient',
    templateUrl: './abstract-ingredient.component.html',
    styleUrls: ['./abstract-ingredient.component.scss']
})
export class AbstractIngredientComponent extends StateModel implements OnInit, OnChanges {

    @Input() public ingredientId: string = '';
    @Input() public mode: EAbstractIngredientMode = null;
    @Input() public dialogRef: MatDialogRef<AbstractIngredientModalComponent>;
    @Output() public selectIngredientEE: EventEmitter<IIngredientFull> = new EventEmitter();
    @Input() public mini: boolean = false;


    public ingredientForm: FormGroup;
    public apiUrl: string = appConfig.apiUrl;
    public cachedPhotos: FormArray = new FormArray([]);
    public uploader: FileUploader;
    public eCountUnit = Object.keys(ECountUnit);
    public eType = Object.keys(EType);
    public abstractIngredientFullModel$: Observable<AbstractIngredientFullModel>;
    public abstractIngredientFullPhotosModel$: Observable<AbstractIngredientFullPhotosModel>;

    public constructor(
        private ingredientsService: IngredientsService,
        private unSnackBarService: UnSnackBarService,
        private ingredientsCreateModalService: AbstractIngredientService,
        private authService: AuthService,
        private router: Router,
        private abstractIngredientService: AbstractIngredientService,
        private imageUploaderService: ImageUploaderService,
        private dialog: MatDialog,
        private appHttpInterceptor: AppHttpInterceptor
    ) {
        super(EStateModel.NONE);
    }

    public ngOnInit(): void {
        if (!this.ingredientForm) {
            this.initIngredientForm();
        }
    }

    public ngOnChanges(simpleChanges: SimpleChanges) {
        if (!this.ingredientForm) {
            this.initIngredientForm();
        }
        this.changesIngredientIdStream(this.ingredientId)
    }

    private changesIngredientIdStream(id: string): void {
        id ? this.changeIngredientIdIfIdYes(id) : this.changeIngredientIdIfIdNo()
    }

    private changeIngredientIdIfIdYes(ingredientId: string): void {

        this.abstractIngredientFullModel$ = this.getFullIngredient$(ingredientId)
            .startWith(new AbstractIngredientFullModel(null, EStateModel.LOADING))
            .do((abstractIngredientFullModel: AbstractIngredientFullModel) => {
                this.initIngredientFull(abstractIngredientFullModel && abstractIngredientFullModel.ingredient, abstractIngredientFullModel && abstractIngredientFullModel.ingredient && abstractIngredientFullModel.ingredient.id || ingredientId);
                this.initUploader();
            });

        this.abstractIngredientFullPhotosModel$ = this.getPhotos$(ingredientId)
            .startWith(new AbstractIngredientFullPhotosModel(null, EStateModel.LOADING))
            .do((abstractIngredientFullPhotosModel: AbstractIngredientFullPhotosModel) => {
                this.initIngredientPhotos(abstractIngredientFullPhotosModel && Array.isArray(abstractIngredientFullPhotosModel.photos) ? abstractIngredientFullPhotosModel.photos : [] );
            });
    }

    private changeIngredientIdIfIdNo(): void {

        this.abstractIngredientFullModel$ = this.createFullIngredient$()
            .do((abstractIngredientFullModel: AbstractIngredientFullModel) => {
                this.ingredientId = abstractIngredientFullModel && abstractIngredientFullModel.ingredient && abstractIngredientFullModel.ingredient.id;
                this.initIngredientFull(abstractIngredientFullModel && abstractIngredientFullModel.ingredient, abstractIngredientFullModel && abstractIngredientFullModel.ingredient && abstractIngredientFullModel.ingredient.id);
                this.initUploader();
            });

        this.abstractIngredientFullPhotosModel$ = this.createPhotos$()
            .do((abstractIngredientFullPhotosModel: AbstractIngredientFullPhotosModel) => {
                this.initIngredientPhotos(abstractIngredientFullPhotosModel && Array.isArray(abstractIngredientFullPhotosModel.photos) ? abstractIngredientFullPhotosModel.photos : [] );
            });

    }

    private createFullIngredient$(): Observable<AbstractIngredientFullModel> {
        return Observable.of(new AbstractIngredientFullModel(new IngredientFullModel(null), EStateModel.LOADED));
    }

    private getPhotos$(ingredientId: string): Observable<AbstractIngredientFullPhotosModel> {
        this.resetPhotos();
        return this.ingredientsService.getPhotos(ingredientId)
            .map((photos : string[]) => {
                return new AbstractIngredientFullPhotosModel(
                    Array.isArray(photos) && photos.length ? photos : [],
                    EStateModel.LOADED
                )
            })
            .catch(() => {
                return Observable.of(new AbstractIngredientFullPhotosModel(
                    [],
                    EStateModel.ERROR
                ))
            });
    }

    private createPhotos$(): Observable<AbstractIngredientFullPhotosModel> {
        this.resetPhotos();
        return Observable.of(new AbstractIngredientFullPhotosModel(
            [],
            EStateModel.LOADED
        ))
    }

    private resetPhotos(): void {
        this.ingredientForm.controls['photos'] = new FormArray([]);
    }

    private getFullIngredient$(ingredientId: string): Observable<AbstractIngredientFullModel> {
        return this.ingredientsService.getFullIngredientById(ingredientId)
            .map((ingredientFullResponse : IIngredientFullResponse) => {
                return new AbstractIngredientFullModel(
                    new IngredientFullModel(ingredientFullResponse),
                    EStateModel.LOADED
                )
            })
            .catch(() => {
                return Observable.of(new AbstractIngredientFullModel(
                    new IngredientFullModel(null),
                    EStateModel.ERROR
                ))
            });

    }

    private initIngredientFull(ingredientFull: IngredientFullModel, id: string): void {
        this.ingredientForm.get('id').setValue(id);
        this.ingredientForm.get('name').setValue(ingredientFull && ingredientFull.name || '');
        this.ingredientForm.get('count').setValue(ingredientFull && ingredientFull.count || 0);
        this.ingredientForm.get('countUnit').setValue(ingredientFull && ingredientFull.countUnit || ECountUnit.UNIT);
        this.ingredientForm.get('type').setValue(ingredientFull && ingredientFull.type || EType.COMMON);
        this.ingredientForm.get('costPrice').setValue(ingredientFull && ingredientFull.costPrice || 0);
        this.ingredientForm.get('sellPrice').setValue(ingredientFull && ingredientFull.sellPrice || 0);
        this.ingredientForm.get('alco').setValue(ingredientFull && ingredientFull.alco || false);
        this.ingredientForm.get('deleted').setValue(ingredientFull && ingredientFull.deleted || false);
        this.ingredientForm.get('isAdditional').setValue(ingredientFull && ingredientFull.isAdditional || false);
        this.ingredientForm.get('description').setValue(ingredientFull && ingredientFull.description || '');
    }

    private initIngredientPhotos(photos: string[]): void {
        photos
            .map((photo: string) => new FormControl(photo))
            .forEach((control: FormControl) => {
                this.photos.push(control);
            });
    }

    private initIngredientForm(): void {
        this.ingredientForm = new FormGroup({
            id: new FormControl('', Validators.required),
            name: new FormControl('', Validators.required),
            count: new FormControl(0),
            countUnit: new FormControl(ECountUnit.UNIT),
            type: new FormControl(EType.COMMON),
            costPrice: new FormControl(0, Validators.required),
            sellPrice: new FormControl(0, Validators.required),
            photos: new FormArray([]),
            alco: new FormControl(false, Validators.required),
            deleted: new FormControl(false),
            isAdditional: new FormControl(false),
            description: new FormControl(''),
        });
    }

    public get photos(): FormArray {
        return (<FormArray>this.ingredientForm.get('photos'));
    }

    private initUploader(): void {
        this.uploader = this.imageUploaderService.getNewUploader({
            url: `${appConfig.apiUrl}/ingredients/setUploadPhotos`
        })
    }

    public onSubmit(): void {
        if (this.ingredientForm.valid) {
            this.state = EStateModel.LOADING;
            const sub: Subscription = (this.uploader.getNotUploadedItems().length ? this.submitFormDataWithPhotos() : this.submitFormData())
                .finally(() => sub.unsubscribe())
                .subscribe((res: string) => {
                    this.ingredientsCreateModalService.setIngredientId(res);
                    this.unSnackBarService.openSucces(this.isModeEdit() ? 'Информация об ингредиенте успешно изменена' : 'Новый игредиент создан');
                    this.dialogRef && this.dialogRef.close();
                    this.state = EStateModel.LOADED;
                    return res;
                },
                    (error: ConnectorErrorModel) => {
                        this.unSnackBarService.openError(error && error.description || '');
                        this.state = EStateModel.ERROR;
                        return Observable.throw(error);
                    })
        }
    }

    private submitFormDataWithPhotos(): Observable<string> {
        return this.submitFormData()
            .switchMap((ingredientId: string) => {
                const subject: Subject<string> = new Subject();
                this.uploader.setOptions({headers: this.appHttpInterceptor.getHeadersIntercept().concat([{name: 'ingredientid', value: ingredientId}])});
                this.uploader.onCompleteAll = () => {
                    subject.next(ingredientId);
                };
                this.uploader.uploadAll();
                return subject.asObservable();
            })
    }

    private removeCachedPhotos(productId: string): Observable<boolean> {
        return this.cachedPhotos.value.length ? this.removePhotos(
            productId || '',
            this.cachedPhotos.value.length
                ? <string[]>this.cachedPhotos.value.map((path: string) => path.split('/').pop())
                : []
            )
            .do(() => {
                    this.cachedPhotos = new FormArray([]);
                })
            : Observable.of(true);
    }
    
    private submitFormData(): Observable<string> {
        return this.insertOrUpdateIngredient(this.getIngredientFullRequest())
            .switchMap((ingredientId: string) => {
                return this.removeCachedPhotos(ingredientId)
                    .map(() => ingredientId)
            })
            .map((ingredientId: string) => ingredientId)
            .catch((ingredientId) => Observable.of(ingredientId || ''))
    }
    
    private getIngredientFullRequest(): IIngredientFullRequest {
        return {
            id: this.ingredientForm.getRawValue().id,
            name: this.ingredientForm.getRawValue().name,
            count: this.ingredientForm.getRawValue().count,
            countUnit: this.ingredientForm.getRawValue().countUnit,
            type: this.ingredientForm.getRawValue().type,
            costPrice: this.ingredientForm.getRawValue().costPrice,
            sellPrice: this.ingredientForm.getRawValue().sellPrice,
            description: this.ingredientForm.getRawValue().description,
            alco: this.ingredientForm.getRawValue().alco,
            isAdditional: this.ingredientForm.getRawValue().isAdditional
        };
    }

    private insertOrUpdateIngredient(ingredientFullRequest: IIngredientFullRequest): Observable<string> {
        return (this.isModeEdit()
                ? this.ingredientsService.updateIngredient(ingredientFullRequest)
                : this.ingredientsService.insertIngredient(ingredientFullRequest)
            )
            .map((ingredientId: string) => ingredientId)
            .catch(() => Observable.of(''))
    }

    public removeIngredient(ingredientId: string): void {
        const subRemove: Subscription = this.ingredientsService.removeIngredientById(ingredientId)
            .map((res: boolean) => {
                this.abstractIngredientService.setIngredientId('');
                this.dialogRef && this.dialogRef.close();
                this.unSnackBarService.openSucces('Ингредиент успешно перенесен в корзину');
                return res;
            })
            .catch((error: ConnectorErrorModel) => {
                this.abstractIngredientService.setIngredientId('');
                this.unSnackBarService.openError('Ошибка переноса ингредиента в корзину');
                return null;
            })
            .subscribe(() => {
                subRemove.unsubscribe();
            })
    }

    public deletePhoto(i: number): void {
        if (this.isModeEdit() || this.isModeCreate()) {
            this.cachedPhotos.push(this.photos.controls[i]);
            this.photos.removeAt(i);
        }
    }

    public removePhotos(ingredientId: string, ingredientFileNames: string[]): Observable<boolean>{
        return this.ingredientsService.removePhotos(ingredientId, ingredientFileNames);
    }

    public isModeCreate(): boolean {
        return this.mode === EAbstractIngredientMode.CREATE;
    }

    public isModeEdit(): boolean {
        return this.mode === EAbstractIngredientMode.EDIT;
    }

    public isModeView(): boolean {
        return this.mode === EAbstractIngredientMode.VIEW;
    }


    public editIngredient(ingredientId: string) {
        let dialogRef = this.dialog.open(AbstractIngredientModalComponent, {panelClass: 'un-large-panel-class', data: {ingredientId: ingredientId, mode: EAbstractProductMode.EDIT}});
    }

    public selectIngredient(ingredientFull: IngredientFullModel): void {
        this.selectIngredientEE.emit(ingredientFull)
    }

    public getCountUnitTitle(countUnit: ECountUnit): string {
        switch(countUnit) {
            case ECountUnit.UNIT:
                return 'шт';
            case ECountUnit.GRAM:
                return 'гр';
            case ECountUnit.KILOGRAM:
                return 'кг';
            default:
                return 'шт';
        }
    }

    public getTypeTitle(type: EType): string {
        switch(type) {
            case EType.COMMON:
                return 'обычный ингредиент';
            case EType.DECOR:
                return 'декор';
            default:
                return 'обычный ингредиент';
        }
    }
}