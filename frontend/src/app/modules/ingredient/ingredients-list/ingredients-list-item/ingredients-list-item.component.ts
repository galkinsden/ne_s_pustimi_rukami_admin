import {Component, Input, OnInit} from "@angular/core";
import * as moment from 'moment';
import {EStatus, IngredientModel} from "../../models/ingredients.model";

@Component({
    selector: 'ingredients-list-item',
    templateUrl: './ingredients-list-item.component.html',
    styleUrls: ['./ingredients-list-item.component.scss']
})
export class IngredientsListItemComponent {
    @Input() public searchString: string = '';
    @Input() public selected: boolean = false;
    @Input() public model: IngredientModel;
    @Input() public groupStatus: EStatus;
    @Input() public searchMode: boolean = false;

    constructor() {}

    public enumStatus = EStatus;

    public getLowerStatus(status: EStatus): string {
        return status ? `is-${status.toLowerCase()}` : '';
    }

    public getFormatDate(dateDelivery: string): string {
        return moment(dateDelivery).format('DD.MM.YYYY HH:mm:ss')
    }
}