import {
    AfterViewInit, Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output,
    ViewChild
} from "@angular/core";
import {IngredientsService} from "../../ingredients.service";
import {ActivatedRoute, Params} from "@angular/router";
import * as moment from 'moment';
import {Moment} from 'moment';
import {IngredientsListControllerService} from "./ingredients-list-controller.service";
import {Observable} from "rxjs/Observable";
import {Subscription} from "rxjs/Subscription";
import {MatDialog} from "@angular/material";
import {IngredientsListControllerModel} from "../../models/ingredient-list-controller.model";
import {AbstractIngredientModalComponent} from "../../abstract-ingredient-modal/abstract-ingredient-modal.component";
import {EAbstractIngredientMode} from "../../abstract-ingredient/enum/abstract-ingredient-mode.enum";
import {FormControl, FormGroup} from "@angular/forms";
import "rxjs/add/operator/map";
import "rxjs/add/operator/debounceTime";
import "rxjs/add/operator/delay";
import "rxjs/add/observable/fromEvent";
import 'rxjs/add/operator/mergeMap';
import {Subject} from "rxjs/Subject";

@Component({
    selector: 'ingredients-list-controller',
    templateUrl: './ingredients-list-controller.component.html',
    styleUrls: ['./ingredients-list-controller.component.scss']
})
export class IngredientsListControllerComponent implements OnInit, OnDestroy {

    @Input() public disabledSearch: boolean = false;
    @Input() write: boolean = false;
    @Input() read: boolean = false;
    @Input() mini: boolean = false;

    private subscription: Subscription;
    public $model: Observable<IngredientsListControllerModel>;
    public formController: FormGroup;
    public keyUp = new Subject<any>();

    private intervalTime: number = 300;

    constructor(
        private ingredientsListController: IngredientsListControllerService,
        private ingredientsService: IngredientsService,
        private route: ActivatedRoute,
        private dialog: MatDialog
    ) {
    }

    public ngOnInit(): void {

        this.formController = this.createFormControl();

        this.$model = this.ingredientsListController.getObservableModel()
            .map((model: IngredientsListControllerModel) => {
                return model;
            });

        this.subscription = this.keyUp
            .map(event => event.target.value)
            .debounceTime(this.intervalTime)
            .flatMap((search: string) => Observable.of(search))
            .subscribe((searchString: string) => {
                this.onSubmit(searchString);
            });

        this.ingredientsListController.setSearchString('');
    }

    public ngOnDestroy(): void {
        this.subscription && this.subscription.unsubscribe();
    }

    public createNew(): void {
        let dialogRef = this.dialog.open(AbstractIngredientModalComponent, {panelClass: 'un-large-panel-class', data: {ingredientId: null, mode: EAbstractIngredientMode.CREATE}});
    }

    public onSubmit(searchString: string): void {
        this.ingredientsListController.setSearchString(searchString || this.formController.get('name').value);
    }

    public onSubmitInterval($event): void {
        this.keyUp.next($event);
    }

    private createFormControl(): FormGroup {
        return new FormGroup({
            name: new FormControl('')
        });
    }
}