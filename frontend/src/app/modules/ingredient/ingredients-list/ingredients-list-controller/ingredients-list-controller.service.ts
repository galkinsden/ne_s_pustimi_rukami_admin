import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Moment} from 'moment';
import {IngredientsListControllerModel} from "../../models/ingredient-list-controller.model";

/**
 * Сервис для контроля параметров отображения списков (дата, поисковая строчка, режим поиска)
 */
@Injectable()
export class IngredientsListControllerService {
    /**
     * Модель для хранения текущего состояний
     * @type {BehaviorSubject<IngredientsListControllerModel>}
     */
    private readonly model: BehaviorSubject<IngredientsListControllerModel> = new BehaviorSubject<IngredientsListControllerModel>(new IngredientsListControllerModel());

    public setSearchString(searchString: string): IngredientsListControllerService {
        return this.setModel(new IngredientsListControllerModel(searchString && searchString.toLocaleLowerCase() || ''));
    }

    /**
     * Получить наблюдаемую модель
     * @returns {Observable<IngredientsListControllerModel>}
     */
    public getObservableModel(): Observable<IngredientsListControllerModel> {
        return this.model.asObservable();
    }

    /**
     * Получить актуальную модель
     * @returns {IngredientsListControllerModel}
     */
    public getModel(): IngredientsListControllerModel {
        return this.model.getValue();
    }

    private setModel(model: IngredientsListControllerModel): IngredientsListControllerService {
        this.model.next(model);
        return this;
    }

}
