import {Component, Input, OnInit} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {ActivatedRoute, Params, Router} from "@angular/router";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/last';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/exhaustMap';
import 'rxjs/add/observable/interval';
import {merge} from "rxjs/observable/merge";
import {IngredientsListModel} from "../models/ingredient-list.model";
import {Subject} from "rxjs/Subject";
import {IngredientsService} from "../ingredients.service";
import {IngredientsListControllerService} from "./ingredients-list-controller/ingredients-list-controller.service";
import {EStatus, IConfigGroup, IGroup, IngredientModel} from "../models/ingredients.model";
import {IngredientsListControllerModel} from "../models/ingredient-list-controller.model";
import {EStateModel} from "../../../libraries/common/utils/state/state-model.enum";
import {IngredientsListItemModel} from "../models/ingredient-list-item.model";
import {ConnectorErrorModel} from "../../../libraries/common/utils/connector-error.model";
import {AbstractIngredientService} from "../abstract-ingredient/abstract-ingredient.service";
import {IGetAllRequest} from "../connectors/models/request/get-all-request";

interface IIngredientsListResponse {
    state: EStateModel;
    list: IngredientModel[];
    searchString: string;
}

@Component({
    selector: 'ingredients-list',
    templateUrl: './ingredients-list.component.html',
    styleUrls: ['./ingredients-list.component.scss']
})
export class IngredientsListComponent implements OnInit {

    @Input() write: boolean = false;
    @Input() read: boolean = false;
    @Input() mini: boolean = false;
    @Input() isAdditional: boolean = null;

    public $model: Observable<IngredientsListModel>;
    private _clickSelectedIngredient$: Subject<string> = new Subject();
    private searchString$: Subject<string> = new Subject();
    /**
     * Время обновления списка
     * @type {number}
     */
    protected readonly intervalTime: number = 1000 * 60 * 10;

    public constructor(
        private ingredientsService: IngredientsService,
        private router: Router,
        private route: ActivatedRoute,
        private ingredientsListControllerService: IngredientsListControllerService,
        private abstractIngredientService: AbstractIngredientService
    ) {

    }

    public ngOnInit(): void {
        this.$model = merge(this.getIngredientsStream(), this.getAutoUpdateStream())
            .combineLatest(this.getSelectedIdNumberStream())
            .map(([ingredientListResponse, selectedId] : [IIngredientsListResponse, string]) => {
                return new IngredientsListModel(
                    this.groupBuyers(ingredientListResponse && ingredientListResponse.list || []),
                    ingredientListResponse && ingredientListResponse.list && ingredientListResponse.list.length || 0,
                    ingredientListResponse && ingredientListResponse.list && selectedId ? ingredientListResponse.list.find((ingredient: IngredientModel) => ingredient.id === selectedId) :  null,
                    ingredientListResponse && ingredientListResponse.searchString || '',
                    ingredientListResponse && ingredientListResponse.state || EStateModel.LOADED
                )
            })
            .catch(() => {
                return Observable.of(new IngredientsListModel(
                    this.groupBuyers([]),
                    0,
                    null,
                    '',
                    EStateModel.ERROR
                ));
            })
            .do((model: IngredientsListModel) => {
                this.ingredientsService.countIngredients.next(model.total);
                this.ingredientsService.selectedIngredient.next(model.selected);

                if (!this.mini) {
                    this.router.navigate(['./'], {
                        relativeTo: this.route,
                        replaceUrl: true,
                        queryParams: {
                            id: model.selected ? model.selected.id : null
                        }
                    });
                }
            })
    }

    private getAutoUpdateStream(): Observable<IIngredientsListResponse> {
        //TODO filters
        return Observable
            .interval(this.intervalTime)
            .exhaustMap(() => this.ingredientsService.getAll({searchString: '', isAdditional: this.isAdditional})
                .map((res: IngredientModel[]) => ({state: EStateModel.LOADED, list: res, searchString: ''}))
                .catch((err: ConnectorErrorModel) => Observable.throw({state: EStateModel.ERROR, list: [], searchString: ''}))
            )
           /* .filter((i: IAppointmentsListResponse) => i !== null && !this.model.isSearchMode);*/
    }

    public onClickToItem(item: IngredientsListItemModel): void {
        this._clickSelectedIngredient$.next(item ? item.id : null);

    }

    private getAllIngredients(getAllRequest: IGetAllRequest): Observable<IngredientModel[]> {
        return this.ingredientsService.getAll(getAllRequest)
            .map((ingredientModels: IngredientModel[]) => {
                return ingredientModels;
            })
    }

    private getSelectedIdNumberStream(): Observable<string> {
        return this.route.queryParams
            .first()
            .map((params: Params): string => {
                const result: string = params['id'];
                return result || null;
            })
            .merge(this._clickSelectedIngredient$);
    }

    /**
     * Группировка записей по типу и их сортировка
     * @returns {IGroup[]}
     */
    private groupBuyers(list: IngredientModel[]): IGroup[] {
        const result: IGroup[] = IngredientsService
            .getGroups()
            .map((group: IGroup) => ({...group, list: []}));

        list.forEach((item: IngredientModel) => {
            const config: IConfigGroup = IngredientsService.getGroupTypeByIngredientStatus(item.status);
            const group: IGroup = result.find((g: IGroup) => g.status === config.status);
            if (group) {
                group.list.push(item);
            }
        });

        return result
            .sort((a: IGroup, b: IGroup) => a.weight - b.weight)
            .map((g: IGroup) => {
                g.list = g.list
                    .sort((a: IngredientModel, b: IngredientModel) => (a.name && b.name) ? a.name.localeCompare(b.name) : 0);
                return g;
            });
    }

    public getLowerStatus(status: EStatus): string {
        return status ? `is-${(status + '').toLowerCase()}` : '';
    }

    private getIngredientsStream(): Observable<IIngredientsListResponse> {
        return this.ingredientsListControllerService.getObservableModel()
            .combineLatest(this.abstractIngredientService.getObservableModel())
            .switchMap(([controllerModel, ingredientId]: [IngredientsListControllerModel, string]) => {
                return this.getAllIngredients({searchString: controllerModel && controllerModel.searchString || '', isAdditional: this.isAdditional})
                    .map((res: IngredientModel[]) => ({
                        state: EStateModel.LOADED,
                        list: res,
                        searchString: controllerModel && controllerModel.searchString || ''
                    }))
                    .catch((err: ConnectorErrorModel) => Observable.throw({state: EStateModel.ERROR, list: [], searchString: ''}))
                    .startWith({state: EStateModel.LOADING, list: [], searchString: ''})
            });
    }
}