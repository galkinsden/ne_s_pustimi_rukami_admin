import {Injectable} from "@angular/core";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {IngredientsListItemModel} from "./models/ingredient-list-item.model";
import {EStatus, IConfigGroup, IIngredient, IngredientModel} from "./models/ingredients.model";
import {ConnectorErrorModel} from "../../libraries/common/utils/connector-error.model";
import {Observable} from "rxjs/Observable";
import {HttpClient} from "@angular/common/http";
import {IngredientsConnector} from "./connectors/ingredients.connector";
import {IIngredientFull, IngredientFullModel} from "./models/ingredient-full-model.model";
import {IIngredientFullRequest} from "./connectors/models/request/ingredient-full-request.model";
import {IIngredientFullResponse} from "./connectors/models/response/ingredient-full-response.model";
import {IGetAllRequest} from "./connectors/models/request/get-all-request";


@Injectable()
export class IngredientsService {

    private ingredientsConnector: IngredientsConnector;

    constructor(http: HttpClient) {
        this.ingredientsConnector = new IngredientsConnector(http);
    }

    public readonly countIngredients: BehaviorSubject<number> = new BehaviorSubject(null);

    public readonly selectedIngredient: BehaviorSubject<IngredientsListItemModel> = new BehaviorSubject(null);
    
    public getAll(getAllRequest: IGetAllRequest): Observable<IngredientModel[] | ConnectorErrorModel> {
        return this.ingredientsConnector.getAll(getAllRequest)
            .map((res: IIngredient[]) => {
                return res && res.length ? res.map((r: IIngredient) => {
                    return new IngredientModel(r);
                }) : [];
            });
    }

    public getFullIngredientById(ingredientId: string): Observable<IIngredientFullResponse | ConnectorErrorModel> {
        return this.ingredientsConnector.getFullIngredientById(ingredientId);
    }

    public getIngredientsFullByIds(ids: string[]): Observable<IngredientFullModel[] | ConnectorErrorModel> {
        return this.ingredientsConnector.getIngredientsFullByIds(ids)
            .map((ingredientFullResponses: IIngredientFullResponse[]) => ingredientFullResponses.map((ingredientFullResponse: IIngredientFullResponse) => new IngredientFullModel(ingredientFullResponse)))
    }

    public insertIngredient(ingredient: IIngredientFullRequest): Observable<string> {
        return this.ingredientsConnector.insertIngredient(ingredient)
            .map((res: string) => res)
    }

    public removeIngredientById(ingredientId: string): Observable<boolean> {
        return this.ingredientsConnector.removeIngredientById(ingredientId)
            .map((res: boolean) => res)
    }

    public removePhotos(ingredientId: string, ingredientFileNames: string[]): Observable<boolean> {
        return this.ingredientsConnector.removePhotos(ingredientId, ingredientFileNames)
            .map((res: boolean) => res)
    }

    public getPhotos(ingredientId: string): Observable<string[] | ConnectorErrorModel> {
        return this.ingredientsConnector.getPhotos(ingredientId);
    }

    public updateIngredient(ingredient: IIngredientFullRequest): Observable<string> {
        return this.ingredientsConnector.updateIngredient(ingredient)
            .map((res: string) => res)
    }

    /**
     * Получаем список групп (коллекция Map)
     * @returns {Array<IConfigGroup>}
     */
    public static getGroups(): IConfigGroup[] {
        return Array.from(configGroup.values());
    }

    public static getGroupTypeByIngredientStatus(status: EStatus): IConfigGroup {
        switch (status) {
            case EStatus.AVAILABLE:
                return configGroup.get(EStatus.AVAILABLE);
            case EStatus.NOT_AVAILABLE:
                return configGroup.get(EStatus.NOT_AVAILABLE);
            case EStatus.DOWNSHIFTER:
                return configGroup.get(EStatus.DOWNSHIFTER);
            default:
                return configGroup.get(EStatus.OUTSIDE);
        }
    }

}

export const configGroup: Map<EStatus, IConfigGroup> = new Map([
    [EStatus.DOWNSHIFTER,    {status: EStatus.DOWNSHIFTER, title: 'Ошибка вычета', weight: 0}],
    [EStatus.AVAILABLE,    {status: EStatus.AVAILABLE, title: 'В наличии', weight: 1}],
    [EStatus.NOT_AVAILABLE,     {status: EStatus.NOT_AVAILABLE, title: 'Нет в наличии', weight: 1}]
]);