import {IIngredient, IngredientModel} from "./ingredients.model";

export interface IIngredientFull extends IIngredient {
    photos: string[];
    alco: boolean;
    description: string;
    deleted: boolean;
}

export class IngredientFullModel extends IngredientModel implements IIngredientFull {

    public readonly photos: string[];
    public readonly alco: boolean;
    public readonly description: string;
    public readonly deleted: boolean;

    public constructor(src: IIngredientFull) {
        super(src);
        this.alco = src && src.alco;
        this.deleted = src && src.deleted;
        this.description = src && src.description || '';
        this.photos = src && Array.isArray(src.photos) ? src.photos : [];
    }
}