export interface IIngredientsListItem {
    id: string;
    name: string;
}

export class IngredientsListItemModel implements IIngredientsListItem {

    public constructor(
        public readonly id: string = '',
        public readonly name: string = '',
    ) {

    }
}