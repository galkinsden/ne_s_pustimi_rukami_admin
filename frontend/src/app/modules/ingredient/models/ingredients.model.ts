import {IIngredientsListItem} from "./ingredient-list-item.model";
import {OrderModel} from "../../../libraries/buyer/order/models/order.model";
import * as uuidv1 from 'uuid/v1';

export enum EStatus {
    AVAILABLE = 'AVAILABLE',
    NOT_AVAILABLE = 'NOT_AVAILABLE',
    OUTSIDE = 'OUTSIDE',
    DOWNSHIFTER = 'DOWNSHIFTER'
}

/**
 * Конфигурация группы
 */
export interface IConfigGroup {
    status: EStatus;
    title: string; // русское название
    weight: number; // вес для сортировки группы
}

export enum ECountUnit {
    UNIT = 'UNIT',
    GRAM = 'GRAM',
    KILOGRAM = 'KILOGRAM'
}

export enum EType {
    COMMON = 'COMMON',
    DECOR = 'DECOR'
}

/**
 * Группа и ее список записей
 */
export interface IGroup extends IConfigGroup {
    list: IIngredientsListItem[];
}

export interface IIngredient {
    id: string;
    name: string;
    count: number;
    countUnit: ECountUnit;
    type: EType;
    costPrice: number;
    sellPrice: number;
    isAdditional: boolean;
}

export class IngredientModel implements IIngredient {

    public readonly id: string;
    public readonly name: string;
    public readonly count: number;
    public readonly countUnit: ECountUnit;
    public readonly type: EType;
    public readonly costPrice: number;
    public readonly sellPrice: number;
    public readonly status: EStatus; //считается динамически
    public readonly isAdditional: boolean;

    public constructor(src: IIngredient) {
        this.id = src && src.id || uuidv1();
        this.name = src && src.name || '';
        this.count = src && src.count || 0;
        this.countUnit = src && src.countUnit || ECountUnit.UNIT;
        this.type = src && src.type || EType.COMMON;
        this.costPrice = src && src.costPrice || 0;
        this.sellPrice = src && src.sellPrice || 0;
        this.status = this.getStatusIngredient(src && src.count);
        this.isAdditional = src && (typeof src.isAdditional === 'boolean') ? src.isAdditional : false;
    }
    public getStatusIngredient(count: number): EStatus {
        if (count && count > 0) {
            return EStatus.AVAILABLE;
        } else if(count && count < 0) {
            return EStatus.DOWNSHIFTER;
        } else {
            return EStatus.NOT_AVAILABLE;
        }
    }

}