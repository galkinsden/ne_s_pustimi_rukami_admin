import {StateModel} from "../../../libraries/common/utils/state/state.model";
import {EStateModel} from "../../../libraries/common/utils/state/state-model.enum";
import {IGroup} from "./ingredients.model";
import {IngredientsListItemModel} from "./ingredient-list-item.model";

export class IngredientsListModel extends StateModel {

    public constructor(
        public readonly groups: IGroup[] = [],
        public readonly total: number = 0,
        public readonly selected: IngredientsListItemModel = null,
        public readonly searchString: string = '',
        public readonly state: EStateModel = EStateModel.LOADING
    ) {
        super(state);
    }
}