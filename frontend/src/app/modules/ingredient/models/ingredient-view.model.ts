import {StateModel} from "../../../libraries/common/utils/state/state.model";
import {EStateModel} from "../../../libraries/common/utils/state/state-model.enum";
import {IngredientFullModel} from "./ingredient-full-model.model";
import {EAbstractIngredientMode} from "../abstract-ingredient/enum/abstract-ingredient-mode.enum";

export interface IIngredientView {
    ingredientId: string;
    state: EStateModel;
}

export class IngredientsViewModel extends StateModel implements IIngredientView {

    public constructor(
        public readonly ingredientId: string = '',
        public readonly state: EStateModel = EStateModel.NONE,
        public readonly mode: EAbstractIngredientMode = EAbstractIngredientMode.VIEW
    ) {
        super(state);
    }
}