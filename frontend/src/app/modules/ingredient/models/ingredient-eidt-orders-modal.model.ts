import {EStateModel} from "../../../libraries/common/utils/state/state-model.enum";
import {StateModel} from "../../../libraries/common/utils/state/state.model";

export class IngredientEditOrdersModalModel extends StateModel {

    public constructor(
        public readonly state: EStateModel = EStateModel.NONE
    ) {
        super(state);
    }
}