import {IIngredientResponse} from "./ingredient-response.model";

export interface IIngredientFullResponse extends IIngredientResponse {
    photos: string[];
    description: string;
    alco: boolean;
    deleted: boolean;
}