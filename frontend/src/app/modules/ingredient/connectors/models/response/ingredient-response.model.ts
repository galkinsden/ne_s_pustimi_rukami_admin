import {ECountUnit, EType} from "../../../models/ingredients.model";

export interface IIngredientResponse {
    id: string;
    name: string;
    count: number;
    countUnit: ECountUnit;
    type: EType;
    costPrice: number;
    sellPrice: number;
    isAdditional: boolean;
}
