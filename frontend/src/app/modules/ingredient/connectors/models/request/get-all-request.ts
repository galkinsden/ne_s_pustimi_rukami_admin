export interface IGetAllRequest {
    searchString: string;
    isAdditional: boolean;
}