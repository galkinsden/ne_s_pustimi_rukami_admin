import {ECountUnit, EType} from "../../../models/ingredients.model";

export interface IIngredientFullRequest {
    id: string;
    name: string;
    count: number;
    countUnit: ECountUnit;
    type: EType;
    costPrice: number;
    sellPrice: number;
    alco: boolean;
    isAdditional: boolean;
    description: string;
}
