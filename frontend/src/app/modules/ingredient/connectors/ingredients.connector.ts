import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {ConnectorErrorModel, IError} from '../../../libraries/common/utils/connector-error.model';
import {appConfig} from '../../../app.config';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {IIngredient} from "../models/ingredients.model";
import {IIngredientFull} from "../models/ingredient-full-model.model";
import {IIngredientFullResponse} from "./models/response/ingredient-full-response.model";
import {IIngredientFullRequest} from "./models/request/ingredient-full-request.model";
import {IGetAllRequest} from "./models/request/get-all-request";

export interface IIngredientsConnectorGetAllResponse {
    data: IIngredient[];
    error: IError;
}

export interface IIngredientsConnectorGetIngredientFullByIdResponse {
    data: IIngredientFullResponse;
    error: IError;
}

export interface IIngredientsConnectorGetIngredientsFullByIdsResponse {
    data: IIngredientFullResponse[];
    error: IError;
}

export interface IIngredientsConnectorUpdateIngredientResponse {
    data: string;
    error: IError;
}

export interface IIngredientsConnectorRemoveIngredientResponse {
    data: boolean;
    error: IError;
}

export interface IIngredientsConnectorInsertIngredientResponse {
    data: string;
    error: IError;
}

export interface IIngredientsConnectorRemovePhotosResponse {
    data: boolean;
    error: IError;
}

export interface IIngredientsConnectorGetPhotosResponse {
    data: string[];
    error: IError;
}

export class IngredientsConnector {

    constructor(private http: HttpClient) {}

    public getAll(getAllRequest: IGetAllRequest): Observable<IIngredient[] | ConnectorErrorModel> {
        let request: Object = {searchString: getAllRequest && getAllRequest.searchString};
        if (getAllRequest && getAllRequest.isAdditional) {
            request = {...request, ...{ isAdditional: getAllRequest.isAdditional}};
        } else if (getAllRequest && (typeof getAllRequest.isAdditional === 'boolean') && !getAllRequest.isAdditional) {
            request = {...request, ...{ isAdditional: getAllRequest.isAdditional}};
        }
        return this.http.post(`${appConfig.apiUrl}/ingredients/getAll`, request)
            .map((res: IIngredientsConnectorGetAllResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public getFullIngredientById(ingredientId: string): Observable<IIngredientFullResponse | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/ingredients/getIngredientFullById`, {ingredientId: ingredientId})
            .map((res: IIngredientsConnectorGetIngredientFullByIdResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public getIngredientsFullByIds(ids: string[]): Observable<IIngredientFullResponse[] | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/ingredients/getIngredientsFullByIds`, {ids: ids})
            .map((res: IIngredientsConnectorGetIngredientsFullByIdsResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public insertIngredient(ingredient: IIngredientFullRequest): Observable<string | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/ingredients/setInsertIngredient`, {ingredient: ingredient})
            .map((res: IIngredientsConnectorInsertIngredientResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public updateIngredient(ingredient: IIngredientFullRequest): Observable<string | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/ingredients/setUpdateIngredient`, {ingredient: ingredient})
            .map((res: IIngredientsConnectorUpdateIngredientResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public removePhotos(ingredientId: string, ingredientFileNames: string[]): Observable<boolean | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/ingredients/setRemovePhotos`, {ingredientId: ingredientId, ingredientFileNames: ingredientFileNames})
            .map((res: IIngredientsConnectorRemovePhotosResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public getPhotos(ingredientId: string): Observable<string[] | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/ingredients/getPhotos`, {ingredientId: ingredientId})
            .map((res: IIngredientsConnectorGetPhotosResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public removeIngredientById(ingredientId: string): Observable<boolean | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/ingredients/setRemoveIngredient`, {ingredientId: ingredientId})
            .map((res: IIngredientsConnectorRemoveIngredientResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    private handleError(res: IError): Observable<ConnectorErrorModel> {
        return Observable.throw(new ConnectorErrorModel(res));
    }
}