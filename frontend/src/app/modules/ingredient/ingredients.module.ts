import {ModuleWithProviders, NgModule} from "@angular/core";
import {IngredientsComponent} from "./ingredients.component";
import {IngredientsListComponent} from "./ingredients-list/ingredients-list.component";
import {IngredientsListControllerComponent} from "./ingredients-list/ingredients-list-controller/ingredients-list-controller.component";
import {IngredientsListItemComponent} from "./ingredients-list/ingredients-list-item/ingredients-list-item.component";
import {IngredientsViewComponent} from "./ingredients-view/ingredients-view.component";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {
    MatButtonModule, MatCheckboxModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule,
    MatOptionModule,
    MatSelectModule, MatTooltipModule
} from "@angular/material";
import {HighlightTextPipeModule} from "../../libraries/common/ui/highlight-text/highlight-text-pipe.module";
import {DateSpinnerModule} from "../../libraries/common/ui/date-spinner/date-spinner.module";
import {PreloaderModule} from "../../libraries/common/ui/preloader/preloader.module";
import {NgbTimepickerModule} from "@ng-bootstrap/ng-bootstrap";
import {IngredientsService} from "./ingredients.service";
import {IngredientsListControllerService} from "./ingredients-list/ingredients-list-controller/ingredients-list-controller.service";
import {DisableControlModule} from "../../libraries/common/utils/disable-control/disable-control.module";
import {EmptyAreaModule} from "../../libraries/common/ui/empty-area/empty-area.module";
import {StickyModule} from "../../libraries/common/utils/sticky/sticky.module";
import {UnProgressBarModule} from "../../libraries/common/ui/progress/un-progress-bar.module";
import {PhotoLargeModalModule} from "../../libraries/common/ui/photo-large-modal/photo-large-modal.module";
import {AbstractIngredientModalComponent} from "./abstract-ingredient-modal/abstract-ingredient-modal.component";
import {AbstractIngredientModalModule} from "./abstract-ingredient-modal/abstract-ingredient-modal.module";
import {AbstractIngredientModule} from "./abstract-ingredient/abstract-ingredient-module";
import {AbstractIngredientService} from "./abstract-ingredient/abstract-ingredient.service";
import {AbstractIngredientComponent} from "./abstract-ingredient/abstract-ingredient.component";


@NgModule({
    declarations: [
        IngredientsComponent,
        IngredientsListComponent,
        IngredientsListControllerComponent,
        IngredientsListItemComponent,
        IngredientsViewComponent
    ],
    exports     : [
        IngredientsComponent,
        IngredientsListComponent,
        IngredientsListControllerComponent,
        IngredientsListItemComponent,
        IngredientsViewComponent
    ],
    imports     : [
        CommonModule,
        FormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        HighlightTextPipeModule,
        MatFormFieldModule,
        MatSelectModule,
        ReactiveFormsModule,
        DateSpinnerModule,
        PreloaderModule,
        MatOptionModule,
        NgbTimepickerModule,
        DisableControlModule,
        StickyModule,
        EmptyAreaModule,
        MatIconModule,
        MatTooltipModule,
        MatDialogModule,
        UnProgressBarModule,
        PhotoLargeModalModule,
        AbstractIngredientModalModule,
        AbstractIngredientModule,
        MatCheckboxModule
    ],
    providers: [IngredientsService, IngredientsListControllerService, AbstractIngredientService],
    entryComponents: [ AbstractIngredientModalComponent, AbstractIngredientComponent]
})
export class IngredientsModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: IngredientsModule};
    }
}
