import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {AbstractIngredientModalComponent} from "./abstract-ingredient-modal.component";
import {MatButtonModule, MatDialogModule, MatIconModule} from "@angular/material";
import {AbstractIngredientModule} from "../abstract-ingredient/abstract-ingredient-module";


@NgModule({
    declarations: [AbstractIngredientModalComponent],
    exports: [AbstractIngredientModalComponent],
    imports: [
        CommonModule,
        RouterModule,
        MatDialogModule,
        AbstractIngredientModule,
        MatIconModule,
        MatButtonModule
    ],
    providers: []
})

export class AbstractIngredientModalModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: AbstractIngredientModalModule};
    }
}
