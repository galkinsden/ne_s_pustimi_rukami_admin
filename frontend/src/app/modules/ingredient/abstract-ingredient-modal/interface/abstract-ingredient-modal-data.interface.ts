import {IIngredientFull} from "../../models/ingredient-full-model.model";
import {EAbstractIngredientMode} from "../../abstract-ingredient/enum/abstract-ingredient-mode.enum";

export interface IAbstractIngredientModalData {
    mode: EAbstractIngredientMode,
    ingredientId: string
}