import {Component, Inject, Input, OnDestroy, OnInit} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material";
import 'rxjs/add/operator/finally';
import 'rxjs/add/observable/throw';
import {IAbstractIngredientModalData} from "./interface/abstract-ingredient-modal-data.interface";
import {EAbstractProductMode} from "../../products/abstract-product/enum/abstract-product-mode.enum";
import {EAbstractIngredientMode} from "../abstract-ingredient/enum/abstract-ingredient-mode.enum";

@Component({
    selector: 'abstract-ingredient-modal',
    templateUrl: './abstract-ingredient-modal.component.html',
    styleUrls: ['./abstract-ingredient-modal.component.scss']
})
export class AbstractIngredientModalComponent {

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: IAbstractIngredientModalData,
        public dialogRef: MatDialogRef<AbstractIngredientModalComponent>,
    ) {

    }

    public close(): void {
       this.dialogRef.close();
   }

    public isModeCreate(): boolean {
        return this.data.mode === EAbstractIngredientMode.CREATE;
    }

    public isModeEdit(): boolean {
        return this.data.mode === EAbstractIngredientMode.EDIT;
    }

    public isModeView(): boolean {
        return this.data.mode === EAbstractIngredientMode.VIEW;
    }

}