import {Component, EventEmitter, Input, Output} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {ConnectorErrorModel} from "../../libraries/common/utils/connector-error.model";
import {EPermissionsCode} from "../../libraries/user/permissions/permissions-code.enum";
import {AuthService} from "../../libraries/auth/auth.service";
import {UserService} from "../../libraries/user/user.service";
import {UnSnackBarService} from "../../libraries/common/ui/snackbar/un-snack-bar.service";
import {IAbstractPermissions} from "../../libraries/user/permissions/abstract-permissions.interface";
import {merge} from "rxjs/observable/merge";
import {forkJoin} from "rxjs/observable/forkJoin";
import {IIngredient} from "../ingredient/models/ingredients.model";

@Component({
    selector: 'products',
    templateUrl: './products.component.html',
    styleUrls: ['./products.component.scss']
})
export class ProductsComponent {

    //@Input() public isBasket: boolean = false;
    @Input() public mini: boolean = false;
    @Output() public selectProduct: EventEmitter<string> = new EventEmitter();

    public static PATH: string = 'products';
    public model$: Observable<IAbstractPermissions>;

    public constructor(
        private userService: UserService,
        private authService: AuthService,
        private unSnackBarService: UnSnackBarService
    ) {

    }

    public ngOnInit(): void {
        const userId: string = this.authService.getAuthUser().id;
     /*   this.model$ = forkJoin(
                this.userService.checkPermissionRead(userId, EPermissionsCode.products),
                this.userService.checkPermissionWrite(userId, EPermissionsCode.products)
            )
            .map(([read, write]: [boolean, boolean]) => {
                if (!read) {
                    this.unSnackBarService.openError('Нет доступа для просмотра');
                }
                return {
                    read: read,
                    write: write
                }
            })
            .catch((error: ConnectorErrorModel) => {
                this.unSnackBarService.openError(error.description);
                return Observable.of({
                    read: false,
                    write: false
                });
            })*/
     this.model$ =  Observable.of({
            read: true,
            write: true
        });

    }

    public selectProductEE(id: string): void {
        this.selectProduct.emit(id);
    }
}