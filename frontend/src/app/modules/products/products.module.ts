import {ModuleWithProviders, NgModule} from "@angular/core";
import {ProductsComponent} from "./products.component";
import {ProductsListComponent} from "./products-list/products-list.component";
import {ProductsListControllerComponent} from "./products-list/products-list-controller/products-list-controller.component";
import {ProductsListItemComponent} from "./products-list/products-list-item/products-list-item.component";
import {ProductsViewComponent} from "./products-view/products-view.component";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {
    MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatDialogModule, MatFormFieldModule, MatIconModule,
    MatInputModule,
    MatOptionModule,
    MatRadioModule,
    MatSelectModule, MatTooltipModule
} from "@angular/material";
import {HighlightTextPipeModule} from "../../libraries/common/ui/highlight-text/highlight-text-pipe.module";
import {DateSpinnerModule} from "../../libraries/common/ui/date-spinner/date-spinner.module";
import {PreloaderModule} from "../../libraries/common/ui/preloader/preloader.module";
import {NgbTimepickerModule} from "@ng-bootstrap/ng-bootstrap";
import {ProductsService} from "./products.service";
import {ProductsListControllerService} from "./products-list/products-list-controller/products-list-controller.service";
import {DisableControlModule} from "../../libraries/common/utils/disable-control/disable-control.module";
import {EmptyAreaModule} from "../../libraries/common/ui/empty-area/empty-area.module";
import {StickyModule} from "../../libraries/common/utils/sticky/sticky.module";
import {IngredientsModule} from "../ingredient/ingredients.module";
import {ProductsSelectIngredientModalComponent} from "./products-select-ingredient.component/products-select-ingredient-modal.component";
import {AbstractProductModalComponent} from "./abstract-product-modal/abstract-product-modal.component";
import {AbstractProductService} from "./abstract-product/abstract-product.service";
import {AbstractProductComponent} from "./abstract-product/abstract-product.component";
import {AbstractProductModule} from "./abstract-product/abstract-product.module";
import {AbstractProductModalModule} from "./abstract-product-modal/abstract-product-modal.module";


@NgModule({
    declarations: [
        ProductsComponent,
        ProductsListComponent,
        ProductsListControllerComponent,
        ProductsListItemComponent,
        ProductsViewComponent,
        ProductsSelectIngredientModalComponent
    ],
    exports     : [
        ProductsComponent,
        ProductsListComponent,
        ProductsListControllerComponent,
        ProductsListItemComponent,
        ProductsViewComponent,
        ProductsSelectIngredientModalComponent
    ],
    imports     : [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatDatepickerModule,
        HighlightTextPipeModule,
        MatFormFieldModule,
        MatSelectModule,
        DateSpinnerModule,
        PreloaderModule,
        MatOptionModule,
        NgbTimepickerModule,
        DisableControlModule,
        StickyModule,
        EmptyAreaModule,
        MatIconModule,
        MatRadioModule,
        MatTooltipModule,
        MatDialogModule,
        IngredientsModule,
        AbstractProductModule,
        AbstractProductModalModule,
        MatCheckboxModule
    ],
    providers: [ProductsService, ProductsListControllerService, AbstractProductService],
    entryComponents: [ProductsSelectIngredientModalComponent, AbstractProductModalComponent, AbstractProductComponent]
})
export class ProductsModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: ProductsModule};
    }
}
