import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import * as moment from 'moment';
import {EStatus, ProductModel} from "../../models/products.model";
import {ProductsService} from "../../products.service";
import {AbstractProductService} from "../../abstract-product/abstract-product.service";
import {Subscription} from "rxjs/Subscription";
import {Observable} from "rxjs/Observable";
import {UnSnackBarService} from "../../../../libraries/common/ui/snackbar/un-snack-bar.service";

@Component({
    selector: 'products-list-item',
    templateUrl: './products-list-item.component.html',
    styleUrls: ['./products-list-item.component.scss']
})
export class ProductsListItemComponent {
    @Input() public searchString: string = '';
    @Input() public selected: boolean = false;
    @Input() public model: ProductModel;
    @Input() public groupStatus: EStatus;
    @Input() public searchMode: boolean = false;
    @Input() public disabledIncrementButtons: boolean = false;

    @Output() public updateCountCb: EventEmitter<boolean> = new EventEmitter<boolean>();

    constructor(
        private productsService: ProductsService,
        private abstractProductService: AbstractProductService,
        private unSnackBarService: UnSnackBarService
    ) {}

    public enumStatus = EStatus;

    public getLowerStatus(status: EStatus): string {
        return status ? `is-${status.toLowerCase()}` : '';
    }

    public getFormatDate(date: string): string {
        return moment(date).format('DD.MM.YYYY')
    }

    public addOneProduct($event): void {
        this.updateCountCb.emit(true);
        $event.stopPropagation();
        const sub: Subscription = this.productsService.incrementIngredientsByProductId(this.model.id)
            .map(() => {
                this.updateCountCb.emit(false);
                this.unSnackBarService.openSucces('Количество товара успешно увеличено на 1');
                this.abstractProductService.setProductId(this.model.id);
            })
            .catch(() => {
                this.updateCountCb.emit(false);
                this.unSnackBarService.openSucces('Ошибка увеличения количества товара');
                return Observable.of();
            })
            .subscribe(() => sub && sub.unsubscribe());

    }

    public removeOneProduct($event): void {
        this.updateCountCb.emit(true);
        $event.stopPropagation();
        const sub: Subscription = this.productsService.decrementIngredientsByProductId(this.model.id)
            .map(() => {
                this.updateCountCb.emit(false);
                this.unSnackBarService.openSucces('Количество товара успешно уменьшено на 1');
                this.abstractProductService.setProductId(this.model.id);
            })
            .catch(() => {
                this.updateCountCb.emit(false);
                this.unSnackBarService.openSucces('Ошибка уменьшения количества товара');
                return Observable.of();
            })
            .subscribe(() => sub && sub.unsubscribe());
    }
}