import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Moment} from 'moment';
import {ProductsListControllerModel} from "../../models/product-list-controller.model";

/**
 * Сервис для контроля параметров отображения списков (дата, поисковая строчка, режим поиска)
 */
@Injectable()
export class ProductsListControllerService {
    /**
     * Модель для хранения текущего состояний
     * @type {BehaviorSubject<ProductsListControllerModel>}
     */
    private readonly model: BehaviorSubject<ProductsListControllerModel> = new BehaviorSubject<ProductsListControllerModel>(new ProductsListControllerModel());

    /**
     * Устанавливает поисковую строчку
     * @param {string} searchString
     * @returns {ProductsListControllerService}
     */
    public setSearch(searchString: string): ProductsListControllerService {
        return this.setModel(new ProductsListControllerModel(
            !this.model.getValue().isSearchMode ?  searchString.length > 0 : true,
            searchString
        ));
    }

    /**
     * Сбрасываем режим поиска и поисковую строчку
     * @returns {ProductsListControllerService}
     */
    public resetSearch(): ProductsListControllerService {
        return this.setModel(new ProductsListControllerModel(
            false,
            ''
        ));
    }

    /**
     * Получить наблюдаемую модель
     * @returns {Observable<ProductsListControllerModel>}
     */
    public getObservableModel(): Observable<ProductsListControllerModel> {
        return this.model.asObservable();
    }

    /**
     * Получить актуальную модель
     * @returns {ProductsListControllerModel}
     */
    public getModel(): ProductsListControllerModel {
        return this.model.getValue();
    }

    private setModel(model: ProductsListControllerModel): ProductsListControllerService {
        this.model.next(model);
        return this;
    }

}
