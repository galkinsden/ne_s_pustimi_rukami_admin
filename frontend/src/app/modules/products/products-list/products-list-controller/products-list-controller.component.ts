import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {ProductsService} from "../../products.service";
import {ActivatedRoute, Params} from "@angular/router";
import * as moment from 'moment';
import {Moment} from 'moment';
import {ProductsListControllerService} from "./products-list-controller.service";
import {Observable} from "rxjs/Observable";
import {Subscription} from "rxjs/Subscription";
import {MatDialog} from "@angular/material";
import {ProductsListControllerModel} from "../../models/product-list-controller.model";
import {AbstractProductModalComponent} from "../../abstract-product-modal/abstract-product-modal.component";
import {EAbstractProductMode} from "../../abstract-product/enum/abstract-product-mode.enum";
import {Subject} from "rxjs/Subject";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
    selector: 'products-list-controller',
    templateUrl: './products-list-controller.component.html',
    styleUrls: ['./products-list-controller.component.scss']
})
export class ProductsListControllerComponent /*implements OnInit, OnDestroy*/{

    @Input() public disabledSearch: boolean = false;
    @Input() public write: boolean = false;
    @Input() public read: boolean = false;
    @Input() public isBasket: boolean = false;
    @Input() public mini: boolean = false;

    public formController: FormGroup;
    public keyUp = new Subject<any>();

    private intervalTime: number = 300;

    private subscription: Subscription;
    public $model: Observable<ProductsListControllerModel>;

    constructor(
        private productsListController: ProductsListControllerService,
        private productsService: ProductsService,
        private route: ActivatedRoute,
        private dialog: MatDialog
    ) {
    }

    public ngOnInit(): void {

        this.formController = this.createFormControl();

        this.$model = this.productsListController.getObservableModel()
            .map((model: ProductsListControllerModel) => {
                return model;
            });

        this.subscription = this.keyUp
            .map(event => event.target.value)
            .debounceTime(this.intervalTime)
            .flatMap((search: string) => Observable.of(search))
            .subscribe((searchString: string) => {
                this.onSubmit(searchString);
            });

        this.productsListController.setSearch('');
    }

    public ngOnDestroy(): void {
        this.subscription && this.subscription.unsubscribe();
    }

    public resetSearch(): void {
        this.productsListController.resetSearch();
    }

    public search(value: string): void {
        this.productsListController.setSearch(value);
    }

    public createNew(): void {
        let dialogRef = this.dialog.open(AbstractProductModalComponent, {panelClass: 'un-large-panel-class', data: {mode: EAbstractProductMode.CREATE, productFull: null}});
    }

    public onSubmit(searchString: string): void {
        this.productsListController.setSearch(searchString || this.formController.get('name').value);
    }

    public onSubmitInterval($event): void {
        this.keyUp.next($event);
    }

    private createFormControl(): FormGroup {
        return new FormGroup({
            name: new FormControl('')
        });
    }
}