import {Component, Input, OnInit} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {ActivatedRoute, Params, Router} from "@angular/router";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/last';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/exhaustMap';
import 'rxjs/add/observable/interval';
import {merge} from "rxjs/observable/merge";
import {ProductsListModel} from "../models/product-list.model";
import {Subject} from "rxjs/Subject";
import {ProductsService} from "../products.service";
import {ProductsListControllerService} from "./products-list-controller/products-list-controller.service";
import {ProductsViewModel} from "../models/product-view.model";
import {EStatus, IConfigGroup, IGroup, ProductModel} from "../models/products.model";
import {ProductsListControllerModel} from "../models/product-list-controller.model";
import {EStateModel} from "../../../libraries/common/utils/state/state-model.enum";
import {ProductsListItemModel} from "../models/product-list-item.model";
import {AuthService} from "../../../libraries/auth/auth.service";
import {ConnectorErrorModel} from "../../../libraries/common/utils/connector-error.model";
import {AbstractProductService} from "../abstract-product/abstract-product.service";
import * as moment from 'moment';

interface IProductsListResponse {
    state: EStateModel;
    list: ProductModel[];
    selectedId: string;
    searchString: string;
}

@Component({
    selector: 'products-list',
    templateUrl: './products-list.component.html',
    styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit {

    @Input() public write: boolean = false;
    @Input() public read: boolean = false;
    @Input() public isBasket: boolean = false;
    @Input() public mini: boolean = false;

    public $model: Observable<ProductsListModel>;
    private _clickSelectedProduct$: Subject<string> = new Subject();

    public isUpdateProductCount: boolean = false;
    /**
     * Время обновления списка
     * @type {number}
     */
    protected readonly intervalTime: number = 1000 * 60 * 10;

    public constructor(
        private productsService: ProductsService,
        private router: Router,
        private route: ActivatedRoute,
        private productsListControllerService: ProductsListControllerService,
        private abstractProductService: AbstractProductService
    ) {

    }

    public ngOnInit(): void {
        this.$model = merge(this.getProductsStream(), this.getAutoUpdateStream())
            .combineLatest(this.getSelectedIdNumberStream())
            .map(([productListResponse, selectedId] : [IProductsListResponse, string]) => {
                return new ProductsListModel(
                    this.groupProducts(productListResponse && productListResponse.list || []),
                    productListResponse && productListResponse.list && productListResponse.list.length || 0,
                    productListResponse && productListResponse.list && selectedId ? productListResponse.list.find((product: ProductModel) => product.id === selectedId) :  null,
                    productListResponse && productListResponse.state || EStateModel.LOADED,
                    productListResponse && productListResponse.searchString || ''
                )
            })
            .catch(() => {
                return Observable.of(new ProductsListModel(
                    this.groupProducts([]),
                    0,
                    null,
                    EStateModel.ERROR,
                    ''
                ));
            })
            .do((model: ProductsListModel) => {
                this.productsService.countProducts.next(model.total);
                this.productsService.selectedProduct.next(model.selected);
                this.router.navigate(['./'], {
                    relativeTo: this.route,
                    replaceUrl: true,
                    queryParams: {
                        id: model.selected ? model.selected.id : null
                    }
                });
            })
    }

 /*   private getSelectedModel(productsListResponse: IProductsListResponse, selectedIdFromUrl: string): ProductModel {
        if (productsListResponse && productsListResponse.list) {
            if (productsListResponse.selectedId && productsListResponse.list.find((product: ProductModel) => product.id === productsListResponse.selectedId)) {
                return productsListResponse.list.find((product: ProductModel) => product.id === productsListResponse.selectedId);
            } else if (selectedIdFromUrl) {
                const productModel: ProductModel = productsListResponse.list.find((product: ProductModel) => product.id === selectedIdFromUrl);
                return productModel ? productModel : null;
            }
        } else {
            return null;
        }
    }*/

    private getAutoUpdateStream(): Observable<IProductsListResponse> {
        return Observable
            .interval(this.intervalTime)
            .exhaustMap(() => this.productsService.getAll('')
                .map((res: ProductModel[]) => ({state: EStateModel.LOADED, list: res}))
                .catch((err: ConnectorErrorModel) => Observable.throw({state: EStateModel.ERROR, list: []}))
            )
    }

    public onClickToItem(item: ProductsListItemModel): void {
        this._clickSelectedProduct$.next(item ? item.id : null);
    }

    public updateCountCb(isUpdateProductCount: boolean): void {
        this.isUpdateProductCount = isUpdateProductCount;
       // this.abstractProductService.setProductId(null);
    }

    private getSelectedIdNumberStream(): Observable<string> {
        return this.route.queryParams
            .first()
            .map((params: Params): string => {
                const result: string = params['id'];
                return result || null;
            })
            .merge(this._clickSelectedProduct$);
    }

    /**
     * Группировка записей по типу и их сортировка
     * @returns {IGroup[]}
     */
    private groupProducts(list: ProductModel[]): IGroup[] {
        const result: IGroup[] = ProductsService
            .getGroups()
            .map((group: IGroup) => ({...group, list: []}));

        list.forEach((item: ProductModel) => {
            const config: IConfigGroup = ProductsService.getGroupTypeByProductStatus(item.status);
            const group: IGroup = result.find((g: IGroup) => g.status === config.status);
            if (group) {
                group.list.push(item);
            }
        });

        return result
            .sort((a: IGroup, b: IGroup) => a.weight - b.weight)
            .map((g: IGroup) => {
                g.list = g.list
                    .sort((a: ProductModel, b: ProductModel) => {
                        return a.date && b.date
                            ? moment(b.date).valueOf() - moment(a.date).valueOf()
                                || a.name.replace(/\s/g, '').localeCompare(b.name.replace(/\s/g, ''), undefined, {numeric: true})
                            : 0
                    })
                return g;
            });
    }

    public getLowerStatus(status: EStatus): string {
        return status ? `is-${(status + '').toLowerCase()}` : '';
    }

    private getProductsStream(): Observable<IProductsListResponse> {
        return this.productsListControllerService.getObservableModel()
            .combineLatest(this.abstractProductService.getObservableModel())
            .switchMap(([productsListControllerModel, productId]: [ProductsListControllerModel, string]) => {
                return this.productsService.getAll(productsListControllerModel && productsListControllerModel.searchString || '')
                    .map((res: ProductModel[]) => ({
                        state: EStateModel.LOADED,
                        list: res,
                        selectedId: productId,
                        searchString: productsListControllerModel && productsListControllerModel.searchString || ''
                    }))
                    .catch((err: ConnectorErrorModel) => Observable.throw({state: EStateModel.ERROR, list: [], selectedId: null, searchString: ''}))
                    .startWith({state: EStateModel.LOADING, list: [], selectedId: null, searchString: ''})
            });
    }
}