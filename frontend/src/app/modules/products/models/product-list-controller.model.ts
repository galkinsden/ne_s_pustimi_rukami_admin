/**
 * Модель состояния контроллера отвечающего за отображением списка
 */
export class ProductsListControllerModel {
	constructor(
		public readonly isSearchMode: boolean = false,
		public readonly searchString: string = '',
	) {}
}
