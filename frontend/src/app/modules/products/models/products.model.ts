import {IProductsListItem} from "./product-list-item.model";
import * as uuidv1 from 'uuid/v1';
import {IProductResponse} from "../connectors/models/response/product-response.model";

export enum EStatus {
    DELETED_INGREDIENT = 'DELETED_INGREDIENT',
    AVAILABLE = 'AVAILABLE',
    NOT_AVAILABLE = 'NOT_AVAILABLE',
    OUTSIDE = 'OUTSIDE'
}

/**
 * Конфигурация группы
 */
export interface IConfigGroup {
    status: EStatus;
    title: string; // русское название
    weight: number; // вес для сортировки группы
}

/**
 * Группа и ее список записей
 */
export interface IGroup extends IConfigGroup {
    list: IProductsListItem[];
}

export interface IProduct {
    id: string;
    name: string;
    priceOld: number;
    priceCurrent: number;
    date: string;
    count: number;
    on: boolean;
}

export class ProductModel implements IProduct {

    public readonly id: string;
    public readonly name: string;
    public readonly priceOld: number;
    public readonly priceCurrent: number;
    public readonly date: string;
    public readonly count: number;
    public readonly on: boolean;
    public readonly status: EStatus;//считается динамически
    //public readonly isNoAnswerQuestions: boolean;

    public constructor(src: IProduct) {
        this.id = src && src.id || uuidv1();
        this.name = src && src.name || '';
        this.count = src && src.count || 0;
        this.priceOld = src && src.priceOld || 0;
        this.priceCurrent = src && src.priceCurrent || 0;
        this.date = src && src.date || '';
        this.on = src && src.on;
        this.status = this.getStatusProduct(src && src.count);
    }

    public getStatusProduct(count: number): EStatus {
        if (count) {
            return EStatus.AVAILABLE;
        } else if (count === null){
            return EStatus.DELETED_INGREDIENT;
        } else {
            return EStatus.NOT_AVAILABLE;
        }
    }
}