import {IIngredientFull, IngredientFullModel} from "../../ingredient/models/ingredient-full-model.model";

export interface IProductIngredientFull {
    count: number;
    ingredient: IIngredientFull;
}

export class ProductIngredientFullModel {

    public readonly count: number;
    public ingredient: IngredientFullModel;

    public constructor(src: IProductIngredientFull) {
        this.count = src && src.count || null;
        this.ingredient = src && src.ingredient ? new IngredientFullModel(src.ingredient) : null;
    }

}