import {StateModel} from "../../../libraries/common/utils/state/state.model";
import {EStateModel} from "../../../libraries/common/utils/state/state-model.enum";
import {ProductFullModel} from "./product-full-model.model";
import {EAbstractProductMode} from "../abstract-product/enum/abstract-product-mode.enum";

export interface IProductView {
    productId: string;
    state: EStateModel;
    mode: EAbstractProductMode;
}

export class ProductsViewModel extends StateModel implements IProductView {

    public constructor(
        public readonly productId: string = '',
        public readonly state: EStateModel = EStateModel.NONE,
        public readonly mode: EAbstractProductMode = EAbstractProductMode.VIEW
    ) {
        super(state);
    }
}