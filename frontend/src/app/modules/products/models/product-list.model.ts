import {StateModel} from "../../../libraries/common/utils/state/state.model";
import {EStateModel} from "../../../libraries/common/utils/state/state-model.enum";
import {IGroup} from "./products.model";
import {ProductsListItemModel} from "./product-list-item.model";

export class ProductsListModel extends StateModel {

    public constructor(
        public readonly groups: IGroup[] = [],
        public readonly total: number = 0,
        public readonly selected: ProductsListItemModel = null,
        public readonly state: EStateModel = EStateModel.NONE,
        public readonly searchString: string = ''
    ) {
        super(state);
    }
}