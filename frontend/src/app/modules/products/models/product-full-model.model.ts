import {EStatus, IProduct, ProductModel} from "./products.model";
import {EProductType} from "./product-type.enum";
import {IngredientFullModel} from "../../ingredient/models/ingredient-full-model.model";
import {IProductIngredientFull, ProductIngredientFullModel} from "./product-ingredient-full.model";
import {CollectionModel, ICollection} from "../../collections/models/collection.model";

export interface IProductFull extends IProduct {
    additional: IProductIngredientFull[];  //цветы, открытка id ингридентов дополнительных
    ingredients: IProductIngredientFull[];
    description: string;
    categories: string[]; // массив названий категорий товара
    collections: string[];
    type: EProductType[];
    alco: boolean; // есть ли в наборе элемент с алкоголем
}

export class ProductFullModel extends ProductModel implements IProductFull {

    public readonly ingredients: ProductIngredientFullModel[];
    public readonly additional: ProductIngredientFullModel[];
    public readonly categories: string[];
    public readonly collections: string[];
    public readonly photos: string[];
    public readonly description: string;
    public readonly alco: boolean;
    public readonly type: EProductType[];

    public constructor(src: IProductFull) {
        super(src);
        this.description = src && src.description || '';
        this.ingredients = src && Array.isArray(src.ingredients) ? src.ingredients.map((ing: IProductIngredientFull) => new ProductIngredientFullModel(ing)) : [];
        this.additional = src && Array.isArray(src.additional) ? src.additional.map((ing: IProductIngredientFull) => new ProductIngredientFullModel(ing)) : [];
        this.categories = src && Array.isArray(src.categories) ? src.categories : [];
        this.collections = src && Array.isArray(src.collections) ? src.collections : [];
        this.alco = src && src.alco;
        this.type = src && src.type ? src.type : [];
    }
}