export interface IProductIngredient {
    count: number;
    id: string;
}

export class ProductIngredientModel {

    public readonly count: number;
    public readonly id: string;

    public constructor(src: IProductIngredient) {
        this.count = src && src.count || null;
        this.id = src && src.id || '';
    }

}