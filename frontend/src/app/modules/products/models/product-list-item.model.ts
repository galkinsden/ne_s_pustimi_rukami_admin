export interface IProductsListItem {
    id: string;
    name: string;
}

export class ProductsListItemModel implements IProductsListItem {

    public constructor(
        public readonly id: string = '',
        public readonly name: string = '',
    ) {

    }
}