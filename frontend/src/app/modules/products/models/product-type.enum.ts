export enum EProductType {
    MAN = 'MAN',
    WOMAN = 'WOMAN',
    CHILD = 'CHILD',
    CORPORATE = 'CORPORATE'
}