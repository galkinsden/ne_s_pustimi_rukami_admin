import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatFormFieldModule, MatInputModule, MatSelectModule, MatTooltipModule} from "@angular/material";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DisableControlModule} from "../../../libraries/common/utils/disable-control/disable-control.module";
import {PreloaderModule} from "../../../libraries/common/ui/preloader/preloader.module";
import {EmptyAreaModule} from "../../../libraries/common/ui/empty-area/empty-area.module";
import {UnSnackBarModule} from "../../../libraries/common/ui/snackbar/un-snack-bar.module";
import {ProductsTypeComponent} from "./products-type.component";


@NgModule({
    declarations: [ProductsTypeComponent],
    exports     : [ProductsTypeComponent],
    imports     : [
        MatInputModule,
        MatFormFieldModule,
        FormsModule,
        ReactiveFormsModule,
        DisableControlModule,
        PreloaderModule,
        EmptyAreaModule,
        CommonModule,
        MatSelectModule,
        MatTooltipModule,
        UnSnackBarModule
    ],
    providers   : []
})

export class ProductsTypeModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: ProductsTypeModule};
    }
}
