import {Component, Input, OnChanges} from '@angular/core';
import {FormArray, FormControl} from "@angular/forms";
import {EAbstractProductMode} from "../abstract-product/enum/abstract-product-mode.enum";
import {EProductType} from "../models/product-type.enum";
import {MatSelectChange} from "@angular/material";

@Component({
    selector: 'products-type',
    templateUrl: 'products-type.component.html',
    styleUrls: ['./products-type.component.scss']
})
export class ProductsTypeComponent implements OnChanges {

    @Input() public formProductsType: FormArray = new FormArray([]);
    @Input() public productId: string;
    @Input() public mode: EAbstractProductMode = EAbstractProductMode.VIEW;
    @Input() public productTypes: EProductType[] = [];

    public formSelectControl: FormControl = new FormControl();
    public EProductType: string[] = Object.keys(EProductType);

    public ngOnChanges() {
        this.init();
    }

    private init(): void {
        this.clearFormArray(this.formProductsType);
        this.productTypes.forEach((type: EProductType) => {
            this.formProductsType.push(this.createProductType(type));
        });
        this.formSelectControl = new FormControl(this.productTypes);
    }

    public isModeCreate(): boolean {
        return this.mode === EAbstractProductMode.CREATE;
    }

    public isModeEdit(): boolean {
        return this.mode === EAbstractProductMode.EDIT;
    }

    public isModeView(): boolean {
        return this.mode === EAbstractProductMode.VIEW;
    }

    public clearFormArray(formArray: FormArray): void {
        while (formArray.length !== 0) {
            formArray.removeAt(0)
        }
    }

    public selectionChange($event: MatSelectChange): void {
        this.clearFormArray(this.formProductsType);
        $event && $event.value && $event.value.forEach((type: EProductType) => {
            this.formProductsType.push(this.createProductType(type))
        })
    }

    public createProductType(type: EProductType): FormControl {
        return new FormControl(type || null)
    }

    public getConvertProductTypeEnum(type: EProductType): string {
        switch (type) {
            case EProductType.MAN:
                return 'Мужской';
            case EProductType.WOMAN:
                return 'Женский';
            case EProductType.CHILD:
                return 'Детский';
            case EProductType.CORPORATE:
                return 'Корпоративный';
            default:
                return 'Мужские';
        }
    }
}