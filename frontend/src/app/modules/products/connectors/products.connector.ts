import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {ConnectorErrorModel, IError} from '../../../libraries/common/utils/connector-error.model';
import {appConfig} from '../../../app.config';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {IProduct} from "../models/products.model";
import {IProductFull} from "../models/product-full-model.model";
import {IProductFullResponse} from "./models/response/product-full-response.model";
import {IProductResponse} from "./models/response/product-response.model";
import {IProductFullRequest} from "./models/request/product-full-request.model";

export interface IProductsConnectorGetAllResponse {
    data: IProductResponse[];
    error: IError;
}

export interface IProductsConnectorGetIntredientFullByIdResponse {
    data: IProductFullResponse;
    error: IError;
}

export interface IProductsConnectorGetProductsByIdsResponse {
    data: IProductResponse[];
    error: IError;
}

export interface IProductsConnectorGetProductsFullByIdsResponse {
    data: IProductFullResponse[];
    error: IError;
}

export interface IProductsConnectorUpdateProductResponse {
    data: string;
    error: IError;
}

export interface IProductsConnectorRemoveProductResponse {
    data: boolean;
    error: IError;
}

export interface IProductsConnectorInsertProductResponse {
    data: string;
    error: IError;
}

export interface IProductsConnectorRemovePhotosResponse {
    data: boolean;
    error: IError;
}

export interface IProductsConnectorGetPhotosResponse {
    data: string[];
    error: IError;
}

export interface IProductsConnectorIncrementIngredientsByProductIdResponse {
    data: boolean;
    error: IError;
}

export interface IProductsConnectorDecrementIngredientsByProductIdResponse {
    data: boolean;
    error: IError;
}

export class ProductsConnector {

    constructor(private http: HttpClient) {}

    public getAll(searchString: string): Observable<IProductResponse[] | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/products/getAll`, {searchString: searchString})
            .map((res: IProductsConnectorGetAllResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public getFullProductById(productId: string): Observable<IProductFullResponse | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/products/getProductFullById`, {productId: productId})
            .map((res: IProductsConnectorGetIntredientFullByIdResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public getPhotos(productId: string): Observable<string[] | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/products/getPhotos`, {productId: productId})
            .map((res: IProductsConnectorGetPhotosResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public insertProduct(product: IProductFullRequest): Observable<string | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/products/setInsertProduct`, {product: product})
            .map((res: IProductsConnectorInsertProductResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public updateProduct(product: IProductFullRequest): Observable<string | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/products/setUpdateProduct`, {product: product})
            .map((res: IProductsConnectorUpdateProductResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public removePhotos(productId: string, productFileNames: string[]): Observable<boolean | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/products/setRemovePhotos`, {productId: productId, productFileNames: productFileNames})
            .map((res: IProductsConnectorRemovePhotosResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public removeProductById(productId: string): Observable<boolean | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/products/setRemoveProduct`, {productId: productId})
            .map((res: IProductsConnectorRemoveProductResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public getProductsByIds(ids: string[]): Observable<IProductResponse[] | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/products/getProductsByIds`, {ids: ids})
            .map((res: IProductsConnectorGetProductsByIdsResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public getProductsFullByIds(ids: string[]): Observable<IProductFullResponse[] | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/products/getProductsFullByIds`, {ids: ids})
            .map((res: IProductsConnectorGetProductsFullByIdsResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public incrementIngredientsByProductId(id: string): Observable<boolean | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/products/setIncrementIngredientsByProductId`, {productId: id})
            .map((res: IProductsConnectorIncrementIngredientsByProductIdResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public decrementIngredientsByProductId(id: string): Observable<boolean | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/products/setDecrementIngredientsByProductId`, {productId: id})
            .map((res: IProductsConnectorDecrementIngredientsByProductIdResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    private handleError(res: IError): Observable<ConnectorErrorModel> {
        return Observable.throw(new ConnectorErrorModel(res));
    }
}