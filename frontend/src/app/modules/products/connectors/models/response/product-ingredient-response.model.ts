import {IIngredientFullResponse} from "../../../../ingredient/connectors/models/response/ingredient-full-response.model";

export interface IProductIngredientResponse {
    count: number;
    ingredient: IIngredientFullResponse;
}