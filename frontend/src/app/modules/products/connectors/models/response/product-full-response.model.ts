import {IProductResponse} from "./product-response.model";
import {EProductType} from "../../../models/product-type.enum";
import {IProductIngredientResponse} from "./product-ingredient-response.model";
import {ICollection} from "../../../../collections/models/collection.model";

export interface IProductFullResponse extends IProductResponse {
    additional: IProductIngredientResponse[]; //цветы, открытка id ингридентов дополнительных
    photos: string[]; //хранится не в бд
    ingredients: IProductIngredientResponse[];
    description: string;
    categories: string[]; // массив названий категорий товара
    collections: string[];
    type: EProductType[];
    alco: boolean; //есть ли в наборе алкоголь
    on: boolean; // Отображать ли на сайте
}