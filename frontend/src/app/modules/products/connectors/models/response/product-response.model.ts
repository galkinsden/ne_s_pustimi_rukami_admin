export interface IProductResponse {
    id: string;
    name: string; //название товара
    priceOld: number; // старая цена
    priceCurrent: number; //текущая цена
    date: string; // дата добавления
    count: number; //число товаров
    on: boolean;
}