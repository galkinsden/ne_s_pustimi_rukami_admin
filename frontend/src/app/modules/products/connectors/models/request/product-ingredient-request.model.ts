export interface IProductIngredientRequest {
    id: string;
    count: number;
}