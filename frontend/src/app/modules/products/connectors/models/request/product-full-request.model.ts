import {EProductType} from "../../../models/product-type.enum";
import {IProductIngredientRequest} from "./product-ingredient-request.model";

export interface IProductFullRequest {
    id: string;
    name: string; //название товара
    priceOld: number; // старая цена
    priceCurrent: number; //текущая цена
    date: string; // дата добавления
    additional: string[]; //цветы, открытка id ингридентов дополнительных
    ingredients: IProductIngredientRequest[];
    description: string;
    categories: string[]; // массив названий категорий товара
    collections: string[];
    type: EProductType[];
    on: boolean; //Отображать ли на сайте
}