import {Injectable} from "@angular/core";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {ProductsListItemModel} from "./models/product-list-item.model";
import {EStatus, IConfigGroup, IProduct, ProductModel} from "./models/products.model";
import {ConnectorErrorModel} from "../../libraries/common/utils/connector-error.model";
import {Observable} from "rxjs/Observable";
import {HttpClient} from "@angular/common/http";
import {ProductsConnector} from "./connectors/products.connector";
import {IProductFull, ProductFullModel} from "./models/product-full-model.model";
import {IProductFullRequest} from "./connectors/models/request/product-full-request.model";
import {IProductFullResponse} from "./connectors/models/response/product-full-response.model";
import {IProductResponse} from "./connectors/models/response/product-response.model";
import {IngredientsService} from "../ingredient/ingredients.service";


@Injectable()
export class ProductsService {

    private productsConnector: ProductsConnector;

    constructor(http: HttpClient, private ingredientsService: IngredientsService) {
        this.productsConnector = new ProductsConnector(http);
    }

    public readonly countProducts: BehaviorSubject<number> = new BehaviorSubject(null);

    public readonly selectedProduct: BehaviorSubject<ProductsListItemModel> = new BehaviorSubject(null);
    
    public getAll(searchString: string): Observable<ProductModel[] | ConnectorErrorModel> {
        return this.productsConnector.getAll(searchString)
            .map((res: IProductResponse[]) => {
                return res && res.length ? res.map((r: IProductResponse) => new ProductModel(r)) : [];
            });
    }

    public getFullProductById(productId: string): Observable<IProductFullResponse | ConnectorErrorModel> {
        return this.productsConnector.getFullProductById(productId);
    }

    public getPhotos(productId: string): Observable<string[] | ConnectorErrorModel> {
        return this.productsConnector.getPhotos(productId);
    }

    public insertProduct(product: IProductFullRequest): Observable<string> {
        return this.productsConnector.insertProduct(product)
            .map((res: string) => res)
    }

    public removeProductById(productId: string): Observable<boolean> {
        return this.productsConnector.removeProductById(productId)
            .map((res: boolean) => res)
    }

    public removePhotos(productId: string, productFileNames: string[]): Observable<boolean> {
        return this.productsConnector.removePhotos(productId, productFileNames)
            .map((res: boolean) => res)
    }

    public updateProduct(product: IProductFullRequest): Observable<string> {
        return this.productsConnector.updateProduct(product)
            .map((res: string) => res)
    }

    public getProductsByIds(ids: string[]): Observable<ProductModel[]> {
        return this.productsConnector.getProductsByIds(ids)
            .map((res: IProductResponse[]) => {
                return res && res.length ? res.map((r: IProductResponse) => new ProductModel(r)) : [];
            });
    }

    public getProductsFullByIds(ids: string[]): Observable<ProductFullModel[]> {
        return this.productsConnector.getProductsFullByIds(ids)
            .map((res: IProductFullResponse[]) => {
                return res && res.length ? res.map((r: IProductFullResponse) => new ProductFullModel(r)) : [];
            });
    }

    public incrementIngredientsByProductId(id: string): Observable<boolean> {
        return this.productsConnector.incrementIngredientsByProductId(id)
            .map((r: boolean) => r);
    }

    public decrementIngredientsByProductId(id: string): Observable<boolean> {
        return this.productsConnector.decrementIngredientsByProductId(id)
            .map((r: boolean) => r);
    }

    public getStatusProduct(r: IProductResponse): EStatus {
        if (r.count) {
            return EStatus.AVAILABLE;
        } else if (r.count === null){
            return EStatus.DELETED_INGREDIENT;
        } else {
            return EStatus.NOT_AVAILABLE;
        }
    }

    /**
     * Получаем список групп (коллекция Map)
     * @returns {Array<IConfigGroup>}
     */
    public static getGroups(): IConfigGroup[] {
        return Array.from(configGroup.values());
    }

    public static getGroupTypeByProductStatus(status: EStatus): IConfigGroup {
        switch (status) {
            case EStatus.DELETED_INGREDIENT:
                return configGroup.get(EStatus.DELETED_INGREDIENT);
            case EStatus.AVAILABLE:
                return configGroup.get(EStatus.AVAILABLE);
            case EStatus.NOT_AVAILABLE:
                return configGroup.get(EStatus.NOT_AVAILABLE);
            default:
                return configGroup.get(EStatus.OUTSIDE);
        }
    }

}

export const configGroup: Map<EStatus, IConfigGroup> = new Map([
    [EStatus.DELETED_INGREDIENT,    {status: EStatus.DELETED_INGREDIENT, title: 'Проверьте ингредиенты', weight: 0}],
    [EStatus.AVAILABLE,    {status: EStatus.AVAILABLE, title: 'В наличии', weight: 1}],
    [EStatus.NOT_AVAILABLE,     {status: EStatus.NOT_AVAILABLE, title: 'Нет в наличии', weight: 2}]
]);