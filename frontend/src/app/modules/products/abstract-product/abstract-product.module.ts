import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {AbstractProductComponent} from "./abstract-product.component";
import {
    MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatDialogModule, MatFormFieldModule, MatIconModule,
    MatInputModule, MatRadioModule,
    MatTooltipModule
} from "@angular/material";
import {UnSnackBarModule} from "../../../libraries/common/ui/snackbar/un-snack-bar.module";
import {UnProgressBarModule} from "../../../libraries/common/ui/progress/un-progress-bar.module";
import {PhotoLargeModalModule} from "../../../libraries/common/ui/photo-large-modal/photo-large-modal.module";
import {PhotoSmallModule} from "../../../libraries/common/ui/photo-small/photo-small.module";
import {ImageUploaderModule} from "../../../libraries/common/ui/image-uploader/image-uploader.module";
import {PreloaderModule} from "../../../libraries/common/ui/preloader/preloader.module";
import {SecureModule} from "../../../libraries/common/ui/img-secure/secure.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DisableControlModule} from "../../../libraries/common/utils/disable-control/disable-control.module";
import {EmptyAreaModule} from "../../../libraries/common/ui/empty-area/empty-area.module";
import {RatingModule} from "../../rating/rating.module";
import {ReviewModule} from "../../review/review.module";
import {CollectionsModule} from "../../collections/collections.module";
import {QuestionModule} from "../../question/question.module";
import {CategoriesModule} from "../../categories/collections.module";
import {ProductsIngredientsModule} from "../products-intredients/products-ingredients.module";
import {ProductsTypeModule} from "../products-type/products-type.module";


@NgModule({
    declarations: [AbstractProductComponent],
    exports: [AbstractProductComponent],
    imports: [
        CommonModule,
        RouterModule,
        MatDialogModule,
        UnSnackBarModule,
        MatIconModule,
        UnProgressBarModule,
        MatFormFieldModule,
        PhotoSmallModule,
        PhotoLargeModalModule,
        ImageUploaderModule,
        PreloaderModule,
        SecureModule,
        FormsModule,
        ReactiveFormsModule,
        DisableControlModule,
        MatInputModule,
        MatButtonModule,
        MatTooltipModule,
        MatDatepickerModule,
        MatCheckboxModule,
        EmptyAreaModule,
        ReviewModule,
        RatingModule,
        CollectionsModule,
        CategoriesModule,
        MatRadioModule,
        QuestionModule,
        ProductsIngredientsModule,
        ProductsTypeModule
    ],
    providers: []
})

export class AbstractProductModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: AbstractProductModule};
    }
}
