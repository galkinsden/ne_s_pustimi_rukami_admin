export enum EAbstractProductMode {
    CREATE = 'CREATE',
    EDIT = 'EDIT',
    VIEW = 'VIEW'
}