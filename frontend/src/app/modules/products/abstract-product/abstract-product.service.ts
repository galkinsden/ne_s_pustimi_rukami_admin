import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class AbstractProductService {

    private readonly model: BehaviorSubject<string> = new BehaviorSubject<string>('');

    public setProductId(productId: string): AbstractProductService {
        return this.setModel(productId)
    }

    /**
     * Получить наблюдаемую модель
     * @returns {Observable<ProductFullModel>}
     */
    public getObservableModel(): Observable<string> {
        return this.model.asObservable();
    }

    /**
     * Получить актуальную модель
     * @returns {ProductFullModel}
     */
    public getModel(): string {
        return this.model.getValue();
    }

    private setModel(model: string): AbstractProductService {
        this.model.next(model);
        return this;
    }

}
