import {StateModel} from "../../../../libraries/common/utils/state/state.model";
import {EStateModel} from "../../../../libraries/common/utils/state/state-model.enum";
export interface IAbstractProduct {
    productFullState: StateModel;
    productFullPhotosState: StateModel;
}

export class AbstractProductModel implements IAbstractProduct {

    public constructor(
        public productFullState: StateModel = new StateModel(EStateModel.NONE),
        public productFullPhotosState: StateModel = new StateModel(EStateModel.NONE)
    ) {

    }
}