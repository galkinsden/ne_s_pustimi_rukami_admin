import {StateModel} from "../../../../libraries/common/utils/state/state.model";
import {ProductFullModel} from "../../models/product-full-model.model";
import {EStateModel} from "../../../../libraries/common/utils/state/state-model.enum";
export interface IAbstractProductFull {
    product: ProductFullModel;
    state: EStateModel;
}

export class AbstractProductFullModel extends StateModel implements IAbstractProductFull {

    public constructor(
        public readonly product: ProductFullModel = null,
        public readonly state: EStateModel = EStateModel.NONE
    ) {
        super(state);
    }
}