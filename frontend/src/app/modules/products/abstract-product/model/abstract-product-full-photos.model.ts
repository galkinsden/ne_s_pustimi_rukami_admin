import {StateModel} from "../../../../libraries/common/utils/state/state.model";
import {EStateModel} from "../../../../libraries/common/utils/state/state-model.enum";
export interface IAbstractProductFullPhotos {
    photos: string[];
    state: EStateModel;
}

export class AbstractProductFullPhotosModel extends StateModel implements IAbstractProductFullPhotos {

    public constructor(
        public readonly photos: string[] = [],
        public readonly state: EStateModel = EStateModel.NONE
    ) {
        super(state);
    }
}