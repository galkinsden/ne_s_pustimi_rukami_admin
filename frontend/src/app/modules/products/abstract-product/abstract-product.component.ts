import {
    Component, EventEmitter, Inject, Input, OnChanges, OnDestroy, OnInit, Output,
    SimpleChanges
} from "@angular/core";
import {FormControl, FormGroup, Validators, FormArray, AbstractControl} from "@angular/forms";
import * as moment from 'moment';
import {FileUploader} from "ng2-file-upload";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/finally';
import 'rxjs/add/observable/throw';
import {StateModel} from "../../../libraries/common/utils/state/state.model";
import {appConfig} from "../../../app.config";
import {ProductsService} from "../products.service";
import {UnSnackBarService} from "../../../libraries/common/ui/snackbar/un-snack-bar.service";
import {AuthService} from "../../../libraries/auth/auth.service";
import {EStateModel} from "../../../libraries/common/utils/state/state-model.enum";
import {Subscription} from "rxjs/Subscription";
import {ProductsSelectIngredientModalComponent} from "../products-select-ingredient.component/products-select-ingredient-modal.component";
import {IProductIngredientFull, ProductIngredientFullModel} from "../models/product-ingredient-full.model";
import {IIngredientFull, IngredientFullModel} from "../../ingredient/models/ingredient-full-model.model";
import {IProductFullRequest} from "../connectors/models/request/product-full-request.model";
import {IProductFull, ProductFullModel} from "../models/product-full-model.model";
import {ConnectorErrorModel} from "../../../libraries/common/utils/connector-error.model";
import {AbstractProductService} from "./abstract-product.service";
import {EAbstractProductMode} from "./enum/abstract-product-mode.enum";
import {MatDialog, MatDialogRef} from "@angular/material";
import {AbstractProductModalComponent} from "../abstract-product-modal/abstract-product-modal.component";
import {Router} from "@angular/router";
import {IProductFullResponse} from "../connectors/models/response/product-full-response.model";
import {RatingService} from "../../rating/rating.service";
import {IRating, RatingModel} from "../../rating/models/rating.model";
import {AbstractProductFullModel} from "./model/abstract-product-full.model";
import {AbstractProductFullPhotosModel} from "./model/abstract-product-full-photos.model";
import {ImageUploaderService} from "../../../libraries/common/ui/image-uploader/image-uploader.service";
import {IReview, ReviewModel} from "../../review/models/review.model";
import {ReviewService} from "../../review/review.service";
import {ISubmitData, ReviewCacheService} from "../../review/review-cache.service";
import {IngredientsService} from "../../ingredient/ingredients.service";
import {IIngredientFullResponse} from "../../ingredient/connectors/models/response/ingredient-full-response.model";
import {QuestionCacheService} from "../../question/cache/question-cache.service";
import {IQuestionWithAnswer, QuestionWithAnswerModel} from "../../question/models/question-with-answers.model";
import {AppHttpInterceptor} from "../../../connectors/app-http.interceptor";
import {Subject} from "rxjs/Subject";
@Component({
    selector: 'abstract-product',
    templateUrl: './abstract-product.component.html',
    styleUrls: ['./abstract-product.component.scss']
})
export class AbstractProductComponent extends StateModel implements OnInit, OnChanges {

    @Input() public mode: EAbstractProductMode = null;
    @Input() public dialogRef: MatDialogRef<AbstractProductModalComponent>;
    @Input() public productId: string = '';
    @Input() public mini: boolean = false;

    @Output() public selectProduct: EventEmitter<string> = new EventEmitter();

    public productForm: FormGroup;
    public uploader: FileUploader;
    public apiUrl: string = appConfig.apiUrl;
    public cachedPhotos: FormArray = new FormArray([]);

    public abstractProductFullModel$: Observable<AbstractProductFullModel>;
    public abstractProductFullPhotosModel$: Observable<AbstractProductFullPhotosModel>;

    constructor(
        private productsService: ProductsService,
        private unSnackBarService: UnSnackBarService,
        private abstractProductService: AbstractProductService,
        private authService: AuthService,
        private dialog: MatDialog,
        private router: Router,
        private ratingService: RatingService,
        private reviewService: ReviewService,
        private imageUploaderService: ImageUploaderService,
        private reviewCacheService: ReviewCacheService,
        private ingredientsService: IngredientsService,
        private questionCacheService: QuestionCacheService,
        private appHttpInterceptor: AppHttpInterceptor
    ) {
        super(EStateModel.NONE);
    }

    public ngOnInit(): void {
        if (!this.productForm) {
            this.initProductForm();
        }
    }

    public ngOnChanges(simpleChanges: SimpleChanges) {
        if (!this.productForm) {
            this.initProductForm();
        }
        this.changesProductIdStream(this.productId)
    }

    private initProductForm(): void {
        this.productForm = new FormGroup({
            id: new FormControl('', Validators.required),
            name: new FormControl('', Validators.required),
            priceOld: new FormControl(0),
            priceCurrent: new FormControl(0, [Validators.required, Validators.min(1)]),
            sumCostPriceIngredients: new FormControl(0),
            sumSellPriceIngredients: new FormControl(0),
            ingredients: new FormArray([], Validators.required),
            additional: new FormArray([]),
            photos: new FormArray([]),
            collections: new FormArray([]),
            categories: new FormArray([]),
            on: new FormControl(false),
            alco: new FormControl(false),
            date: new FormControl(moment().startOf('day'), Validators.required),
            description: new FormControl(''),
            rating: new FormGroup({
                productId: new FormControl('', Validators.required),
                count: new FormControl(0, Validators.required),
                sumRating: new FormControl(0, [Validators.required, Validators.min(0), Validators.max(5)])
            }),
            review: new FormArray([]),
            question: new FormArray([]),
            type: new FormArray([])
        });
    }


    private changesProductIdStream(id: string): void {
        id ? this.changeProductIdIfIdYes(id) : this.changeProductIdIfIdNo()
    }

    private changeProductIdIfIdYes(id: string): void {

        this.abstractProductFullModel$ = this.getProductFull$(id)
            .startWith(new AbstractProductFullModel(null, EStateModel.LOADING))
            .do((abstractProductFullModel: AbstractProductFullModel) => {
                this.initProductFull(abstractProductFullModel && abstractProductFullModel.product, abstractProductFullModel && abstractProductFullModel.product && abstractProductFullModel.product.id || id);
                this.initUploader();
            });

        this.abstractProductFullPhotosModel$ = this.getPhotos$(id)
            .startWith(new AbstractProductFullPhotosModel(null, EStateModel.LOADING))
            .do((abstractProductFullPhotosModel: AbstractProductFullPhotosModel) => {
                this.initProductFullPhotos(abstractProductFullPhotosModel && abstractProductFullPhotosModel.photos || []);
            });

    }

    private changeProductIdIfIdNo(): void {

        this.abstractProductFullModel$ = this.createProductFull$()
            .do((abstractProductFullModel: AbstractProductFullModel) => {
                this.productId = abstractProductFullModel && abstractProductFullModel.product && abstractProductFullModel.product.id;
                this.initProductFull(abstractProductFullModel && abstractProductFullModel.product, abstractProductFullModel && abstractProductFullModel.product && abstractProductFullModel.product.id);
                this.initUploader();
            });

        this.abstractProductFullPhotosModel$ = this.createPhotos$()
            .do((abstractProductFullPhotosModel: AbstractProductFullPhotosModel) => {
                this.initProductFullPhotos(abstractProductFullPhotosModel && abstractProductFullPhotosModel.photos || []);
            });

    }

    public getPhotos$(prodId: string): Observable<AbstractProductFullPhotosModel> {
        this.resetPhotos();
        return this.productsService.getPhotos(prodId)
            .map((res: string[]) => {
                return new AbstractProductFullPhotosModel(res, EStateModel.LOADED);
            })
            .catch((error: ConnectorErrorModel) => {
                this.unSnackBarService.openError(error && error.description ? error.description : 'Ошибка получения фотографий товара');
                return Observable.of(new AbstractProductFullPhotosModel([], EStateModel.ERROR));
            });
    }

    public createPhotos$(): Observable<AbstractProductFullPhotosModel> {
        this.resetPhotos();
        return Observable.of(new AbstractProductFullPhotosModel([], EStateModel.LOADED));
    }

    private getProductFull$(id: string): Observable<AbstractProductFullModel> {

        //TODO разобраться с резетом ингредиентов
        this.isModeView() && this.resetIngredients();

        return this.productsService.getFullProductById(id)
            .map((productFull : IProductFullResponse) => {
                return new AbstractProductFullModel(
                    new ProductFullModel(productFull),
                    EStateModel.LOADED
                )
            })
            .catch(() => {
                return Observable.of(new AbstractProductFullModel(
                    new ProductFullModel(null),
                    EStateModel.ERROR
                ))
            });
    }

    private createProductFull$(): Observable<AbstractProductFullModel> {
        return Observable.of(new AbstractProductFullModel(
            new ProductFullModel(null),
            EStateModel.LOADED
        ))
    }

    private initProductFull(productFull: IProductFull, id: string): void {
        this.productForm.get('id').setValue(id);
        this.productForm.get('name').setValue(productFull && productFull.name || '');
        this.productForm.get('priceOld').setValue(productFull && productFull.priceOld || 0);
        this.productForm.get('priceCurrent').setValue(productFull && productFull.priceCurrent || 0);
        this.productForm.get('on').setValue(productFull && productFull.on || false);
        this.productForm.get('alco').setValue(productFull && productFull.alco || false);
        this.productForm.get('date').setValue(productFull && productFull.date && moment(productFull.date) || moment());
        this.productForm.get('description').setValue(productFull && productFull.description);
    }

    public updateSumSellPriceIngredients(sum: number): void {
        this.productForm.get('sumSellPriceIngredients').setValue(sum);
    }

    public updateSumCostPriceIngredients(sum: number): void {
        this.productForm.get('sumCostPriceIngredients').setValue(sum);
    }

    private initProductFullPhotos(photos: string[]): void {
        photos
            .map((photo: string) => new FormControl(photo))
            .forEach((control: FormControl) => {
                this.photos.push(control);
            });
    }

    private initUploader(): void {
        this.uploader = this.imageUploaderService.getNewUploader({
            url: `${appConfig.apiUrl}/products/setUploadPhotos`
        })
    }

    public addNewIngredient(): void {
        this.updateProductAlco();
    }

    public get ingredients(): FormArray {
        return (<FormArray>this.productForm.get('ingredients'));
    }

    public get additional(): FormArray {
        return (<FormArray>this.productForm.get('additional'));
    }

    public deleteIngredient(i: number): void {
        this.updateProductAlco();
    }

    private updateProductAlco(): void {
        this.productForm.get('alco').setValue(this.ingredients.getRawValue().some((r: IProductIngredientFull) => r && r.ingredient && r.ingredient.alco));
    }

    public deletePhoto(i: number): void {
        if (this.isModeEdit() || this.isModeCreate()) {
            this.cachedPhotos.push(this.photos.controls[i]);
            this.photos.removeAt(i);
        }
    }

    public get photos(): FormArray {
        return (<FormArray>this.productForm.get('photos'));
    }

    public resetPhotos(): void {
        this.productForm.controls['photos'] = new FormArray([]);
    }

    public resetIngredients(): void {
        this.productForm.controls['ingredients'] = new FormArray([], Validators.required);
        this.productForm.controls['additional'] = new FormArray([]);
    }

    public removePhotos(productId: string, productFileNames: string[]): Observable<boolean>{
        return this.productsService.removePhotos(productId, productFileNames);
    }

    public onSubmit(): void {
         if (this.productForm.valid) {
             this.state = EStateModel.LOADING;
             const sub: Subscription = (this.uploader.getNotUploadedItems().length ? this.submitFormDataWithPhotos() : this.submitFormData())
                 .finally(() => sub.unsubscribe())
                 .subscribe((res: string) => {
                         this.abstractProductService.setProductId(res);
                         this.unSnackBarService.openSucces(this.isModeEdit() ? 'Информация о товаре успешно изменена' : 'Новый товар создан');
                         this.dialogRef && this.dialogRef.close();
                         this.state = EStateModel.LOADED;
                         return res;
                     },
                     (error: ConnectorErrorModel) => {
                         this.unSnackBarService.openError(error && error.description || '');
                         this.state = EStateModel.ERROR;
                         return Observable.throw(error);
                     })
         }
    }

    private submitFormDataWithPhotos(): Observable<string> {
        return this.submitFormData()
            .switchMap((productId: string) => {
                const subject: Subject<string> = new Subject();
                this.uploader.setOptions({headers: this.appHttpInterceptor.getHeadersIntercept().concat([{name: 'productid', value: productId}])});
                this.uploader.onCompleteAll = () => {
                    subject.next(productId);
                };
                this.uploader.uploadAll();
                return subject.asObservable();
            })
    }

    private submitFormData(): Observable<string> {
        return this.insertOrUpdateProduct(this.getProductFullRequestForm())
            .switchMap((prodId: string) => {
                return this.setRating({...<IRating>this.productForm.get('rating').value, ...{productId: prodId}})
                    .combineLatest(
                        this.reviewCacheService.changeReviews(this.reviewCacheService.getSubmitData(this.productForm.get('review').value.map((r: IReview) => new ReviewModel(r))), prodId),
                        Observable.of(this.questionCacheService.changeQuestionsWithAnswers(this.productForm.get('question').value, prodId)),
                        this.removeCachedPhotos(prodId)
                    )
                    .map(() => prodId)
            })
            .map((prodId: string) => prodId)
            .catch((prodId) => Observable.of(prodId || ''))
    }

    private setRating(rating: IRating): Observable<boolean> {
        return this.ratingService
            .setRating(rating)
            .catch((error: ConnectorErrorModel) => {
                this.unSnackBarService.openError('Ошибка обновления рейтинга товара');
                return Observable.throw(false);
            });
    }

    public getProductFullRequestForm(): IProductFullRequest {
        return {
            id: this.productForm.getRawValue().id,
            name: this.productForm.getRawValue().name,
            priceOld: this.productForm.getRawValue().priceOld,
            priceCurrent: this.productForm.getRawValue().priceCurrent,
            date: this.productForm.getRawValue().date.startOf('day').toISOString(),
            description: this.productForm.getRawValue().description,
            ingredients: this.productForm.getRawValue().ingredients,
            additional: this.productForm.getRawValue().additional,
            categories: this.productForm.getRawValue().categories,
            collections: this.productForm.getRawValue().collections,
            type: this.productForm.getRawValue().type,
            on: this.productForm.getRawValue().on,
        };
    }

    /**
     * Добавление или изменение товара
     * @param {IProductFullRequest} productFullRequest
     * @returns {Observable<boolean>}
     */
    private insertOrUpdateProduct(productFullRequest: IProductFullRequest): Observable<string> {
        return (this.isModeEdit()
                ? this.productsService.updateProduct(productFullRequest)
                : this.productsService.insertProduct(productFullRequest)
            )
            .map((productId: string) => productId)
            .catch((error: ConnectorErrorModel) => Observable.of(''));
    }

    private removeCachedPhotos(productId: string): Observable<boolean> {
        return this.cachedPhotos.value.length
            ?  this.removePhotos(
            productId || '',
            this.cachedPhotos.value.length
                ? <string[]>this.cachedPhotos.value.map((path: string) => path.split('/').pop())
                : []
                ).do(() => {
                    this.cachedPhotos = new FormArray([]);
                })
            :  Observable.of(true);
    }

    public removeProduct(productId: string): void {
        const subRemove: Subscription = this.productsService.removeProductById(productId)
            .map((res: boolean) => {
                this.abstractProductService.setProductId(productId);
                this.dialogRef && this.dialogRef.close();
                this.unSnackBarService.openSucces('Ингредиент успешно перенесен в корзину');
                return res;
            })
            .catch((error: ConnectorErrorModel) => {
                this.abstractProductService.setProductId(productId);
                this.unSnackBarService.openError('Ошибка переноса продукта в корзину');
                return null;
            })
            .subscribe(() => {
                subRemove.unsubscribe();
            })
    }

    public editProduct(productId: string) {
        let dialogRef = this.dialog.open(AbstractProductModalComponent, {panelClass: 'un-large-panel-class', data: {productId: productId, mode: EAbstractProductMode.EDIT}});
    }

    public isModeCreate(): boolean {
        return this.mode === EAbstractProductMode.CREATE;
    }

    public isModeEdit(): boolean {
        return this.mode === EAbstractProductMode.EDIT;
    }

    public isModeView(): boolean {
        return this.mode === EAbstractProductMode.VIEW;
    }

    public selectProductEE(id: string): void {
        this.selectProduct.emit(id);
    }
}