import {Component, Inject, Input, OnDestroy, OnInit} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material";
import 'rxjs/add/operator/finally';
import 'rxjs/add/observable/throw';
import {IAbstractProductModalData} from "./interface/abstract-product-modal-data.interface";
import {EAbstractProductMode} from "../abstract-product/enum/abstract-product-mode.enum";

@Component({
    selector: 'abstract-product-modal',
    templateUrl: './abstract-product-modal.component.html',
    styleUrls: ['./abstract-product-modal.component.scss']
})
export class AbstractProductModalComponent {

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: IAbstractProductModalData,
        public dialogRef: MatDialogRef<AbstractProductModalComponent>,
    ) {

    }

    public close(): void {
       this.dialogRef.close();
   }

    public isModeCreate(): boolean {
        return this.data.mode === EAbstractProductMode.CREATE;
    }

    public isModeEdit(): boolean {
        return this.data.mode === EAbstractProductMode.EDIT;
    }

    public isModeView(): boolean {
        return this.data.mode === EAbstractProductMode.VIEW;
    }
}