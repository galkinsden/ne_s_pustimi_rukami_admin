import {EAbstractProductMode} from "../../abstract-product/enum/abstract-product-mode.enum";

export interface IAbstractProductModalData {
    mode: EAbstractProductMode,
    productId: string
}