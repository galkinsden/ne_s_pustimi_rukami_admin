import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {AbstractProductModalComponent} from "./abstract-product-modal.component";
import {MatButtonModule, MatDialogModule, MatIconModule} from "@angular/material";
import {AbstractProductModule} from "../abstract-product/abstract-product.module";


@NgModule({
    declarations: [AbstractProductModalComponent],
    exports: [AbstractProductModalComponent],
    imports: [
        CommonModule,
        RouterModule,
        MatDialogModule,
        AbstractProductModule,
        MatIconModule,
        MatButtonModule
    ],
    providers: []
})

export class AbstractProductModalModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: AbstractProductModalModule};
    }
}
