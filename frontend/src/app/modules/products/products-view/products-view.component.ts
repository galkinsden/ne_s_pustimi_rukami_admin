import {Component, EventEmitter, forwardRef, Input, OnDestroy, OnInit, Output} from "@angular/core";
import {ProductsService} from "../products.service";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/last';
import 'rxjs/add/observable/of';
import {EStateModel} from "../../../libraries/common/utils/state/state-model.enum";
import {IProductView, ProductsViewModel} from "../models/product-view.model";
import {ProductsListItemModel} from "../models/product-list-item.model";
import {appConfig} from "../../../app.config";
import {FileUploader} from "ng2-file-upload";


@Component({
    selector: 'products-view',
    templateUrl: './products-view.component.html',
    styleUrls: ['./products-view.component.scss']
})
export class ProductsViewComponent implements OnInit, OnDestroy {

    @Input() public write: boolean = false;
    @Input() public read: boolean = false;
    @Input() public isBasket: boolean = false;
    @Input() public mini: boolean = false;

    @Output() public selectProduct: EventEmitter<string> = new EventEmitter();

    public $model: Observable<ProductsViewModel>;
    public apiUrl: string = appConfig.apiUrl;
    public uploader: FileUploader;

    constructor(
        private productsService: ProductsService
    ) {

    }

    public ngOnInit(): void {
        this.$model = this.changeProductStream$();
    }


    public ngOnDestroy(): void {

    }

    public changeCountProductStream$(): Observable<EStateModel> {
        return this.productsService.countProducts
            .map((count: number) => count > 0 ? EStateModel.WAIT : EStateModel.NONE)
    }

    public changeProductStream$(): Observable<ProductsViewModel> {
        return this.productsService.selectedProduct
            .combineLatest(this.changeCountProductStream$())
            .map(([productsListItemModel, state]: [ProductsListItemModel, EStateModel]) => {
                return {
                    state: state,
                    productId: productsListItemModel && productsListItemModel.id || ''
                }
            })
            .map((productViewResponse: IProductView) => {
                return new ProductsViewModel(
                    productViewResponse.productId,
                    productViewResponse.state
                )
            })
    }

    public selectProductEE(id: string): void {
        this.selectProduct.emit(id);
    }

}