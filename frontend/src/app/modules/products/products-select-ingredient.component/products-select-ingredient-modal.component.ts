import {Component, Inject, Input, OnDestroy, OnInit} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import 'rxjs/add/operator/finally';
import 'rxjs/add/observable/throw';
import {StateModel} from "../../../libraries/common/utils/state/state.model";
import {EStateModel} from "../../../libraries/common/utils/state/state-model.enum";
import {IIngredientFull} from "../../ingredient/models/ingredient-full-model.model";
import {IIngredient} from "../../ingredient/models/ingredients.model";

@Component({
    selector: 'products-select-ingredient-modal',
    templateUrl: './products-select-ingredient-modal.component.html',
    styleUrls: ['./products-select-ingredient-modal.component.scss']
})
export class ProductsSelectIngredientModalComponent extends StateModel implements OnInit {

    constructor(
        public dialogRef: MatDialogRef<ProductsSelectIngredientModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
    ) {
        super(EStateModel.NONE);
    }

    public ngOnInit(): void {

    }

    public selectIngredient(ingredient: IIngredientFull): void {
        this.close(ingredient);
    }

    public close(ingredient: IIngredient): void {
        this.dialogRef.close(ingredient);
    }
}