import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
    MatButtonModule, MatFormFieldModule, MatIconModule, MatInputModule,
    MatSelectModule, MatTooltipModule
} from "@angular/material";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ProductsIngredientsComponent} from "./products-ingredients.component";
import {DisableControlModule} from "../../../libraries/common/utils/disable-control/disable-control.module";
import {PreloaderModule} from "../../../libraries/common/ui/preloader/preloader.module";
import {EmptyAreaModule} from "../../../libraries/common/ui/empty-area/empty-area.module";
import {PhotoSmallModule} from "../../../libraries/common/ui/photo-small/photo-small.module";
import {ProductsIngredientsItemComponent} from "./products-ingredients-item/products-ingredients-item.component";
import {RouterModule} from "@angular/router";


@NgModule({
    declarations: [ProductsIngredientsComponent, ProductsIngredientsItemComponent],
    exports     : [ProductsIngredientsComponent, ProductsIngredientsItemComponent],
    imports     : [
        MatInputModule,
        MatFormFieldModule,
        FormsModule,
        ReactiveFormsModule,
        DisableControlModule,
        PreloaderModule,
        EmptyAreaModule,
        MatIconModule,
        MatButtonModule,
        CommonModule,
        MatSelectModule,
        PhotoSmallModule,
        MatTooltipModule,
        RouterModule
    ],
    providers   : [],
    entryComponents: []
})

export class ProductsIngredientsModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: ProductsIngredientsModule};
    }
}
