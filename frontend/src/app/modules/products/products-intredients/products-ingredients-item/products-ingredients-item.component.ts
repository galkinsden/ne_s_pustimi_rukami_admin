import {Component, EventEmitter, Input, OnChanges, Output} from "@angular/core";
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {IngredientFullModel} from "../../../ingredient/models/ingredient-full-model.model";
import {EAbstractProductMode} from "../../abstract-product/enum/abstract-product-mode.enum";
import {appConfig} from "../../../../app.config";

@Component({
    selector: 'products-ingredients-item',
    templateUrl: './products-ingredients-item.component.html',
    styleUrls: ['./products-ingredients-item.component.scss']
})
export class ProductsIngredientsItemComponent implements OnChanges {

    @Input() public formItem: FormGroup = new FormGroup({
        id: new FormControl(''),
        count: new FormControl(0, [Validators.required, Validators.min(1)])
    });
    @Input() public ingredient: IngredientFullModel = new IngredientFullModel(null);
    @Input() public mode: EAbstractProductMode = EAbstractProductMode.VIEW;
    @Input() public index: number = null;
    @Output() public deleteIngredient: EventEmitter<number> = new EventEmitter<number>();
    @Output() public updateSumCostPriceIngredients: EventEmitter<number> = new EventEmitter<number>();
    @Output() public updateSumSellPriceIngredients: EventEmitter<number> = new EventEmitter<number>();

    public apiUrl: string = appConfig.apiUrl;

    public ngOnChanges(): void {

    }

    public isModeCreate(): boolean {
        return this.mode === EAbstractProductMode.CREATE;
    }

    public isModeEdit(): boolean {
        return this.mode === EAbstractProductMode.EDIT;
    }

    public isModeView(): boolean {
        return this.mode === EAbstractProductMode.VIEW;
    }

    public deleteIngredientEE(index: number): void {
        this.deleteIngredient.emit(index);
    }

    public updateSumCostPriceIngredientsEE(): void {
        this.updateSumCostPriceIngredients.emit();
    }

    public updateSumSellPriceIngredientsEE(): void {
        this.updateSumSellPriceIngredients.emit();
    }

    public countChange(): void {
        this.updateSumCostPriceIngredientsEE();
        this.updateSumSellPriceIngredientsEE();
    }
}