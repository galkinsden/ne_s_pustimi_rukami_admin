import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {EAbstractProductMode} from "../abstract-product/enum/abstract-product-mode.enum";
import {IProductIngredientFull, ProductIngredientFullModel} from "../models/product-ingredient-full.model";
import {Subscription} from "rxjs/Subscription";
import {ProductsSelectIngredientModalComponent} from "../products-select-ingredient.component/products-select-ingredient-modal.component";
import {IngredientFullModel} from "../../ingredient/models/ingredient-full-model.model";
import {MatDialog} from "@angular/material";
import {UnSnackBarService} from "../../../libraries/common/ui/snackbar/un-snack-bar.service";
import {Observable} from "rxjs/Observable";
import {IIngredientFullResponse} from "../../ingredient/connectors/models/response/ingredient-full-response.model";
import {IngredientsService} from "../../ingredient/ingredients.service";
import {appConfig} from "../../../app.config";
import {StateModel} from "../../../libraries/common/utils/state/state.model";
import {EStateModel} from "../../../libraries/common/utils/state/state-model.enum";
import {OrderFullModel} from "../../../libraries/buyer/order/models/order-full.model";
import {ProductModel} from "../models/products.model";
import {IOrder} from "../../../libraries/buyer/order/models/order.model";
import {BuyerAddProductModalComponent} from "../../plan/abstract-buyer/buyer-add-product-modal/buyer-add-product-modal.component";
import {IProductIngredientRequest} from "../connectors/models/request/product-ingredient-request.model";
import {IProductIngredient} from "../models/product-ingredient.model";

export enum EFormArrayIngredientsName {
    ingredients = 'ingredients',
    additional = 'additional'
}

@Component({
    selector: 'products-ingredients',
    templateUrl: 'products-ingredients.component.html',
    styleUrls: ['./products-ingredients.component.scss']
})
export class ProductsIngredientsComponent extends StateModel implements OnChanges {

    @Input() public formIngredients: FormArray = new FormArray([]);
    @Input() public formGroupProduct: FormGroup = new FormGroup({});
    @Input() public mode: EAbstractProductMode = EAbstractProductMode.VIEW;
    @Input() public ingredients: ProductIngredientFullModel[] = [];
    @Input() public formArrayIngredientsName: EFormArrayIngredientsName = EFormArrayIngredientsName.ingredients;

    @Output() public deleteIngredient: EventEmitter<string> = new EventEmitter<string>();
    @Output() public addNewIngredient: EventEmitter<string> = new EventEmitter<string>();
    @Output() public updateSumCostPriceIngredients: EventEmitter<number> = new EventEmitter();
    @Output() public updateSumSellPriceIngredients: EventEmitter<number> = new EventEmitter();

    public apiUrl: string = appConfig.apiUrl;

    public constructor(
        private dialog: MatDialog,
        private unSnackBarService: UnSnackBarService,
        private ingredientsService: IngredientsService
    ) {
        super(EStateModel.NONE);
    }

    public ngOnChanges() {
        this.updateForm();
    }

    private updateForm(): void {
        this.ingredients && Array.isArray(this.ingredients) && this.ingredients
            .map((prod: ProductIngredientFullModel) => this.createIngredient(prod))
            .forEach((group: FormGroup) => {
                this.formIngredients.push(group);
            });
        this.updateSumCostPriceIngredientsEE();
        this.updateSumSellPriceIngredientsEE();
    }

    public isModeCreate(): boolean {
        return this.mode === EAbstractProductMode.CREATE;
    }

    public isModeEdit(): boolean {
        return this.mode === EAbstractProductMode.EDIT;
    }

    public isModeView(): boolean {
        return this.mode === EAbstractProductMode.VIEW;
    }

    public deleteIngredientEE(i: number): void {
        const index: number = this.ingredients.map((prodIng: ProductIngredientFullModel) => prodIng && prodIng.ingredient && prodIng.ingredient.id).indexOf(this.formIngredients.at(i).get('id').value);
        const id: string = this.formIngredients.at(i).get('id').value || '';
        this.ingredients.splice(index, 1);
        this.formIngredients.removeAt(i);
        this.updateSumCostPriceIngredientsEE();
        this.updateSumSellPriceIngredientsEE();
        this.deleteIngredient.emit(id);
    }

    public addNewIngredientEE($event): void {
        const sub: Subscription = this.dialog
            .open(ProductsSelectIngredientModalComponent, {panelClass: 'un-large-panel-class', data: {isAdditional: this.formArrayIngredientsName === EFormArrayIngredientsName.additional}})
            .afterClosed()
            .subscribe((ingredientId: string) => {
                if (ingredientId) {
                    if (!!~this.formIngredients.value.filter((prod: IProductIngredient) => prod && prod.id).map((prod: IProductIngredient) => prod.id).indexOf(ingredientId)) {
                        this.unSnackBarService.openError('Ингредиент уже добавлен в товар');
                    } else {
                        this.state = EStateModel.LOADING;
                        const sub: Subscription = this.getIngredientFullWithPhotos$(ingredientId)
                            .finally(() => {
                                this.state = EStateModel.LOADED;
                                sub.unsubscribe();
                            })
                            .subscribe((ingredientFullModel: IngredientFullModel) => {
                                this.ingredients.push({ingredient: ingredientFullModel, count: 0});
                                this.formIngredients.push(this.createIngredient({ingredient: ingredientFullModel, count: 0}));
                                this.updateSumCostPriceIngredientsEE();
                                this.updateSumSellPriceIngredientsEE();
                                this.addNewIngredient.emit(ingredientId);
                            })
                    }
                }
                sub.unsubscribe();
            });
    }

    private getIngredientFullWithPhotos$(ingredientId: string): Observable<IngredientFullModel> {
        return this.ingredientsService.getFullIngredientById(ingredientId)
            .combineLatest(this.ingredientsService.getPhotos(ingredientId))
            .map(([ingredientFullResponse, photos]: [IIngredientFullResponse, string[]]) => new IngredientFullModel({...ingredientFullResponse, ...{photos: photos}}))
            .catch(() => {
                this.unSnackBarService.openError('Ошибка получения информации по ингредиенту');
                return Observable.of(null);
            })
    }

    public updateSumCostPriceIngredientsEE(): void {
        const sum: number = this.formIngredients.value.reduce((acc: number, productIngredientRequest: IProductIngredientRequest) => {
            const index: number = this.ingredients
                .map((ing: ProductIngredientFullModel) => ing && ing.ingredient && ing.ingredient.id ? ing.ingredient.id : '')
                .filter((id: string) => id)
                .indexOf(productIngredientRequest.id);
            return !!~index && this.ingredients[index].ingredient && this.ingredients[index].ingredient.costPrice && productIngredientRequest.count
                ? acc + this.ingredients[index].ingredient.costPrice * productIngredientRequest.count
                : acc;
        }, 0);
        this.updateSumCostPriceIngredients.emit(sum);
    }

    public updateSumSellPriceIngredientsEE(): void {
        const sum: number = this.formIngredients.value.reduce((acc: number, productIngredientRequest: IProductIngredientRequest) => {
            const index: number = this.ingredients
                .map((ing: ProductIngredientFullModel) => ing && ing.ingredient && ing.ingredient.id ? ing.ingredient.id : '')
                .filter((id: string) => id)
                .indexOf(productIngredientRequest.id);
            return !!~index && this.ingredients[index].ingredient && this.ingredients[index].ingredient.sellPrice && productIngredientRequest.count
                ? acc + this.ingredients[index].ingredient.sellPrice * productIngredientRequest.count
                : acc;
        }, 0);
        this.updateSumSellPriceIngredients.emit(sum);
    }

    public createIngredient(productIngredient: ProductIngredientFullModel): FormGroup {
        return new FormGroup({
            id: new FormControl(productIngredient && productIngredient.ingredient && productIngredient.ingredient.id || ''),
            count: new FormControl(productIngredient && productIngredient.count || 0, [
                Validators.required,
                Validators.min(1),
                Validators.max(productIngredient && productIngredient.ingredient && productIngredient.ingredient.count)
            ])
        });
    }

    public getEmptyIngredientsTitle(): string {
        switch (this.formArrayIngredientsName) {
            case EFormArrayIngredientsName.ingredients:
                return 'Ингредиентов пока нет';
            case EFormArrayIngredientsName.additional:
                return 'Дополнительных ингредиентов пока нет';
            default:
                return 'Ингредиентов пока нет';
        }
    }

    public getButtonAddTitle(): string {
        switch (this.formArrayIngredientsName) {
            case EFormArrayIngredientsName.ingredients:
                return 'Добавьте ингредиенты в данный товар';
            case EFormArrayIngredientsName.additional:
                return 'Добавьте дополнительные ингредиенты в данный товар';
            default:
                return 'Добавьте ингредиенты в данный товар';
        }
    }

    public getIngredientFullItem(formIngredient: FormGroup, productIngredientsFull: ProductIngredientFullModel[]): IngredientFullModel {
        const index: number = productIngredientsFull && productIngredientsFull.length && formIngredient.get('id') && formIngredient.get('id').value
            ? productIngredientsFull.map((productIngredientFull: IProductIngredientFull) => productIngredientFull.ingredient && productIngredientFull.ingredient.id).indexOf(formIngredient.get('id').value)
            : -1;
        return !!~index && productIngredientsFull[index] && productIngredientsFull[index].ingredient ? productIngredientsFull[index].ingredient : new IngredientFullModel(null);
    }
}