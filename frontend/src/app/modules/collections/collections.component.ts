import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import * as uuidv1 from 'uuid/v1';
import {EAbstractProductMode} from "../products/abstract-product/enum/abstract-product-mode.enum";
import {CollectionModel, ICollection} from "./models/collection.model";
import {CollectionsModalComponent} from "./collections-modal/collections-modal.component";
import {MatDialog, MatSelectChange} from "@angular/material";
import {EStateModel} from "../../libraries/common/utils/state/state-model.enum";
import {CollectionsModalModel, ICollectionsModal} from "./collections-modal/models/collections-modal.model";
import {Observable} from "rxjs/Observable";
import {CollectionsService} from "./collections.service";
import {CollectionsModel} from "./models/collections.model";
import {Subscription} from "rxjs/Subscription";
import {UnSnackBarService} from "../../libraries/common/ui/snackbar/un-snack-bar.service";

@Component({
    selector: 'un-collections',
    templateUrl: 'collections.component.html',
    styleUrls: ['./collections.component.scss']
})
export class CollectionsComponent implements OnChanges {

    @Input() public formCollections: FormArray = new FormArray([]);
    @Input() public mode: EAbstractProductMode;
    @Input() public collectionsInput: string[] = [];

    public collectionsModal$: Observable<CollectionsModel>;
    public formSelectControl: FormControl = new FormControl();

    public constructor(
        private dialog: MatDialog,
        private collectionsService: CollectionsService,
        private unSnackBarService: UnSnackBarService
    ) {

    }

    public ngOnChanges() {
        this.init();
    }

    private init(): void {
        this.clearFormArray(this.formCollections);
        this.collectionsModal$ = this.getAllCollections$();
        this.formSelectControl = new FormControl(this.collectionsInput);
    }

    public isModeCreate(): boolean {
        return this.mode === EAbstractProductMode.CREATE;
    }

    public isModeEdit(): boolean {
        return this.mode === EAbstractProductMode.EDIT;
    }

    public isModeView(): boolean {
        return this.mode === EAbstractProductMode.VIEW;
    }

    private getAllCollections$(): Observable<CollectionsModel> {
        return this.collectionsService.getAll()
            .do((res: CollectionModel[]) => {
                this.initCollections(res, this.collectionsInput);
            })
            .map((res: CollectionModel[]) => {
                return new CollectionsModel(
                    res,
                    EStateModel.LOADED
                )
            })
            .catch(() => {
                this.unSnackBarService.openError('Ошибка загрузки списка коллекций');
                return Observable.of(new CollectionsModel(
                    [],
                    EStateModel.ERROR
                ))
            })
            .startWith(new CollectionsModalModel([], EStateModel.LOADING));
    }

    public createCollection(id: string): FormControl {
        return new FormControl(id || '')
    }

    private initCollections(collections: CollectionModel[] = [], ids: string[]): void {
        collections.filter((collection: CollectionModel) => !!~ids.indexOf(collection.id)).forEach((collection: CollectionModel) => {
            this.collections.push(this.createCollection(collection && collection.id || ''))
        });
    }

    public get collections(): FormArray {
        return <FormArray>this.formCollections;
    }

    public editCollections() {
        const sub: Subscription = this.dialog.open(CollectionsModalComponent, {panelClass: 'un-large-panel-class', data: {mode: this.mode}})
            .afterClosed()
            .finally(() => {
                sub.unsubscribe();
            })
            .subscribe(() => {
                this.init();
            })
    }

    public selectionChange($event: MatSelectChange): void {
        this.clearFormArray(this.formCollections);
        $event && $event.value && $event.value.forEach((id: string) => {
            this.formCollections.push(this.createCollection(id))
        })
    }

    public clearFormArray(formArray: FormArray): void {
        while (formArray.length !== 0) {
            formArray.removeAt(0)
        }
    }
}