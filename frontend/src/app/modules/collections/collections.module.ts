import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
    MatButtonModule, MatFormFieldModule, MatIconModule, MatInputModule, MatRadioModule,
    MatSelectModule, MatTooltipModule
} from "@angular/material";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DisableControlModule} from "../../libraries/common/utils/disable-control/disable-control.module";
import {PreloaderModule} from "../../libraries/common/ui/preloader/preloader.module";
import {EmptyAreaModule} from "../../libraries/common/ui/empty-area/empty-area.module";
import {CollectionsComponent} from "./collections.component";
import {CollectionsModalModule} from "./collections-modal/collections-modal.module";
import {CollectionsModalComponent} from "./collections-modal/collections-modal.component";
import {CollectionsService} from "./collections.service";


@NgModule({
    declarations: [CollectionsComponent],
    exports     : [CollectionsComponent],
    imports     : [
        MatInputModule,
        MatFormFieldModule,
        FormsModule,
        ReactiveFormsModule,
        DisableControlModule,
        PreloaderModule,
        EmptyAreaModule,
        MatIconModule,
        MatButtonModule,
        CommonModule,
        MatSelectModule,
        MatTooltipModule,
        CollectionsModalModule
    ],
    providers   : [CollectionsService],
    entryComponents: [CollectionsModalComponent]
})

export class CollectionsModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: CollectionsModule};
    }
}
