import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {ConnectorErrorModel, IError} from '../../../libraries/common/utils/connector-error.model';
import {appConfig} from '../../../app.config';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {ICollection} from "../models/collection.model";

export interface ICollectionsConnectorGetAllResponse {
    data: ICollection[];
    error: IError;
}

export interface ICollectionsConnectorSetCollectionsByIdsResponse {
    data: boolean;
    error: IError;
}


export class CollectionsConnector {

    constructor(private http: HttpClient) {}

    public getAll(): Observable<ICollection[] | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/collection-api/getCollections`, {})
            .map((res: ICollectionsConnectorGetAllResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public setCollectionsByIds(collections: ICollection[]): Observable<boolean | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/collection-api/setCollectionsByIds`, collections)
            .map((res: ICollectionsConnectorSetCollectionsByIdsResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    private handleError(res: IError): Observable<ConnectorErrorModel> {
        return Observable.throw(new ConnectorErrorModel(res));
    }
}