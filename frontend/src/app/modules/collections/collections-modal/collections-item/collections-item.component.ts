import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import * as uuidv1 from 'uuid/v1';
import {MatDialog} from "@angular/material";
import {EAbstractProductMode} from "../../../products/abstract-product/enum/abstract-product-mode.enum";

@Component({
    selector: 'collections-item',
    templateUrl: 'collections-item.component.html',
    styleUrls: ['./collections-item.component.scss']
})
export class CollectionsItemComponent  {

    @Input() public formCollectionsItem: FormGroup = new FormGroup({
        id: new FormControl(uuidv1()),
        name: new FormControl('', Validators.required),
        description: new FormControl('')
    });
    @Input() public mode: EAbstractProductMode;
    @Input() public index: number;

    @Output() public deleteCollection: EventEmitter<number> = new EventEmitter<number>();


    public isModeCreate(): boolean {
        return this.mode === EAbstractProductMode.CREATE;
    }

    public isModeEdit(): boolean {
        return this.mode === EAbstractProductMode.EDIT;
    }

    public isModeView(): boolean {
        return this.mode === EAbstractProductMode.VIEW;
    }

    public deleteCollectionByIndex(index: number): void {
        this.deleteCollection.emit(index);
    }

}