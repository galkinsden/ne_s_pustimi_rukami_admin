import {Component, Inject, Input, OnDestroy, OnInit} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material";
import 'rxjs/add/operator/finally';
import 'rxjs/add/observable/throw';
import {EAbstractProductMode} from "../../products/abstract-product/enum/abstract-product-mode.enum";
import {CollectionsService} from "../collections.service";
import {Observable} from "rxjs/Observable";
import {CollectionsModalModel} from "./models/collections-modal.model";
import {CollectionModel, ICollection} from "../models/collection.model";
import {EStateModel} from "../../../libraries/common/utils/state/state-model.enum";
import {FormArray, FormControl, FormGroup} from "@angular/forms";
import {Subscription} from "rxjs/Subscription";
import {UnSnackBarService} from "../../../libraries/common/ui/snackbar/un-snack-bar.service";
import * as uuidv1 from 'uuid/v1';
import {CategoriesModalModel} from "../../categories/categories-modal/models/categories-modal.model";

@Component({
    selector: 'collections-modal',
    templateUrl: './collections-modal.component.html',
    styleUrls: ['./collections-modal.component.scss']
})
export class CollectionsModalComponent implements OnInit {

    public collectionsModal$: Observable<CollectionsModalModel>;
    public modalForm: FormGroup;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<CollectionsModalComponent>,
        private collectionsService: CollectionsService,
        private unSnackBarService: UnSnackBarService
    ) {

    }

    public ngOnInit() {
        this.modalForm = new FormGroup({
            collections: new FormArray([])
        });
        this.collectionsModal$ = this.getAllCollections$();
    }

    private getAllCollections$(): Observable<CollectionsModalModel> {
        return this.collectionsService.getAll()
            .do((res: CollectionModel[]) => {
                this.initCollections(res);
            })
            .map((res: CollectionModel[]) => {
                return new CollectionsModalModel(
                    res,
                    EStateModel.LOADED
                )
            })
            .catch(() => {
                return Observable.of(new CollectionsModalModel(
                    [],
                    EStateModel.ERROR
                ))
            })
            .startWith(new CollectionsModalModel([], EStateModel.LOADING));
    }

    public createCollection(collection: ICollection): FormGroup {
        return new FormGroup({
            id: new FormControl(collection && collection.id || uuidv1()),
            name: new FormControl(collection && collection.name || ''),
            description: new FormControl(collection && collection.description || '')
        })
    }

    private initCollections(collections: ICollection[] = []): void {
        collections.forEach((collection: ICollection) => {
            this.collections.push(this.createCollection(collection))
        });
    }

    public get collections(): FormArray {
        return <FormArray>this.modalForm.get('collections');
    }

    public close(): void {
       this.dialogRef.close();
   }

    public isModeCreate(): boolean {
        return this.data.mode === EAbstractProductMode.CREATE;
    }

    public isModeEdit(): boolean {
        return this.data.mode === EAbstractProductMode.EDIT;
    }

    public isModeView(): boolean {
        return this.data.mode === EAbstractProductMode.VIEW;
    }

    public addNewCollection(): void {
        this.collections.push(this.createCollection(null));
    }

    public deleteCollectionByIndex(index: number): void {
        this.collections.removeAt(index);
    }

    public onSubmit(): void {
        this.collectionsModal$ = this.setCollections(this.modalForm.get('collections').value);
    }

    private setCollections(collections: ICollection[]): Observable<CollectionsModalModel> {
        return this.collectionsService.setCollectionsByIds(collections)
            .map((res: boolean) => {
                this.unSnackBarService.openSucces('Список коллекций успешно обновлен');
                this.close();
                return new CollectionsModalModel([], EStateModel.LOADED);
            })
            .catch(() => {
                this.unSnackBarService.openError('Ошибка обновления списка коллекций');
                this.close();
                return Observable.of(new CollectionsModalModel([], EStateModel.ERROR))
            })
            .startWith(new CollectionsModalModel([], EStateModel.LOADING));
    }
}