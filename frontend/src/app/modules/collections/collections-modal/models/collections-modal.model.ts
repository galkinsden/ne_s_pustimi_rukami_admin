import {StateModel} from "../../../../libraries/common/utils/state/state.model";
import {EStateModel} from "../../../../libraries/common/utils/state/state-model.enum";
import {CollectionModel} from "../../models/collection.model";
export interface ICollectionsModal {
    collections: CollectionModel[];
    state: EStateModel;
}

export class CollectionsModalModel extends StateModel implements ICollectionsModal {

    public constructor(
        public readonly collections: CollectionModel[] = [],
        public readonly state: EStateModel = EStateModel.NONE
    ) {
        super(state);
    }
}