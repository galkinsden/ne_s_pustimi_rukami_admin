import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {CollectionsModalComponent} from "./collections-modal.component";
import {
    MatButtonModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule,
    MatTooltipModule
} from "@angular/material";
import {CollectionsItemComponent} from "./collections-item/collections-item.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {PreloaderModule} from "../../../libraries/common/ui/preloader/preloader.module";


@NgModule({
    declarations: [CollectionsModalComponent, CollectionsItemComponent],
    exports: [CollectionsModalComponent, CollectionsItemComponent],
    imports: [
        CommonModule,
        RouterModule,
        MatDialogModule,
        MatIconModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        FormsModule,
        ReactiveFormsModule,
        MatTooltipModule,
        PreloaderModule
    ],
    providers: []
})

export class CollectionsModalModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: CollectionsModalModule};
    }
}
