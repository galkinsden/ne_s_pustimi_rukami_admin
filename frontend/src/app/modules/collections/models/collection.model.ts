export interface ICollection {
    id: string; //id коллекции
    name: string; // название
    description?: string; // описание
}

export class CollectionModel {

    public readonly id: string;
    public readonly name: string;
    public readonly description: string;

    public constructor(src: ICollection) {
        this.id = src && src.id;
        this.name = src && src.name;
        this.description = src && src.description;
    }
}