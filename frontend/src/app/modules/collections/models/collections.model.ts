import {CollectionModel} from "./collection.model";
import {EStateModel} from "../../../libraries/common/utils/state/state-model.enum";
import {StateModel} from "../../../libraries/common/utils/state/state.model";

export interface ICollections {
    collections: CollectionModel[];
    state: EStateModel;
}

export class CollectionsModel extends StateModel implements ICollections {

    public constructor(
        public readonly collections: CollectionModel[] = [],
        public readonly state: EStateModel = EStateModel.NONE
    ) {
        super(state);
    }
}