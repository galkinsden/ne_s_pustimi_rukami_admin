import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {ConnectorErrorModel} from "../../libraries/common/utils/connector-error.model";
import {CollectionsConnector} from "./connectors/collections.connector";
import {CollectionModel, ICollection} from "./models/collection.model";

@Injectable()
export class CollectionsService {

    private collectionsConnector: CollectionsConnector;

    public constructor(http: HttpClient) {
        this.collectionsConnector = new CollectionsConnector(http);
    }

    public getAll(): Observable<ICollection[] | ConnectorErrorModel> {
        return this.collectionsConnector.getAll()
            .map((res: ICollection[]) => res.map((r: ICollection) => new CollectionModel(r)));
    }

    public setCollectionsByIds(collections: ICollection[]): Observable<boolean | ConnectorErrorModel> {
        return this.collectionsConnector.setCollectionsByIds(collections);
    }

}