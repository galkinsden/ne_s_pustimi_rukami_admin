import {RatingModel} from "./rating.model";
import {EStateModel} from "../../../libraries/common/utils/state/state-model.enum";
import {StateModel} from "../../../libraries/common/utils/state/state.model";

export interface IAbstractRating {
    rating: RatingModel;
    state: EStateModel;
}

export class AbstractRatingModel extends StateModel implements IAbstractRating {

    public constructor(
        public readonly rating: RatingModel = null,
        public readonly state: EStateModel = EStateModel.NONE
    ) {
        super(state);
    }
}