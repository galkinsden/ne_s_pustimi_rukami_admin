export interface IRating {
    productId: string // id набора
    count: number, // кол-во проголосовавших
    sumRating: number; //суммарный рейтинг
}

export class RatingModel implements IRating {

    public readonly productId: string;
    public readonly count: number;
    public readonly sumRating: number;

    public constructor(src: IRating) {
        this.productId = src && src.productId || '';
        this.count = src && src.count || 0;
        this.sumRating = src && src.sumRating || 0;
    }

}