import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {ConnectorErrorModel, IError} from '../../../libraries/common/utils/connector-error.model';
import {appConfig} from '../../../app.config';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {IRating} from "../models/rating.model";

export interface IRatingConnectorGetResponse {
    data: IRating;
    error: IError;
}

export interface IRatingConnectorSetResponse {
    data: boolean;
    error: IError;
}

export class RatingConnector {

    constructor(private http: HttpClient) {}

    public get(productId: string): Observable<IRating | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/rating-api/get`, { productId: productId })
            .map((res: IRatingConnectorGetResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public setRating(rating: IRating): Observable<boolean | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/rating-api/set`, { productId: rating.productId, count: rating.count, sumRating: rating.sumRating })
            .map((res: IRatingConnectorSetResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    private handleError(res: IError): Observable<ConnectorErrorModel> {
        return Observable.throw(new ConnectorErrorModel(res));
    }
}