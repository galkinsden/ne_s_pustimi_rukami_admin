import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RatingComponent} from "./rating.component";
import {RatingService} from "./rating.service";
import {StarRatingModule} from "angular-star-rating";
import {MatFormFieldModule, MatInputModule} from "@angular/material";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DisableControlModule} from "../../libraries/common/utils/disable-control/disable-control.module";
import {PreloaderModule} from "../../libraries/common/ui/preloader/preloader.module";
import {EmptyAreaModule} from "../../libraries/common/ui/empty-area/empty-area.module";
import {UnSnackBarModule} from "../../libraries/common/ui/snackbar/un-snack-bar.module";


@NgModule({
    declarations: [RatingComponent],
    exports     : [RatingComponent],
    imports     : [
        MatInputModule,
        MatFormFieldModule,
        FormsModule,
        ReactiveFormsModule,
        DisableControlModule,
        PreloaderModule,
        EmptyAreaModule,
        CommonModule,
        UnSnackBarModule,
        StarRatingModule.forRoot()
    ],
    providers   : [RatingService]
})

export class RatingModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: RatingModule};
    }
}
