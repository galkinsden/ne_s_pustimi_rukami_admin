import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {EAbstractProductMode} from "../products/abstract-product/enum/abstract-product-mode.enum";
import {AbstractRatingModel} from "./models/abstract-rating.model";
import {RatingModel} from "./models/rating.model";
import {EStateModel} from "../../libraries/common/utils/state/state-model.enum";
import {Observable} from "rxjs/Observable";
import {ConnectorErrorModel} from "../../libraries/common/utils/connector-error.model";
import {RatingService} from "./rating.service";
import {UnSnackBarService} from "../../libraries/common/ui/snackbar/un-snack-bar.service";

@Component({
    selector: 'un-rating',
    templateUrl: 'rating.component.html',
    styleUrls: ['./rating.component.scss']
})
export class RatingComponent implements OnChanges {

    @Input() public formRating: FormGroup = new FormGroup({
        productId: new FormControl('', Validators.required),
        count: new FormControl(0, Validators.required),
        sumRating: new FormControl(0, [Validators.required, Validators.min(0), Validators.max(5)])
    });
    @Input() public productId: string;
    @Input() public mode: EAbstractProductMode;

    public abstractRatingModel$: Observable<AbstractRatingModel>;

    public constructor(
        private ratingService: RatingService,
        private unSnackBarService: UnSnackBarService
    ) {

    }

    public ngOnChanges(simpleChanges: SimpleChanges) {
        this.abstractRatingModel$ = this.getRating$(this.productId)
           .startWith(new AbstractRatingModel(null, EStateModel.LOADING))
           .do((abstractRatingModel: AbstractRatingModel) => {
               this.initProductRating(abstractRatingModel && abstractRatingModel.rating || null);
           });
    }

    public isModeCreate(): boolean {
        return this.mode === EAbstractProductMode.CREATE;
    }

    public isModeEdit(): boolean {
        return this.mode === EAbstractProductMode.EDIT;
    }

    public isModeView(): boolean {
        return this.mode === EAbstractProductMode.VIEW;
    }

    public onRatingChange(r: {rating: number}): void {
        if (r && r.rating !== this.formRating.get('sumRating').value) {
            this.formRating.get('sumRating').setValue(r.rating);
        }
    }

    public getRating$(prodId: string): Observable<AbstractRatingModel> {
        return this.ratingService.get(prodId)
            .map((RatingModel: RatingModel) => {
                return new AbstractRatingModel(RatingModel, EStateModel.LOADED)
            })
            .catch((error: ConnectorErrorModel) => {
                this.unSnackBarService.openError(error && error.description ? error.description : 'Ошибка получения рейтинга товара');
                return Observable.of(new AbstractRatingModel(null, EStateModel.ERROR));
            })
    }

    private initProductRating(rating: RatingModel): void {
        this.formRating.get('productId').setValue(rating && rating.productId);
        this.formRating.get('count').setValue(rating && rating.count);
        this.formRating.get('sumRating').setValue(rating && rating.sumRating);
    }
}