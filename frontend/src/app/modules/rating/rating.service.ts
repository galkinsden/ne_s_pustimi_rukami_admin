import {Injectable} from "@angular/core";
import {RatingConnector} from "./connectors/rating.connector";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {ConnectorErrorModel} from "../../libraries/common/utils/connector-error.model";
import {IRating, RatingModel} from "./models/rating.model";

@Injectable()
export class RatingService {

    private RatingConnector: RatingConnector;

    public constructor(http: HttpClient) {
        this.RatingConnector = new RatingConnector(http);
    }

    public get(productId: string): Observable<RatingModel | ConnectorErrorModel> {
        return this.RatingConnector.get(productId)
            .map((res: IRating) => new RatingModel(res));
    }

    public setRating(rating: IRating): Observable<boolean | ConnectorErrorModel> {
        return this.RatingConnector.setRating(rating);
    }

}