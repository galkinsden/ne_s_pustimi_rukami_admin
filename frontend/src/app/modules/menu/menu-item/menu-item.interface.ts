
export interface IMenu extends IMenuLink {
  action?: Function;
  menuItems?: IMenu[];
  badge ?: any; // Кружок со значением
  disabled?: boolean;
  isUnavailable?: boolean;
  icon: string;
}

export interface IMenuLink {
  name: string;
  url: string;
}
