import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {MenuComponent} from './menu/menu.component';
import {MenuItemComponent} from './menu-item/menu-item.component';
import {MatIconModule, MatSidenavModule, MatToolbarModule} from "@angular/material";
import {DropdownModule} from "../../libraries/common/ui/dropdown/dropdown.module";

@NgModule({
	declarations: [MenuComponent, MenuItemComponent],
	exports     : [MenuComponent, MenuItemComponent],
	imports     : [CommonModule, RouterModule, MatToolbarModule, MatSidenavModule, MatIconModule, DropdownModule],
	providers   : []
})

export class MenuModule {
	public static forRoot(): ModuleWithProviders {
		return {ngModule: MenuModule};
	}
}
