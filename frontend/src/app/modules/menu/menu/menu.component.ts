import {Component, Input, OnChanges, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {NavigationEnd, NavigationStart, Router} from '@angular/router';
import 'rxjs/add/operator/filter';
import {IMenu} from '../menu-item/menu-item.interface';
import {FormControl} from "@angular/forms";
import {Observable} from 'rxjs/Observable';
import {AuthService} from 'app/libraries/auth/auth.service';
import {UserModel} from "../../../libraries/user/user.model";
import {Subscription} from "rxjs/Subscription";

@Component({
	selector		: 'menu-wrapper',
	templateUrl		: './menu.component.html',
	styleUrls		: ['./menu.component.scss'],
	encapsulation   : ViewEncapsulation.None
})
/**
 * Директива меню
 * @class MenuComponent
 * @directive menu
 */
export class MenuComponent implements OnInit, OnDestroy {
	@Input() public items: IMenu[] = [];
    public isOpenBread: boolean = false;
	public isHover: boolean = false;
    public user: UserModel;
    public subscription: Subscription;

	public constructor(private router: Router, private authService: AuthService) {
		this.subscription = router.events
			.filter((value: NavigationStart, index: number): boolean => value instanceof NavigationEnd)
			.subscribe(() => this.isActiveKpiMenuItem());
	}

	public ngOnDestroy(): void {
		this.subscription && this.subscription.unsubscribe();
	}

	public ngOnInit() {
		this.isActiveKpiMenuItem();
		this.user = this.authService.getAuthUser();
	}

	public openedChange(opened: boolean): void {
		this.isOpenBread = opened;
	}

	public breadToggle(): void {
		this.isOpenBread = !this.isOpenBread;
	}

	public onClick(item: IMenu) {
        this.isOpenBread = false;
        this.isHover = false;
		item.action && item.action() || this.router.navigate([item.url]);
	}

	public clickOnBusketMobile(): void {
        this.isOpenBread = false;
	}

	/**
	 * Проверка, является ли элемент меню текущим
	 */
	public isActiveKpiMenuItem(): void {
		const getItem = ((items) => {
			if (!items) return;
			for (const menuItem of items){
				if (menuItem.url) {
					const itemUrl = menuItem.url.match(/[^\/].*?(?=\?|$)/g);
					const currentUrl = this.router.url.match(/[^\/].*?(?=\?|$)/g);

					if (itemUrl && itemUrl[0] && currentUrl && currentUrl[0]) {
						menuItem.isActive = (itemUrl[0] === currentUrl[0]);
					}
				}

				if (menuItem.menuItems) {
					menuItem.isActive = getItem(menuItem.menuItems) || menuItem.isActive;
				}
			}
			return items.some((i) => i.isActive);
		});
		getItem(this.items);
	}

	public mouseEnter(): void {
		this.isHover = true;
	}

    public mouseLeave(): void {
        this.isHover = false;
    }
}
