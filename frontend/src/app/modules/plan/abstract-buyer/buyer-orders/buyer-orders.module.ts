import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
    MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatIconModule, MatInputModule,
    MatTooltipModule
} from "@angular/material";
import {BuyerOrdersComponent} from "./buyer-orders.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {PreloaderModule} from "../../../../libraries/common/ui/preloader/preloader.module";
import {BuyerOrdersItemComponent} from "./buyer-orders-item/buyer-orders-item.component";
import {DisableControlModule} from "../../../../libraries/common/utils/disable-control/disable-control.module";
import {UnSnackBarModule} from "../../../../libraries/common/ui/snackbar/un-snack-bar.module";
import {EmptyAreaModule} from "../../../../libraries/common/ui/empty-area/empty-area.module";
import {ProductsIngredientsModule} from "../../../products/products-intredients/products-ingredients.module";
import {IngredientsService} from "../../../ingredient/ingredients.service";
import {PhotoSmallModule} from "../../../../libraries/common/ui/photo-small/photo-small.module";
import {OrdersCacheService} from "./orders-cache/orders-cache.service";
import {RouterModule} from "@angular/router";

@NgModule({
    declarations: [BuyerOrdersComponent, BuyerOrdersItemComponent],
    exports: [BuyerOrdersComponent, BuyerOrdersItemComponent],
    imports: [
        CommonModule,
        MatFormFieldModule,
        MatIconModule,
        FormsModule,
        ReactiveFormsModule,
        PreloaderModule,
        MatButtonModule,
        DisableControlModule,
        MatInputModule,
        UnSnackBarModule,
        MatTooltipModule,
        EmptyAreaModule,
        MatCheckboxModule,
        ProductsIngredientsModule,
        PhotoSmallModule,
        RouterModule
    ],
    providers: [IngredientsService, OrdersCacheService]
})

export class BuyerOrdersModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: BuyerOrdersModule};
    }
}
