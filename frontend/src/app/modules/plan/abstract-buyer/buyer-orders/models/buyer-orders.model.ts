import {EStateModel} from "../../../../../libraries/common/utils/state/state-model.enum";
import {OrderFullModel} from "../../../../../libraries/buyer/order/models/order-full.model";
import {StateModel} from "../../../../../libraries/common/utils/state/state.model";

export interface IBuyerOrders {
    orders: OrderFullModel[];
    state: EStateModel;
}

export class BuyerOrdersModel extends StateModel implements IBuyerOrders {

    public constructor(
        public readonly orders: OrderFullModel[] = [],
        public readonly state: EStateModel = EStateModel.NONE
    ) {
        super(state);
    }

}