import {Component, EventEmitter, Input, OnChanges, Output} from "@angular/core";
import {FormArray, FormControl, FormGroup} from "@angular/forms";
import {EAbstractBuyerMode} from "../../enum/abstract-buyer-mode.enum";
import {IngredientsService} from "../../../../ingredient/ingredients.service";
import {
        BuyerOrdersItemProductPhotosModel
} from "./models/buyer-orders-item-product-photos.model";
import {Observable} from "rxjs/Observable";
import {OrderFullModel} from "../../../../../libraries/buyer/order/models/order-full.model";
import {ProductsService} from "../../../../products/products.service";
import {EStateModel} from "../../../../../libraries/common/utils/state/state-model.enum";
import {appConfig} from "../../../../../app.config";
import {OrdersCacheService} from "../orders-cache/orders-cache.service";
import {ProductFullModel} from "../../../../products/models/product-full-model.model";

export interface ISumOrdersItem {
    productId: string;
    ingredients: number;
    product: number;
}

export interface IAddOrDeleteAdditionalCallback {
    productId: string;
    ingredientId: string;
}

@Component({
    selector: 'buyer-orders-item',
    templateUrl: './buyer-orders-item.component.html',
    styleUrls: ['./buyer-orders-item.component.scss']
})
export class BuyerOrdersItemComponent implements OnChanges {

    @Input() public formItem: FormGroup = new FormGroup({
        id: new FormControl(''),
        count: new FormControl(0),
        salePercent: new FormControl(0),
        additional: new FormArray([]),
        preOrder: new FormControl(false)
    });
    @Input() public order: OrderFullModel = new OrderFullModel(null);
    @Input() public mode: EAbstractBuyerMode = EAbstractBuyerMode.VIEW;
    @Input() public index: number = null;

    @Output() public deleteProduct: EventEmitter<number> = new EventEmitter<number>();
    @Output() public addNewIngredient: EventEmitter<IAddOrDeleteAdditionalCallback> = new EventEmitter<IAddOrDeleteAdditionalCallback>();
    @Output() public deleteIngredient: EventEmitter<IAddOrDeleteAdditionalCallback> = new EventEmitter<IAddOrDeleteAdditionalCallback>();
    @Output() public updateSumSellPriceProduct: EventEmitter<ISumOrdersItem> = new EventEmitter<ISumOrdersItem>();

    public apiUrl: string = appConfig.apiUrl;
    public buyerOrdersItemProductPhotosModel$: Observable<BuyerOrdersItemProductPhotosModel>;
    public productCountDif: number = null;
    private productCountDifOld: number = null;
    private oldProductCount: number = null;
    private cacheFormProductCount: number = null;
    private initPreOrder: boolean = false;

    private _sum: ISumOrdersItem = {
        productId: '',
        ingredients: 0,
        product: 0
    };

    public constructor(
        private productsService: ProductsService,
        private ordersCacheService: OrdersCacheService
    ) {

    }

    public ngOnChanges(): void {
        this.initPreOrder = this.formItem.get('preOrder').value;
        this.cacheFormProductCount = this.formItem.get('count').value;
        this.oldProductCount = this.formItem.get('count').value;
        this.productCountDif = this.order && this.order.product ? this.order.product.count : 0;
        this.productCountDifOld = this.productCountDif;
        if (this.order && this.order.product && this.order.product.id) {
            this.buyerOrdersItemProductPhotosModel$ = this.getPhotos$(this.order.product.id);
            this.updateSumSellPriceProductEE();
        }
    }

    public changePreOrder(): void {
        this.formItem.get('count').setValue(this.cacheFormProductCount);
        this.oldProductCount = this.formItem.get('count').value;
        this.productCountDif = this.order && this.order.product ? this.order.product.count : 0;
        if (this.formItem.get('preOrder').value) {
            if (!this.initPreOrder) {
                this.productCountDif = this.order && this.order.product ? this.order.product.count + this.formItem.get('count').value: 0;
            }
        } else {
            if (this.initPreOrder) {
                this.productCountDif = this.order && this.order.product ? this.order.product.count - this.formItem.get('count').value : 0;
            }
        }
        this.productCountDifOld = this.productCountDif;
        this.changeCount();
    }

    private getPhotos$(id: string): Observable<BuyerOrdersItemProductPhotosModel> {
        return this.productsService.getPhotos(id)
            .map((photos: string[]) => new BuyerOrdersItemProductPhotosModel(photos, EStateModel.LOADED))
            .catch(() => Observable.of(new BuyerOrdersItemProductPhotosModel([], EStateModel.ERROR)))
            .startWith(new BuyerOrdersItemProductPhotosModel([], EStateModel.LOADING))
    }

    public isModeCreate(): boolean {
        return this.mode === EAbstractBuyerMode.CREATE;
    }

    public isModeEdit(): boolean {
        return this.mode === EAbstractBuyerMode.EDIT;
    }

    public isModeView(): boolean {
        return this.mode === EAbstractBuyerMode.VIEW;
    }

    public deleteProductEE(): void {
        this.deleteProduct.emit(this.index);
    }

    public changeCount(): void {
        this.updateSumSellPriceProductEE();
        if (!this.formItem.get('preOrder').value) {
            if (this.oldProductCount > this.formItem.get('count').value) {
                this.minusFromOrderCount();
            } else {
                this.plusToOrderCount();
            }
            if (this.productCountDif < 0) {
                this.productCountDif = this.productCountDifOld;
                this.formItem.get('count').setValue(this.cacheFormProductCount);
            }
        }
        if (this.formItem.get('count').value < 0) {
            this.formItem.get('count').setValue(this.cacheFormProductCount);
        }
       this.ordersCacheService.onChangeCountProduct(this.formItem.get('count').value, this.order);
    }

    private minusFromOrderCount(): void {
        this.productCountDif = this.productCountDifOld + (this.oldProductCount - this.formItem.get('count').value);
    }

    private plusToOrderCount(): void {
        this.productCountDif = this.productCountDifOld - (this.formItem.get('count').value - this.oldProductCount);
    }

    public updateSumSellPriceIngredientsEE(sum: number): void {
        const prodId: string = this.order && this.order.product && this.order.product.id || '';
        this._sum = {ingredients: sum || 0, product: this._sum && this._sum.product || 0, productId: prodId};
        this.updateSumSellPriceProduct.emit(this._sum);
    }

    public updateSumSellPriceProductEE(): void {
        const count: number = this.formItem.get('count').value || 0;
        const prodId: string = this.order && this.order.product && this.order.product.id || '';
        const priceCurrent: number = this.order && this.order.product && this.order.product.priceCurrent || 0;
        this._sum = {ingredients: this._sum && this._sum.ingredients || 0, product: count && priceCurrent && priceCurrent * count|| 0, productId: prodId};
        this.updateSumSellPriceProduct.emit(this._sum);
    }

    public addNewIngredientEE(ingredientId: string): void {
        this.addNewIngredient.emit({
            productId: this.order && this.order.product && this.order.product.id,
            ingredientId: ingredientId
        });
    }

    public deleteIngredientEE(ingredientId: string): void {
        this.deleteIngredient.emit({
            productId: this.order && this.order.product && this.order.product.id,
            ingredientId: ingredientId
        });
    }

}