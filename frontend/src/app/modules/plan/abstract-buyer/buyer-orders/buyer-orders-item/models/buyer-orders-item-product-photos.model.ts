import {StateModel} from "../../../../../../libraries/common/utils/state/state.model";
import {EStateModel} from "../../../../../../libraries/common/utils/state/state-model.enum";

export interface IBuyerOrdersItemProductPhotos {
    photos: string[];
    state: EStateModel;
}

export class BuyerOrdersItemProductPhotosModel extends StateModel implements IBuyerOrdersItemProductPhotos {

    public constructor(
        public readonly photos: string[] = [],
        public readonly state: EStateModel = EStateModel.NONE
    ) {
        super(state);
    }

}