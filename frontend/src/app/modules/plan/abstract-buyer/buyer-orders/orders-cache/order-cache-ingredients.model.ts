export interface IOrderCacheIngredients {
    id: string;
    countOld: number;
    countNew: number;
}

export class OrderCacheIngredientsModel implements IOrderCacheIngredients {

    public readonly id: string;
    public readonly countOld: number;
    public readonly countNew: number;

    public constructor(src: IOrderCacheIngredients) {
        this.id = src && src.id || '';
        this.countOld = src && src.countOld || null;
        this.countNew = src && src.countOld || null;
    }
}