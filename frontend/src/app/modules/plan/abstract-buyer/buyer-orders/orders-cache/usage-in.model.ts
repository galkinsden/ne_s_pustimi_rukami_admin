export class IUsageIn {
    isProductIngredient: boolean;
    id: string;
}

export class UsageInModel {

    public readonly isProductIngredient: boolean;
    public readonly id: string;

    public constructor(src: IUsageIn) {
        this.isProductIngredient = src && src.isProductIngredient;
        this.id = src && src.id || '';
    }

}