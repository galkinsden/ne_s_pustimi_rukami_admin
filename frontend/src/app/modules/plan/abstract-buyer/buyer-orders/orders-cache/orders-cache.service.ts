import {Injectable} from "@angular/core";
import {OrderFullModel} from "../../../../../libraries/buyer/order/models/order-full.model";
import {OrderCacheIngredientsModel} from "./order-cache-ingredients.model";
import {ProductIngredientFullModel} from "../../../../products/models/product-ingredient-full.model";

@Injectable()
export class OrdersCacheService {

    private cacheOld: OrderCacheIngredientsModel[] = [];

    public setCacheOld(orderFullModel: OrderFullModel[]) {
        this.cacheOld = orderFullModel.reduce((acc: OrderCacheIngredientsModel[], r: OrderFullModel) => {
            const productIngredients: OrderCacheIngredientsModel[] = r && r.product && r.product.ingredients
                ? r.product.ingredients
                    .filter((a: ProductIngredientFullModel) => a && a.ingredient && a.ingredient.id)
                    .map((a: ProductIngredientFullModel) => {
                        return new OrderCacheIngredientsModel({
                            id: a.ingredient.id,
                            countOld: a.ingredient.count,
                            countNew: a.ingredient.count
                        })
                    })
                    .filter((ing: OrderCacheIngredientsModel) => ing && ing.id && !~acc.map((ing: OrderCacheIngredientsModel) => ing && ing.id || '').indexOf(ing.id))
                : [];
            acc = acc.concat(productIngredients);
            const orderAdditionalIngredients: OrderCacheIngredientsModel[] = r && r.additional
                ? r.additional
                    .filter((a: ProductIngredientFullModel) => a && a.ingredient && a.ingredient.id)
                    .map((a: ProductIngredientFullModel) => {
                        return new OrderCacheIngredientsModel({
                            id: a.ingredient.id,
                            countOld: a.ingredient.count,
                            countNew: a.ingredient.count
                        })
                    })
                    .filter((ing: OrderCacheIngredientsModel) => ing && ing.id && !~acc.map((ing: OrderCacheIngredientsModel) => ing && ing.id || '').indexOf(ing.id))
                : [];
            return acc.concat(orderAdditionalIngredients);
        },[]);
    }

    public getCacheOld(): OrderCacheIngredientsModel[] {
        return this.cacheOld;
    }

    public onChangeCountProduct(newValue: number, orderFullModel: OrderFullModel): void {
        console.log(newValue, orderFullModel);
    }

}