import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from "@angular/core";
import {BuyerOrdersModel} from "./models/buyer-orders.model";
import {Observable} from "rxjs/Observable";
import {IOrder} from "../../../../libraries/buyer/order/models/order.model";
import {FormArray, FormControl, FormGroup} from "@angular/forms";
import {EAbstractBuyerMode} from "../enum/abstract-buyer-mode.enum";
import {OrderFullModel} from "../../../../libraries/buyer/order/models/order-full.model";
import {ProductsService} from "../../../products/products.service";
import {EStateModel} from "../../../../libraries/common/utils/state/state-model.enum";
import {Subscription} from "rxjs/Subscription";
import {MatDialog} from "@angular/material";
import {BuyerAddProductModalComponent} from "../buyer-add-product-modal/buyer-add-product-modal.component";
import {UnSnackBarService} from "../../../../libraries/common/ui/snackbar/un-snack-bar.service";
import {IProductIngredient} from "../../../products/models/product-ingredient.model";
import {ProductIngredientFullModel} from "../../../products/models/product-ingredient-full.model";
import {IngredientFullModel} from "../../../ingredient/models/ingredient-full-model.model";
import {IngredientsService} from "../../../ingredient/ingredients.service";
import {IAddOrDeleteAdditionalCallback, ISumOrdersItem} from "./buyer-orders-item/buyer-orders-item.component";
import {ProductFullModel} from "../../../products/models/product-full-model.model";
import {OrdersCacheService} from "./orders-cache/orders-cache.service";

export interface ISumBuyerOrders {
    product: number;
    ingredients: number;
}

@Component({
    selector: 'buyer-orders',
    templateUrl: './buyer-orders.component.html',
    styleUrls: ['./buyer-orders.component.scss']
})
export class BuyerOrdersComponent implements OnChanges {

    @Input() orders: IOrder[] = [];
    @Input() formGroupBuyer: FormGroup;
    @Input() formOrders: FormArray;
    @Input() mode: EAbstractBuyerMode = EAbstractBuyerMode.VIEW;

    @Output() updateSumSellPriceProduct: EventEmitter<number> = new EventEmitter<number>();

    public buyerOrders$: Observable<BuyerOrdersModel>;
    private _sum: Map<string, ISumBuyerOrders> = new Map();

    public cacheOld = {};

    public constructor(
        private productsService: ProductsService,
        private dialog: MatDialog,
        private unSnackBarService: UnSnackBarService,
        private ingredientsService: IngredientsService,
        private ordersCacheService: OrdersCacheService
    ) {

    }

    public ngOnChanges() {
        this.buyerOrders$ = this.getBuyersOrders$(this.orders);
    }

    private getOrderFullModels(orders: IOrder[]): Observable<OrderFullModel[]> {
        return this.productsService.getProductsFullByIds(orders.map((order: IOrder) => order.id))
            .combineLatest(this.ingredientsService.getIngredientsFullByIds(
                orders.reduce((acc: string[], order: IOrder) => {
                    return acc.concat(order.additional && order.additional.length ? order.additional.map((additional: IProductIngredient) => additional.id) : []);
                }, [])))
            .map(([productFullModel, ingredientsFullModels]: [ProductFullModel[], IngredientFullModel[]]) => {
                return productFullModel && productFullModel.length
                    ? productFullModel.reduce((acc: OrderFullModel[], pr: ProductFullModel) => {
                        const index: number = orders.map((or: IOrder) => or.id).indexOf(pr.id);
                        if (!!~index) {
                            const productIngredientsFullModels: ProductIngredientFullModel[] = orders[index] && orders[index].additional && orders[index].additional.length
                                ? orders[index].additional.reduce((acc: ProductIngredientFullModel[], additional: IProductIngredient) => {
                                    const ind: number = ingredientsFullModels.map((ingredientFullModel: IngredientFullModel) => ingredientFullModel.id).indexOf(additional.id);
                                    return !!~ind ? acc.concat(new ProductIngredientFullModel({count: additional.count, ingredient: ingredientsFullModels[ind]})) : acc;
                                }, [])
                                : [];
                            return acc.concat(new OrderFullModel({
                                salePercent: orders[index].salePercent || 0,
                                product: pr || null,
                                count: orders[index].count || 0,
                                preOrder: orders[index].preOrder,
                                additional: productIngredientsFullModels
                            }));
                        } else {
                            return acc;
                        }
                    }, [])
                    : [];
            })
    }

    private getBuyersOrders$(orders: IOrder[]): Observable<BuyerOrdersModel> {
        return (orders ? this.getOrderFullModels(orders) : Observable.of(null))
            .map((orderFullModel: OrderFullModel[]) => {
                this.clearFormArray(this.formOrders);
                orderFullModel.forEach((orderFull: OrderFullModel) => {
                    this.formOrders.push(this.createBuyerOrderFormGroup(orderFull));
                });
                this.ordersCacheService.setCacheOld(orderFullModel);
                this.cacheOld = this.ordersCacheService.getCacheOld();
                return new BuyerOrdersModel(
                    orderFullModel,
                    EStateModel.LOADED
                )
            })
            .catch(() => Observable.of(new BuyerOrdersModel([], EStateModel.ERROR)))
            .startWith(new BuyerOrdersModel([], EStateModel.LOADING))
    }

    private createBuyerOrderFormGroup(orderFull: OrderFullModel): FormGroup {
        const orderFormGroup: FormGroup = new FormGroup({
            id: new FormControl(orderFull && orderFull.product && orderFull.product.id || ''),
            count: new FormControl(orderFull && orderFull.count || 0, ),
            salePercent: new FormControl(orderFull && orderFull.salePercent || 0),
            preOrder: new FormControl(orderFull && (typeof orderFull.preOrder === 'boolean') ? orderFull.preOrder : false, ),
            additional: new FormArray([])
        }, [this.buyerOrderFormValidator.call(this, orderFull)]);
        return orderFormGroup;
    }

    private buyerOrderFormValidator(orderFull: OrderFullModel): any {
        return (formGroup: FormGroup) => {
            const count: number = formGroup.get('count') && formGroup.get('count').value || 0;
            const countProduct: number = orderFull && orderFull.product && orderFull.product.count || 0;
            const preOrder: boolean = formGroup.get('preOrder') && formGroup.get('preOrder').value;
            return !count
                ? {buyerOrderFormValidator: {valid: false}}
                : null;
        };
    }

    private clearFormArray(formArray: FormArray): void {
        while (formArray.length !== 0) {
            formArray.removeAt(0)
        }
    }

    public isModeCreate(): boolean {
        return this.mode === EAbstractBuyerMode.CREATE;
    }

    public isModeEdit(): boolean {
        return this.mode === EAbstractBuyerMode.EDIT;
    }

    public isModeView(): boolean {
        return this.mode === EAbstractBuyerMode.VIEW;
    }

    public getOrder(formOrder: FormGroup, orderFullModels: OrderFullModel[]): OrderFullModel {
        const index: number = orderFullModels && orderFullModels.length && formOrder.get('id') && formOrder.get('id').value
            ? orderFullModels.map((orderFullModel: OrderFullModel) => orderFullModel.product.id).indexOf(formOrder.get('id').value)
            : -1;
        return !!~index && orderFullModels[index] && orderFullModels[index] ? orderFullModels[index] : new OrderFullModel(null);
    }

    public addNewOrder(): void {
        const sub: Subscription = this.dialog
            .open(BuyerAddProductModalComponent, {panelClass: 'un-large-panel-class'})
            .afterClosed()
            .subscribe((productId: string) => {
                console.log('productId',productId)
                if (productId) {
                    if (!!~this.formOrders.value.filter((order: IOrder) => order && order.id).map((order: IOrder) => order && order.id).indexOf(productId)) {
                        this.unSnackBarService.openError('Товар уже добавлен в заказ');
                    } else {
                        this._sum.set(productId, {product: 0, ingredients: 0});
                       // this.buyerProductCacheService.addNewProduct(productId);
                        this.buyerOrders$ = this.getBuyersOrders$(
                            <IOrder[]>this.formOrders.value.concat(
                                <IOrder>{
                                    id: productId,
                                    count: 0,
                                    salePercent: 0,
                                    additional: [],
                                    preOrder: false
                                })
                        );
                    }
                }
                sub.unsubscribe();
            });
    }

    public deleteProduct(index: number): void {
        const productId: string = this.formOrders.at(index) && this.formOrders.at(index).get('id').value || '';
        this._sum.delete(productId);
        this.formOrders.removeAt(index);
        this.updateSum();
        //this.buyerProductCacheService.removeOneProduct(productId)
    }

    public updateSumSellPriceProductEE(buyerOrders: OrderFullModel[], sumProduct: ISumOrdersItem): void {
        const index: number = buyerOrders && buyerOrders.length
            ? buyerOrders
                .filter((orderFullModel: OrderFullModel) => orderFullModel && orderFullModel.product && orderFullModel.product.id)
                .map((orderFullModel: OrderFullModel) => orderFullModel.product.id)
                .indexOf(sumProduct.productId)
            : -1;
        const buyerOrder: OrderFullModel = !!~index && buyerOrders[index] ? buyerOrders[index] : null;

        if (buyerOrder) {
            this._sum.set(buyerOrder.product.id, {
                product: sumProduct.product,
                ingredients: sumProduct.ingredients
            });
        }

        this.updateSum();
    }

    public updateSum(): void {
        this.updateSumSellPriceProduct.emit(Array.from(this._sum.values()).reduce((sum: number, val: ISumBuyerOrders) => {
            return val ? sum + val.product + val.ingredients : sum;
        }, 0));
    }

    public addNewIngredient(addOrDeleteAdditionalCallback: IAddOrDeleteAdditionalCallback): void {
        //this.buyerProductCacheService.addNewAdditional(addOrDeleteAdditionalCallback && addOrDeleteAdditionalCallback.productId, addOrDeleteAdditionalCallback && addOrDeleteAdditionalCallback.ingredientId);
    }

    public deleteIngredient(addOrDeleteAdditionalCallback: IAddOrDeleteAdditionalCallback): void {
        //this.buyerProductCacheService.removeOneAdditional(addOrDeleteAdditionalCallback && addOrDeleteAdditionalCallback.productId, addOrDeleteAdditionalCallback && addOrDeleteAdditionalCallback.ingredientId);
    }

}