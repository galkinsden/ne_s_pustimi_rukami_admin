import {
    Component, EventEmitter, Inject, Input, OnChanges, OnDestroy, OnInit, Output,
    SimpleChanges
} from "@angular/core";
import {MatDatepickerInputEvent, MatDialog, MatDialogRef} from "@angular/material";
import {FormControl, FormGroup, Validators, FormArray} from "@angular/forms";
import {Moment} from "moment";
import * as moment from 'moment';
import {Subscription} from "rxjs/Subscription";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/finally';
import 'rxjs/add/observable/throw';
import {StateModel} from "../../../libraries/common/utils/state/state.model";
import {EStateModel} from "../../../libraries/common/utils/state/state-model.enum";
import {appConfig} from "../../../app.config";
import {EAbstractBuyerMode} from "./enum/abstract-buyer-mode.enum";
import {AbstractBuyerModel} from "./models/abstract-buyer.model";
import {UnSnackBarService} from "../../../libraries/common/ui/snackbar/un-snack-bar.service";
import {PlanService} from "../plan.service";
import {BuyerService} from "../../../libraries/buyer/buyer.service";
import {AbstractBuyerService} from "./abstract-buyer.service";
import {BuyerModel, IBuyer} from "../../../libraries/buyer/models/buyer.model";
import {AbstractBuyerModalComponent} from "../abstract-buyer-modal/abstract-buyer-modal.component";
import {ConnectorErrorModel} from "../../../libraries/common/utils/connector-error.model";
import {EStatus} from "../../../libraries/buyer/enum/status.enum";
import {EDeliveryTypes} from "../../../libraries/buyer/enum/delivery-types.enum";

@Component({
    selector: 'abstract-buyer',
    templateUrl: './abstract-buyer.component.html',
    styleUrls: ['./abstract-buyer.component.scss']
})
export class AbstractBuyerComponent extends StateModel implements OnInit, OnChanges {

    @Input() public buyerId: string = '';
    @Input() public mode: EAbstractBuyerMode = null;
    @Input() public dialogRef: MatDialogRef<AbstractBuyerComponent>;
    @Output() public selectBuyerEE: EventEmitter<string> = new EventEmitter();
    @Input() public mini: boolean = false;


    public buyerForm: FormGroup;
    public apiUrl: string = appConfig.apiUrl;
    public eStatus: string[] = Object.keys(EStatus).filter((status: EStatus) => status !== EStatus.OUTSIDE);
    public eDeliveryTypes = EDeliveryTypes;
    public mask = ['+', '7', ' ', '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

    public abstractBuyerModel$: Observable<AbstractBuyerModel>;

    public constructor(
        private planService: PlanService,
        private buyerService: BuyerService,
        private unSnackBarService: UnSnackBarService,
        private abstractBuyerService: AbstractBuyerService,
        private dialog: MatDialog
    ) {
        super(EStateModel.NONE);
    }

    public ngOnInit(): void {
        if (!this.buyerForm) {
            this.initBuyerForm();
        }
    }

    public ngOnChanges(simpleChanges: SimpleChanges) {
        if (!this.buyerForm) {
            this.initBuyerForm();
        }
        this.changesBuyerIdStream(this.buyerId)
    }

    private changesBuyerIdStream(id: string): void {
        id ? this.changeBuyerIdIfIdYes(id) : this.changeBuyerIdIfIdNo()
    }

    private changeBuyerIdIfIdYes(buyerId: string): void {

        this.abstractBuyerModel$ = this.getBuyer$(buyerId)
            .startWith(new AbstractBuyerModel(null, EStateModel.LOADING))
            .do((abstractBuyerModel: AbstractBuyerModel) => {
                this.initBuyer(abstractBuyerModel && abstractBuyerModel.buyer, abstractBuyerModel && abstractBuyerModel.buyer && abstractBuyerModel.buyer.id || buyerId);
            });
    }

    private changeBuyerIdIfIdNo(): void {

        this.abstractBuyerModel$ = this.createFullBuyer$()
            .do((abstractBuyerModel: AbstractBuyerModel) => {
                this.buyerId = abstractBuyerModel && abstractBuyerModel.buyer && abstractBuyerModel.buyer.id;
                this.initBuyer(abstractBuyerModel && abstractBuyerModel.buyer, abstractBuyerModel && abstractBuyerModel.buyer && abstractBuyerModel.buyer.id);
            });

    }

    private createFullBuyer$(): Observable<AbstractBuyerModel> {
        return Observable.of(new AbstractBuyerModel(new BuyerModel(null), EStateModel.LOADED));
    }

    private getBuyer$(buyerId: string): Observable<AbstractBuyerModel> {
        return this.buyerService.getBuyerById(buyerId)
            .map((buyer : IBuyer) => {
                return new AbstractBuyerModel(
                    new BuyerModel(buyer),
                    EStateModel.LOADED
                )
            })
            .catch(() => {
                return Observable.of(new AbstractBuyerModel(
                    new BuyerModel(null),
                    EStateModel.ERROR
                ))
            });

    }

    private initBuyer(buyer: BuyerModel, id: string): void {
        this.buyerForm.get('id').setValue(id);
        this.buyerForm.get('name').setValue(buyer && buyer.name || '');
        this.buyerForm.get('comment').setValue(buyer && buyer.comment || '');
        this.buyerForm.get('email').setValue(buyer && buyer.email || '');
        this.buyerForm.get('phone').setValue(buyer && buyer.phone || '');
        this.buyerForm.get('address').setValue(buyer && buyer.address || '');
        this.buyerForm.get('dateOrder').setValue(buyer && buyer.dateOrder && moment(buyer.dateOrder) || this.isModeCreate() ? moment() : '');
        this.buyerForm.get('dateDelivery').setValue(buyer && buyer.dateDelivery && buyer.dateDelivery[0] && moment(buyer.dateDelivery[0]).startOf('day') || '');
        this.buyerForm.get('timeDeliveryFrom').setValue(this.createTimeDelivery(buyer && buyer.dateDelivery && buyer.dateDelivery[0] ? moment(buyer.dateDelivery[0]) : moment().startOf('day')));
        this.buyerForm.get('timeDeliveryTo').setValue(this.createTimeDelivery(buyer && buyer.dateDelivery && buyer.dateDelivery[1] ? moment(buyer.dateDelivery[1]) : moment().startOf('day')));
        this.buyerForm.get('status').setValue(this.isModeCreate() ? EStatus.NEWEST : buyer && buyer.status || null);
        this.buyerForm.get('deliveryType').setValue(buyer && buyer.deliveryType || EDeliveryTypes.DELIVERY);
    }

    private createTimeDelivery(date: Moment): {hour: number, minute: number} {
        return {
            hour: date.hour() || 0,
            minute: date.minute() || 0
        }
    }

    private initBuyerForm(): void {
        this.buyerForm = new FormGroup({
            id: new FormControl(''),
            name: new FormControl('', Validators.required),
            comment: new FormControl(''),
            phone: new FormControl(''),
            email: new FormControl(''),
            address: new FormControl(''),
            dateOrder: new FormControl(''),
            dateDelivery: new FormControl(''),
            timeDeliveryFrom: new FormControl(''),
            timeDeliveryTo: new FormControl(''),
            sumPrices: new FormControl(''),
            status: new FormControl(EStatus.NEWEST),
            deliveryType: new FormControl(EDeliveryTypes.DELIVERY),
            orders: new FormArray([])
        });
    }

    public onSubmit(): void {
        if (this.buyerForm.valid) {
            this.state = EStateModel.LOADING;
            const sub: Subscription = this.submitFormData()
                .finally(() => sub.unsubscribe())
                .subscribe((res: string) => {
                    this.abstractBuyerService.setBuyerId(res);
                    this.unSnackBarService.openSucces(this.isModeEdit() ? 'Информация о покупателе успешно обновлена' : 'Новый покупатель создан');
                    this.dialogRef && this.dialogRef.close();
                    this.state = EStateModel.LOADED;
                    return res;
                },
                    (error: ConnectorErrorModel) => {
                        this.unSnackBarService.openError(error && error.description || '');
                        this.state = EStateModel.ERROR;
                        return Observable.throw(error);
                    })
        }
    }
    
    private submitFormData(): Observable<string> {
        return this.setOrUpdateBuyer(this.getBuyerRequest())
            .map((ingredientId: string) => ingredientId)
            .catch((ingredientId) => Observable.of(ingredientId || ''))
    }
    
    private getBuyerRequest(): IBuyer {
        return {
            id: this.buyerForm.getRawValue().id,
            name: this.buyerForm.getRawValue().name,
            comment: this.buyerForm.getRawValue().comment,
            phone: this.buyerForm.getRawValue().phone,
            email: this.buyerForm.getRawValue().email,
            address: this.buyerForm.getRawValue().address,
            dateOrder: this.buyerForm.getRawValue().dateOrder,
            dateDelivery: this.buyerForm.getRawValue().dateDelivery
                ? [
                    this.buyerForm.getRawValue().dateDelivery.set('hour', this.buyerForm.getRawValue().timeDeliveryFrom.hour).set('minute', this.buyerForm.getRawValue().timeDeliveryFrom.minute).toISOString(),
                    this.buyerForm.getRawValue().dateDelivery.set('hour', this.buyerForm.getRawValue().timeDeliveryTo.hour).set('minute', this.buyerForm.getRawValue().timeDeliveryTo.minute).toISOString()
                ]
                : [],
            status: this.buyerForm.getRawValue().status,
            orders: this.buyerForm.getRawValue().orders,
            deliveryType: this.buyerForm.getRawValue().deliveryType
        };
    }

    private setOrUpdateBuyer(buyer: IBuyer): Observable<string> {
        return (this.isModeEdit()
                ? this.buyerService.updateBuyer(buyer)
                : this.buyerService.setBuyer(buyer)
            )
            .map((ingredientId: string) => ingredientId)
            .catch(() => Observable.of(''))
    }

    public removeBuyer(buyerId: string): void {
        const subRemove: Subscription = this.buyerService.removeBuyer(buyerId)
            .map((res: boolean) => {
                this.abstractBuyerService.setBuyerId('');
                this.dialogRef && this.dialogRef.close();
                this.unSnackBarService.openSucces('Покупатель успешно перенесен в корзину');
                return res;
            })
            .catch((error: ConnectorErrorModel) => {
                this.abstractBuyerService.setBuyerId('');
                this.unSnackBarService.openError('Ошибка переноса покупателя в корзину');
                return null;
            })
            .subscribe(() => {
                subRemove.unsubscribe();
            })
    }

    public isModeCreate(): boolean {
        return this.mode === EAbstractBuyerMode.CREATE;
    }

    public isModeEdit(): boolean {
        return this.mode === EAbstractBuyerMode.EDIT;
    }

    public isModeView(): boolean {
        return this.mode === EAbstractBuyerMode.VIEW;
    }


    public editBuyer(buyerId: string) {
        let dialogRef = this.dialog.open(AbstractBuyerModalComponent, {panelClass: 'un-large-panel-class', data: {buyerId: buyerId, mode: EAbstractBuyerMode.EDIT}});
    }

    public selectBuyer(id: string): void {
        this.selectBuyerEE.emit(id)
    }

    public getStatusTitle(status: EStatus): string {
        return PlanService.getGroupTypeByBuyerStatus(status) ? PlanService.getGroupTypeByBuyerStatus(status).title : '';
    }

    public updateSumSellPriceProductEE(sum: number): void {
        this.buyerForm.get('sumPrices').setValue(sum || 0);
    }
}