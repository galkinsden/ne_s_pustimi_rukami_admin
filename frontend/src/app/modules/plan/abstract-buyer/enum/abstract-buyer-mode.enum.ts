export enum EAbstractBuyerMode {
    CREATE = 'CREATE',
    EDIT = 'EDIT',
    VIEW = 'VIEW'
}