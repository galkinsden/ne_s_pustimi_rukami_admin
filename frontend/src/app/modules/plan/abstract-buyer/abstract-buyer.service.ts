import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class AbstractBuyerService {

    private readonly model: BehaviorSubject<string> = new BehaviorSubject<string>('');

    public setBuyerId(id: string): AbstractBuyerService {
        return this.setModel(id);
    }

    public getObservableModel(): Observable<string> {
        return this.model.asObservable();
    }

    public getModel(): string {
        return this.model.getValue();
    }

    private setModel(model: string): AbstractBuyerService {
        this.model.next(model);
        return this;
    }

}
