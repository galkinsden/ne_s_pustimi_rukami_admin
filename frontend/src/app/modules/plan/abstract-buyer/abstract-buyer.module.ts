import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {AbstractBuyerComponent} from "./abstract-buyer.component";
import {
    MatButtonModule, MatDatepickerModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule,
    MatRadioModule, MatSelectModule, MatTooltipModule
} from "@angular/material";
import {UnSnackBarModule} from "../../../libraries/common/ui/snackbar/un-snack-bar.module";
import {UnProgressBarModule} from "../../../libraries/common/ui/progress/un-progress-bar.module";
import {PhotoLargeModalModule} from "../../../libraries/common/ui/photo-large-modal/photo-large-modal.module";
import {PhotoSmallModule} from "../../../libraries/common/ui/photo-small/photo-small.module";
import {ImageUploaderModule} from "../../../libraries/common/ui/image-uploader/image-uploader.module";
import {PreloaderModule} from "../../../libraries/common/ui/preloader/preloader.module";
import {SecureModule} from "../../../libraries/common/ui/img-secure/secure.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DisableControlModule} from "../../../libraries/common/utils/disable-control/disable-control.module";
import {EmptyAreaModule} from "../../../libraries/common/ui/empty-area/empty-area.module";
import {AbstractBuyerService} from "./abstract-buyer.service";
import {BuyerOrdersModule} from "./buyer-orders/buyer-orders.module";
import {BuyerAddProductModalComponent} from "./buyer-add-product-modal/buyer-add-product-modal.component";
import {BuyerAddProductModalModule} from "./buyer-add-product-modal/buyer-add-product-modal.module";
import {NgbTimepickerModule} from "@ng-bootstrap/ng-bootstrap";
import {UnMapAddressModule} from "../../../libraries/common/ui/un-map-address/un-map-address.module";
import {TextMaskModule} from "angular2-text-mask";


@NgModule({
    declarations: [AbstractBuyerComponent],
    exports: [AbstractBuyerComponent],
    imports: [
        CommonModule,
        RouterModule,
        MatDialogModule,
        UnSnackBarModule,
        MatIconModule,
        UnProgressBarModule,
        MatFormFieldModule,
        PhotoSmallModule,
        PhotoLargeModalModule,
        ImageUploaderModule,
        PreloaderModule,
        SecureModule,
        FormsModule,
        ReactiveFormsModule,
        DisableControlModule,
        MatInputModule,
        MatButtonModule,
        MatRadioModule,
        MatDatepickerModule,
        EmptyAreaModule,
        MatSelectModule,
        BuyerOrdersModule,
        BuyerAddProductModalModule,
        NgbTimepickerModule,
        MatTooltipModule,
        UnMapAddressModule,
        TextMaskModule
    ],
    providers: [AbstractBuyerService],
    entryComponents: [BuyerAddProductModalComponent]
})

export class AbstractBuyerModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: AbstractBuyerModule};
    }
}
