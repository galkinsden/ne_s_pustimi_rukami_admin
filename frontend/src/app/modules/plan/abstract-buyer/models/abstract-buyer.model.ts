import {StateModel} from "../../../../libraries/common/utils/state/state.model";
import {EStateModel} from "../../../../libraries/common/utils/state/state-model.enum";
import {BuyerModel} from "../../../../libraries/buyer/models/buyer.model";

export interface IAbstractBuyer {
    buyer: BuyerModel;
    state: EStateModel;
}

export class AbstractBuyerModel extends StateModel implements IAbstractBuyer {

    public constructor(
        public readonly buyer: BuyerModel = null,
        public readonly state: EStateModel = EStateModel.NONE
    ) {
        super(state);
    }
}