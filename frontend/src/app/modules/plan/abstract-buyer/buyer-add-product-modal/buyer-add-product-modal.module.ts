import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router'
import {MatButtonModule, MatDialogModule, MatIconModule} from "@angular/material";
import {BuyerAddProductModalComponent} from "./buyer-add-product-modal.component";
import {ProductsModule} from "../../../products/products.module";


@NgModule({
    declarations: [BuyerAddProductModalComponent],
    exports: [BuyerAddProductModalComponent],
    imports: [
        CommonModule,
        RouterModule,
        MatDialogModule,
        MatIconModule,
        MatButtonModule,
        ProductsModule
    ],
    providers: []
})

export class BuyerAddProductModalModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: BuyerAddProductModalModule};
    }
}
