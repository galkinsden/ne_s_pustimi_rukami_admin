import {Component, Inject, Input, OnDestroy, OnInit} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import 'rxjs/add/operator/finally';
import 'rxjs/add/observable/throw';
import {StateModel} from "../../../../libraries/common/utils/state/state.model";
import {EStateModel} from "../../../../libraries/common/utils/state/state-model.enum";

@Component({
    selector: 'buyer-add-product-modal',
    templateUrl: './buyer-add-product-modal.component.html',
    styleUrls: ['./buyer-add-product-modal.component.scss']
})
export class BuyerAddProductModalComponent extends StateModel implements OnInit {

    constructor(
        public dialogRef: MatDialogRef<BuyerAddProductModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
    ) {
        super(EStateModel.NONE);
    }

    public ngOnInit(): void {

    }

    public selectProduct(productId: string): void {
        this.close(productId);
    }

    public close(productId: string): void {
        this.dialogRef.close(productId);
    }
}