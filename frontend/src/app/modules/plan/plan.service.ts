import {Injectable} from "@angular/core";
import {BuyerService} from "../../libraries/buyer/buyer.service";
import {IConfigGroup} from "./interface/plan.interface";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {PlanListItemModel} from "./models/plan-list-item.model";
import {Moment} from "moment";
import {EStatus} from "../../libraries/buyer/enum/status.enum";

/**
 * Настройки даты в календаре
 */
export interface ICalendarDate {
    minDate: Moment;
    maxDate: Moment;
    currentDate: Moment;
}

@Injectable()
export class PlanService {

    public constructor(
        private buyerService: BuyerService
    ) {

    }

    public readonly countBuyers: BehaviorSubject<number> = new BehaviorSubject(null);

    public readonly selectedBuyer: BehaviorSubject<PlanListItemModel> = new BehaviorSubject(null);

    /**
     * Получаем список групп (коллекция Map)
     * @returns {Array<IConfigGroup>}
     */
    public static getGroups(): IConfigGroup[] {
        return Array.from(configGroup.values());
    }

    public static getGroupTypeByBuyerStatus(status: EStatus): IConfigGroup {
        switch (status) {
            case EStatus.NEWEST:
                return configGroup.get(EStatus.NEWEST);
            case EStatus.WAIT:
            return configGroup.get(EStatus.WAIT);
            case EStatus.SENT:
                return configGroup.get(EStatus.SENT);
            case EStatus.READY:
                return configGroup.get(EStatus.READY);
            case EStatus.PREORDER:
                return configGroup.get(EStatus.PREORDER);
            case EStatus.CANCELED:
                return configGroup.get(EStatus.CANCELED);
            case EStatus.FINISHED:
                return configGroup.get(EStatus.FINISHED);
            default:
                return configGroup.get(EStatus.OUTSIDE);
        }
    }

    public getDatesToCalendar(date: Moment): ICalendarDate {
        const minDate: Moment = null;
        const maxDate: Moment = null;
        return {
            minDate,
            maxDate,
            currentDate: date
        };
    }

}

export const configGroup: Map<EStatus, IConfigGroup> = new Map([
    [EStatus.NEWEST,    {status: EStatus.NEWEST, title: 'Новые', weight: 0}],
    [EStatus.WAIT,     {status: EStatus.WAIT, title: 'Ожидают сборку', weight: 1}],
    [EStatus.READY,     {status: EStatus.READY, title: 'Собранные', weight: 2}],
    [EStatus.SENT,     {status: EStatus.SENT, title: 'В пути', weight: 3}],
    [EStatus.PREORDER,     {status: EStatus.PREORDER, title: 'Предзаказ', weight: 4}],
    [EStatus.CANCELED,     {status: EStatus.CANCELED, title: 'Отмененные', weight: 5}],
    [EStatus.FINISHED,    {status: EStatus.FINISHED, title: 'Завершенные', weight: 6}],
]);