import {Component, Input, OnInit} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {BuyerModel} from "../../../libraries/buyer/models/buyer.model";
import {PlanService} from "../plan.service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/last';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/exhaustMap';
import 'rxjs/add/observable/interval';
import {PlanListModel} from "../models/plan-list.model";
import {IConfigGroup, IGroup} from "app/modules/plan/interface/plan.interface";
import * as moment from 'moment';
import {PlanListItemModel} from "../models/plan-list-item.model";
import {AuthService} from "../../../libraries/auth/auth.service";
import {Subject} from "rxjs/Subject";
import {PlanListControllerService} from "./plan-list-controller/plan-list-controller.service";
import {PlanListControllerModel} from "../models/plan-list-controller.model";
import {EStateModel} from "../../../libraries/common/utils/state/state-model.enum";
import {PlanViewService} from "../plan-view/plan-view.service";
import {merge} from "rxjs/observable/merge";
import {BuyerService} from "../../../libraries/buyer/buyer.service";
import {EStatus} from "../../../libraries/buyer/enum/status.enum";
import {PlanViewModel} from "../models/plan-view.model";
import {AbstractBuyerService} from "../abstract-buyer/abstract-buyer.service";
import {ConnectorErrorModel} from "../../../libraries/common/utils/connector-error.model";

interface IBuyersListResponse {
    state: EStateModel;
    list: BuyerModel[];
}

@Component({
    selector: 'plan-list',
    templateUrl: './plan-list.component.html',
    styleUrls: ['./plan-list.component.scss']
})
export class PlanListComponent implements OnInit {

    @Input() write: boolean = false;
    @Input() read: boolean = false;
    @Input() mini: boolean = false;

    public $model: Observable<PlanListModel>;
    private _clickSelectedBuyer$: Subject<string> = new Subject();
    /**
     * Время обновления списка
     * @type {number}
     */
    protected readonly intervalTime: number = 1000 * 60 * 3;

    public constructor(
        private planService: PlanService,
        private router: Router,
        private route: ActivatedRoute,
        private authService: AuthService,
        private planListControllerService: PlanListControllerService,
        private planViewService: PlanViewService,
        private buyerService: BuyerService,
        private abstractBuyerService: AbstractBuyerService
    ) {

    }

    public ngOnInit(): void {
        this.$model = this.planViewStream$()
            .switchMap((planViewModel: PlanViewModel) => {
                return merge(this.getBuyersStream(), this.getAutoUpdateStream())
                    .combineLatest(this.getSelectedIdNumberStream())
                    .map(([buyerListResponse, selectedId] : [IBuyersListResponse, string]) => {
                        return new PlanListModel(
                            this.groupBuyers(buyerListResponse.list),
                            buyerListResponse.list.length,
                            selectedId ? buyerListResponse.list.find((buyer: BuyerModel) => buyer.id === selectedId) :  null,
                            buyerListResponse.state
                        );
                    })
                    .catch(() => {
                        return Observable.of(
                            new PlanListModel(
                                this.groupBuyers([]),
                                0,
                                null,
                                EStateModel.ERROR
                            )
                        )
                    })
                    .do((model: PlanListModel) => {
                        this.planService.selectedBuyer.next(model.selected);
                        this.planService.countBuyers.next(model.total);
                        this.router.navigate(['./'], {
                            relativeTo: this.route,
                            replaceUrl: true,
                            queryParams: {
                                date: this.planListControllerService.getModel().dateDelivery.valueOf(),
                                id: model.selected ? model.selected.id : null
                            }
                        });
                    })
            })
    }

    private getAutoUpdateStream(): Observable<IBuyersListResponse> {
        return Observable
            .interval(this.intervalTime)
            .exhaustMap(() => this.buyerService.getBuyersByDateDeliveryOrStatusNew(this.planListControllerService.getModel().dateDelivery.toISOString())
                .map((res: BuyerModel[]) => ({state: EStateModel.LOADED, list: res}))
                .catch(() => Observable.of({state: EStateModel.ERROR, list: []}))
            )
    }

    public planViewStream$(): Observable<PlanViewModel> {
        return this.planViewService.getObservableModel();
    }

    public onClickToItem(item: PlanListItemModel): void {
        this._clickSelectedBuyer$.next(item ? item.id : null);

    }

    private getSelectedIdNumberStream(): Observable<string> {
        return this.route.queryParams
            .first()
            .map((params: Params): string => {
                const result: string = params['id'];
                return result || null;
            })
            .merge(this._clickSelectedBuyer$);
    }

    /**
     * Группировка записей по типу и их сортировка
     * @returns {IGroup[]}
     */
    private groupBuyers(list: BuyerModel[]): IGroup[] {
        const result: IGroup[] = PlanService
            .getGroups()
            .map((group: IGroup) => ({...group, list: []}));

        list.forEach((item: BuyerModel) => {
            const config: IConfigGroup = PlanService.getGroupTypeByBuyerStatus(item.status);
            const group: IGroup = result.find((g: IGroup) => g.status === config.status);
            if (group) {
                group.list.push(item);
            }
        });

        return result
            .sort((a: IGroup, b: IGroup) => a.weight - b.weight)
            .map((g: IGroup) => {
                g.list = g.list
                    .sort((a: BuyerModel, b: BuyerModel) => (a.dateDelivery[0] && b.dateDelivery[0]) ? moment(a.dateDelivery[0]).startOf('day').valueOf() - moment(b.dateDelivery[0]).startOf('day').valueOf() : 0)
                    .sort((a: BuyerModel, b: BuyerModel) => (a.name && b.name) ? a.name.localeCompare(b.name) : 0);
                return g;
            });
    }

    public getLowerStatus(status: EStatus): string {
        return status ? `is-${(status + '').toLowerCase()}` : '';
    }

    private getBuyersStream(): Observable<IBuyersListResponse> {
        return this.planListControllerService.getObservableModel()
            .combineLatest(this.abstractBuyerService.getObservableModel())
            .switchMap(([model, buyerId]: [PlanListControllerModel, string]) => {
                return this.buyerService.getBuyersByDateDeliveryOrStatusNew(model.dateDelivery.toISOString())
                    .map((res: BuyerModel[]) => ({
                        state: EStateModel.LOADED,
                        list: res
                    }))
                    .catch((err: ConnectorErrorModel) => Observable.of({state: EStateModel.ERROR, list: []}))
                    .startWith({state: EStateModel.LOADING, list: []})
            });
    }

    public getGroupIconByStatus(status: EStatus): string {
        switch (status) {
            case EStatus.NEWEST:
                return 'priority_high';
            case EStatus.FINISHED:
                return 'done';
            case EStatus.WAIT:
                return 'gavel';
            case EStatus.READY:
                return 'card_giftcard';
            case EStatus.PREORDER:
                return 'event';
            case EStatus.CANCELED:
                return 'close';
            case EStatus.SENT:
                return 'directions_car';
            default:
                return '';
        }
    }
}
