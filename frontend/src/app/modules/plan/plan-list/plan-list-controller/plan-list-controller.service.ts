import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Moment} from 'moment';
import {PlanListControllerModel} from "../../models/plan-list-controller.model";

/**
 * Сервис для контроля параметров отображения списков (дата, поисковая строчка, режим поиска)
 */
@Injectable()
export class PlanListControllerService {
    /**
     * Модель для хранения текущего состояний
     * @type {BehaviorSubject<PlanListControllerModel>}
     */
    private readonly model: BehaviorSubject<PlanListControllerModel> = new BehaviorSubject<PlanListControllerModel>(new PlanListControllerModel());

    /**
     * Устанавливает поисковую строчку
     * @param {string} searchString
     * @returns {PlanListControllerService}
     */
    public setSearch(searchString: string): PlanListControllerService {
        return this.setModel(new PlanListControllerModel(
            this.model.getValue().dateDelivery,
            !this.model.getValue().isSearchMode ?  searchString.length > 0 : true,
            searchString
        ));
    }

    /**
     * Сбрасываем режим поиска и поисковую строчку
     * @returns {PlanListControllerService}
     */
    public resetSearch(): PlanListControllerService {
        return this.setModel(new PlanListControllerModel(
            this.model.getValue().dateDelivery,
            false,
            ''
        ));
    }

    /**
     * Устанавливаем текущую дату
     * @param {number} date
     * @returns {PlanListControllerService}
     */
    public setDate(date: Moment): PlanListControllerService {
        if (!this.model.getValue().dateDelivery.isSame(date, 'days')) {
            return this.setModel(new PlanListControllerModel(date, this.model.getValue().isSearchMode, this.model.getValue().searchString));
        }
        return this;
    }

    /**
     * Получить наблюдаемую модель
     * @returns {Observable<PlanListControllerModel>}
     */
    public getObservableModel(): Observable<PlanListControllerModel> {
        return this.model.asObservable();
    }

    /**
     * Получить актуальную модель
     * @returns {PlanListControllerModel}
     */
    public getModel(): PlanListControllerModel {
        return this.model.getValue();
    }

    private setModel(model: PlanListControllerModel): PlanListControllerService {
        this.model.next(model);
        return this;
    }

}
