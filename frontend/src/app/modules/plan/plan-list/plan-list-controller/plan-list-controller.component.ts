import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {ICalendarDate, PlanService} from "../../plan.service";
import {ActivatedRoute, Params} from "@angular/router";
import * as moment from 'moment';
import {Moment} from 'moment';
import {PlanListControllerModel} from "../../models/plan-list-controller.model";
import {PlanListControllerService} from "./plan-list-controller.service";
import {Observable} from "rxjs/Observable";
import {Subscription} from "rxjs/Subscription";
import {MatDialog} from "@angular/material";
import {AbstractBuyerModalComponent} from "../../abstract-buyer-modal/abstract-buyer-modal.component";
import {EAbstractBuyerMode} from "../../abstract-buyer/enum/abstract-buyer-mode.enum";

@Component({
    selector: 'plan-list-controller',
    templateUrl: './plan-list-controller.component.html',
    styleUrls: ['./plan-list-controller.component.scss']
})
export class PlanListControllerComponent implements OnInit, OnDestroy{
    @Input() public disabledSearch: boolean = false;
    @Input() write: boolean = false;
    @Input() read: boolean = false;
    @Input() mini: boolean = false;
    private subscription: Subscription;
    public $model: Observable<PlanListControllerModel>;
    public maxDate: Moment = null;
    public minDate: Moment = null;

    constructor(
        private planListController: PlanListControllerService,
        private planService: PlanService,
        private route: ActivatedRoute,
        private dialog: MatDialog
    ) {
    }

    public ngOnInit(): void {
        this.subscription = this.route.queryParams
            .first()
            .map((params: Params): Moment => {
                const result: number = parseInt(params['date'] as string, 10);
                return isNaN(result) ? moment() : moment(result);
            })
            .map((date: Moment) => {
                return this.planService.getDatesToCalendar(date);
            })
            .subscribe((date: ICalendarDate) => {
                this.maxDate = date.maxDate;
                this.minDate = date.minDate;
                this.change(date.currentDate.startOf('day'));

            });
        this.$model = this.planListController.getObservableModel()
            .map((model: PlanListControllerModel) => {
                return model;
            });
    }

    public ngOnDestroy(): void {
        this.subscription && this.subscription.unsubscribe();
    }

    public change(value: Moment): void {
        this.planListController.setDate(value);
    }

    public resetSearch(): void {
        this.planListController.resetSearch();
    }

    public search(value: string): void {
        this.planListController.setSearch(value);
    }

    public createNew(): void {
        let dialogRef = this.dialog.open(AbstractBuyerModalComponent, {panelClass: 'un-large-panel-class', data: {buyerId: null, mode: EAbstractBuyerMode.CREATE}});
    }
}