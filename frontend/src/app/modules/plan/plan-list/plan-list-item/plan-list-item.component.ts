import {Component, Input, OnInit} from "@angular/core";
import {BuyerModel} from '../../../../libraries/buyer/models/buyer.model';
import * as moment from 'moment';
import {EStatus} from "../../../../libraries/buyer/enum/status.enum";

@Component({
    selector: 'plan-list-item',
    templateUrl: './plan-list-item.component.html',
    styleUrls: ['./plan-list-item.component.scss']
})
export class PlanListItemComponent {
    @Input() public searchString: string = '';
    @Input() public selected: boolean = false;
    @Input() public model: BuyerModel;
    @Input() public groupStatus: EStatus;
    @Input() public searchMode: boolean = false;

    constructor() {}

    public enumStatus = EStatus;

    public getLowerStatus(status: EStatus): string {
        return status ? `is-${(status + '').toLowerCase()}` : '';
    }

    public getFormatDate(dateDelivery: string): string {
        return moment(dateDelivery).format('DD.MM.YYYY HH:mm:ss')
    }
}