/**
 * Модель для отображения компонента PlanListComponent
 */
import {EStateModel} from "../../../libraries/common/utils/state/state-model.enum";
import {StateModel} from "../../../libraries/common/utils/state/state.model";
import {PlanListItemModel} from "./plan-list-item.model";
import {IGroup} from "app/modules/plan/interface/plan.interface";

export class PlanListModel extends StateModel {

    public constructor(
        public readonly groups: IGroup[] = [],
        public readonly total: number = 0,
        public readonly selected: PlanListItemModel = null,
        public readonly state: EStateModel = EStateModel.NONE
    ) {
        super(state);
    }
}