import {Moment} from 'moment';
import * as moment from 'moment';

/**
 * Модель состояния контроллера отвечающего за отображением списка
 */
export class PlanListControllerModel {
	constructor(
		public readonly dateDelivery: Moment = moment().startOf('day'),
		public readonly isSearchMode: boolean = false,
		public readonly searchString: string = '',
	) {}
}
