import {IPlanListItem} from "../interface/plan-list-item.interface";

export class PlanListItemModel implements IPlanListItem {

    public constructor(
        public readonly id: string = '',
        public readonly name: string = '',
        public readonly dateOrder: string = '',
    ) {

    }
}