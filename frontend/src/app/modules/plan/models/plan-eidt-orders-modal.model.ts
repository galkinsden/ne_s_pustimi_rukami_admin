import {OrderModel} from "../../../libraries/buyer/order/models/order.model";
import {StateModel} from "app/libraries/common/utils/state/state.model";
import {EStateModel} from "app/libraries/common/utils/state/state-model.enum";

export class PlanEditOrdersModalModel extends StateModel {

    public constructor(
        public readonly state: EStateModel = EStateModel.NONE
    ) {
        super(state);
    }
}