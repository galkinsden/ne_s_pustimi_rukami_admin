import {StateModel} from "../../../libraries/common/utils/state/state.model";
import {EStateModel} from "../../../libraries/common/utils/state/state-model.enum";
import {EAbstractBuyerMode} from "../abstract-buyer/enum/abstract-buyer-mode.enum";

export interface IPlanView {
    buyerId: string;
    state: EStateModel;
    mode: EAbstractBuyerMode;
}
export class PlanViewModel extends StateModel implements IPlanView {

    public constructor(
        public readonly buyerId: string = null,
        public readonly state: EStateModel = EStateModel.NONE,
        public readonly mode: EAbstractBuyerMode = EAbstractBuyerMode.VIEW
    ) {
        super(state);
    }
}