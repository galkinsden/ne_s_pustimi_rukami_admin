import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from "@angular/core";
import * as moment from 'moment';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/last';
import 'rxjs/add/observable/of';
import {EStateModel} from "../../../libraries/common/utils/state/state-model.enum";
import {IngredientsListItemModel} from "../../ingredient/models/ingredient-list-item.model";
import {appConfig} from "../../../app.config";
import {IIngredientView, IngredientsViewModel} from "../../ingredient/models/ingredient-view.model";
import {IIngredientFull, IngredientFullModel} from "../../ingredient/models/ingredient-full-model.model";
import {Observable} from "rxjs/Observable";
import {IngredientsService} from "../../ingredient/ingredients.service";
import {IPlanView, PlanViewModel} from "../models/plan-view.model";
import {PlanService} from "../plan.service";
import {PlanListItemModel} from "../models/plan-list-item.model";

@Component({
    selector: 'plan-view',
    templateUrl: './plan-view.component.html',
    styleUrls: ['./plan-view.component.scss']
})
export class PlanViewComponent {

    @Input() write: boolean = false;
    @Input() read: boolean = false;
    @Input() mini: boolean = false;
    @Output() public selectBuyerEE: EventEmitter<string> = new EventEmitter();

    public $model: Observable<PlanViewModel>;
    public disable: boolean = true;
    public apiUrl: string = appConfig.apiUrl;

    constructor(
        private planService: PlanService
    ) {

    }

    public ngOnInit(): void {
        this.$model = this.changeIngredientStream$();
    }

    public changeCountBuyerStream$(): Observable<EStateModel> {
        return this.planService.countBuyers
            .map((count: number) => count > 0 ? EStateModel.WAIT : EStateModel.NONE);
    }

    public changeIngredientStream$(): Observable<PlanViewModel> {
        return this.planService.selectedBuyer
            .combineLatest(this.changeCountBuyerStream$())
            .switchMap(([planListItemModel, state]: [PlanListItemModel, EStateModel]) => {
                return Observable.of({state: state, buyerId: planListItemModel && planListItemModel.id ? planListItemModel.id : ''})
            })
            .map((planViewResponse: IPlanView) => {
                return new PlanViewModel(
                    planViewResponse.buyerId,
                    planViewResponse.state
                )
            })
    }

    public selectBuyer(id: string): void {
        this.selectBuyerEE.emit(id)
    }
}