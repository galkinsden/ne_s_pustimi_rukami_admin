import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {PlanViewModel} from "../models/plan-view.model";


@Injectable()
export class PlanViewService {
    /**
     * Модель для хранения текущего состояний
     * @type {BehaviorSubject<PlanListControllerModel>}
     */
    private readonly model: BehaviorSubject<PlanViewModel> = new BehaviorSubject<PlanViewModel>(new PlanViewModel(''));

    public setBuyerId(buyerId: string): PlanViewService {
        return this.setModel(new PlanViewModel(buyerId));
    }

    /**
     * Получить наблюдаемую модель
     * @returns {Observable<PlanViewModel>}
     */
    public getObservableModel(): Observable<PlanViewModel> {
        return this.model.asObservable();
    }

    /**
     * Получить актуальную модель
     * @returns {PlanViewModel}
     */
    public getModel(): PlanViewModel {
        return this.model.getValue();
    }

    private setModel(model: PlanViewModel): PlanViewService {
        this.model.next(model);
        return this;
    }

}
