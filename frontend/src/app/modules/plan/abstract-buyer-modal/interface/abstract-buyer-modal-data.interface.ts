import {EAbstractBuyerMode} from "../../abstract-buyer/enum/abstract-buyer-mode.enum";

export interface IAbstractBuyerModalData {
    mode: EAbstractBuyerMode,
    buyerId: string
}