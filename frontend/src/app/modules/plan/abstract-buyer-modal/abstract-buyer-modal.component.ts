import {Component, Inject, Input, OnDestroy, OnInit} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material";
import 'rxjs/add/operator/finally';
import 'rxjs/add/observable/throw';
import {IAbstractBuyerModalData} from "./interface/abstract-buyer-modal-data.interface";
import {EAbstractBuyerMode} from "../abstract-buyer/enum/abstract-buyer-mode.enum";

@Component({
    selector: 'abstract-buyer-modal',
    templateUrl: './abstract-buyer-modal.component.html',
    styleUrls: ['./abstract-buyer-modal.component.scss']
})
export class AbstractBuyerModalComponent {

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: IAbstractBuyerModalData,
        public dialogRef: MatDialogRef<AbstractBuyerModalComponent>,
    ) {

    }

    public close(): void {
       this.dialogRef.close();
   }

    public isModeCreate(): boolean {
        return this.data.mode === EAbstractBuyerMode.CREATE;
    }

    public isModeEdit(): boolean {
        return this.data.mode === EAbstractBuyerMode.EDIT;
    }

    public isModeView(): boolean {
        return this.data.mode === EAbstractBuyerMode.VIEW;
    }

}