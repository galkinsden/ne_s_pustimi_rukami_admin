import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {AbstractBuyerModalComponent} from "./abstract-buyer-modal.component";
import {MatButtonModule, MatDialogModule, MatIconModule} from "@angular/material";
import {AbstractBuyerModule} from "../abstract-buyer/abstract-buyer.module";

@NgModule({
    declarations: [AbstractBuyerModalComponent],
    exports: [AbstractBuyerModalComponent],
    imports: [
        CommonModule,
        RouterModule,
        MatDialogModule,
        AbstractBuyerModule,
        MatIconModule,
        MatButtonModule
    ],
    providers: []
})

export class AbstractBuyerModalModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: AbstractBuyerModalModule};
    }
}
