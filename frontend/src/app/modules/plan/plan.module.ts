import {PlanComponent} from "./plan.component";
import {MatDatepickerModule, MatFormFieldModule, MatIconModule, MatOptionModule} from "@angular/material";
import {MatButtonModule, MatInputModule, MatSelectModule} from "@angular/material";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {PlanService} from "./plan.service";
import {ModuleWithProviders, NgModule} from "@angular/core";
import {BuyerService} from "../../libraries/buyer/buyer.service";
import {PlanListItemComponent} from "./plan-list/plan-list-item/plan-list-item.component";
import {PlanListControllerComponent} from "./plan-list/plan-list-controller/plan-list-controller.component";
import {PlanListComponent} from "./plan-list/plan-list.component";
import {PlanViewComponent} from "./plan-view/plan-view.component";
import {HighlightTextPipeModule} from "../../libraries/common/ui/highlight-text/highlight-text-pipe.module";
import {DateSpinnerModule} from "../../libraries/common/ui/date-spinner/date-spinner.module";
import {PlanListControllerService} from "./plan-list/plan-list-controller/plan-list-controller.service";
import {PlanViewService} from "./plan-view/plan-view.service";
import {PreloaderModule} from "../../libraries/common/ui/preloader/preloader.module";
import {NgbTimepickerModule} from "@ng-bootstrap/ng-bootstrap";
import {DisableControlModule} from "../../libraries/common/utils/disable-control/disable-control.module";
import {EmptyAreaModule} from "../../libraries/common/ui/empty-area/empty-area.module";
import {AbstractBuyerComponent} from "./abstract-buyer/abstract-buyer.component";
import {AbstractBuyerModalComponent} from "./abstract-buyer-modal/abstract-buyer-modal.component";
import {AbstractBuyerModule} from "./abstract-buyer/abstract-buyer.module";
import {AbstractBuyerModalModule} from "./abstract-buyer-modal/abstract-buyer-modal.module";
import {BuyerAddProductModalComponent} from "./abstract-buyer/buyer-add-product-modal/buyer-add-product-modal.component";
import {StickyModule} from "../../libraries/common/utils/sticky/sticky.module";

@NgModule({
    declarations: [
        PlanComponent,
        PlanListComponent,
        PlanListControllerComponent,
        PlanListItemComponent,
        PlanViewComponent
    ],
    exports     : [
        PlanComponent,
        PlanListComponent,
        PlanListControllerComponent,
        PlanListItemComponent,
        PlanViewComponent
    ],
    imports     : [
        CommonModule,
        FormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatDatepickerModule,
        MatButtonModule,
        HighlightTextPipeModule,
        MatFormFieldModule,
        FormsModule,
        MatSelectModule,
        ReactiveFormsModule,
        DateSpinnerModule,
        PreloaderModule,
        MatOptionModule,
        NgbTimepickerModule,
        DisableControlModule,
        EmptyAreaModule,
        AbstractBuyerModule,
        AbstractBuyerModalModule,
        MatIconModule,
        StickyModule
    ],
    providers: [PlanService, BuyerService, PlanListControllerService, PlanViewService],
    entryComponents: [AbstractBuyerComponent, AbstractBuyerModalComponent, BuyerAddProductModalComponent]
})
export class PlanModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: PlanModule};
    }
}
