import {Component, EventEmitter, Input, Output} from "@angular/core";
import {UserService} from "../../libraries/user/user.service";
import {IIngredient} from "../ingredient/models/ingredients.model";
import {UnSnackBarService} from "../../libraries/common/ui/snackbar/un-snack-bar.service";
import {IAbstractPermissions} from "../../libraries/user/permissions/abstract-permissions.interface";
import {Observable} from "rxjs/Observable";
import {AuthService} from "../../libraries/auth/auth.service";
import {IBuyer} from "../../libraries/buyer/models/buyer.model";

@Component({
    selector: 'plan',
    templateUrl: './plan.component.html',
    styleUrls: ['./plan.component.scss']
})
export class PlanComponent {
    @Input() mini: boolean = false;
    @Output() public selectBuyerEE: EventEmitter<string> = new EventEmitter();

    public static PATH: string = 'plan';
    public model$: Observable<IAbstractPermissions>;

    public constructor(
        private userService: UserService,
        private authService: AuthService,
        private unSnackBarService: UnSnackBarService
    ) {

    }

    public ngOnInit(): void {
        this.model$ = Observable.of({
            read: true,
            write: true
        });

    }

    public selectBuyer(buyerId: string): void {
        this.selectBuyerEE.emit(buyerId);
    }
}