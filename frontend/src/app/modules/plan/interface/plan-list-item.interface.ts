export interface IPlanListItem {
    id: string;
    name: string;
    dateOrder: string;
}