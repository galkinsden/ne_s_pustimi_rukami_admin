/**
 * Тип группы
 */
import {IPlanListItem} from "./plan-list-item.interface";
import {EStatus} from "../../../libraries/buyer/enum/status.enum";

/**
 * Конфигурация группы
 */
export interface IConfigGroup {
    status: EStatus;
    title: string; // русское название
    weight: number; // вес для сортировки группы
}

/**
 * Группа и ее список записей
 */
export interface IGroup extends IConfigGroup {
    list: IPlanListItem[];
}
