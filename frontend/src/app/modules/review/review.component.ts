import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {EAbstractProductMode} from "../products/abstract-product/enum/abstract-product-mode.enum";
import {AbstractReviewModel} from "./models/abstract-review.model";
import {Observable} from "rxjs/Observable";
import {EStateModel} from "../../libraries/common/utils/state/state-model.enum";
import {ConnectorErrorModel} from "../../libraries/common/utils/connector-error.model";
import {ReviewService} from "./review.service";
import {UnSnackBarService} from "../../libraries/common/ui/snackbar/un-snack-bar.service";
import {ReviewModel} from "./models/review.model";
import * as uuidv1 from 'uuid/v1';
import {ReviewCacheService} from "./review-cache.service";
import {EGender} from "./enum/gender.enum";

@Component({
    selector: 'un-review',
    templateUrl: 'review.component.html',
    styleUrls: ['./review.component.scss']
})
export class ReviewComponent implements OnChanges {

    @Input() public formReview: FormArray = new FormArray([]);
    @Input() public mode: EAbstractProductMode;
    @Input() public productId: string;

    public abstractReviewModel$: Observable<AbstractReviewModel>;

    public constructor(
        private reviewService: ReviewService,
        private unSnackBarService: UnSnackBarService,
        private reviewCacheService: ReviewCacheService
    ) {

    }

    public ngOnChanges(simpleChanges: SimpleChanges) {
        this.abstractReviewModel$ = this.getReview$(this.productId)
            .startWith(new AbstractReviewModel(null, EStateModel.LOADING))
            .do((abstractReviewModel: AbstractReviewModel) => {
                this.initReviews(abstractReviewModel && abstractReviewModel.reviews || []);
                this.reviewCacheService.setCacheOld(abstractReviewModel.reviews);
            });
    }

    public isModeCreate(): boolean {
        return this.mode === EAbstractProductMode.CREATE;
    }

    public isModeEdit(): boolean {
        return this.mode === EAbstractProductMode.EDIT;
    }

    public isModeView(): boolean {
        return this.mode === EAbstractProductMode.VIEW;
    }

    public getReview$(prodId: string): Observable<AbstractReviewModel> {
        //TODO разобраться
        this.isModeView() && this.resetReview();

        return this.reviewService.getReviewsByProductId(prodId)
            .map((reviewModels: ReviewModel[]) => {
                return new AbstractReviewModel(reviewModels, EStateModel.LOADED)
            })
            .catch((error: ConnectorErrorModel) => {
                this.unSnackBarService.openError(error && error.description ? error.description : 'Ошибка получения отзывов о товаре');
                return Observable.of(new AbstractReviewModel([], EStateModel.ERROR));
            })
    }

    private createReview(reviewModel: ReviewModel): FormGroup {
        return new FormGroup({
            id: new FormControl(reviewModel && reviewModel.id || uuidv1(), Validators.required),
            productId: new FormControl(reviewModel && reviewModel.productId || this.productId, Validators.required),
            name: new FormControl(reviewModel && reviewModel.name || '', Validators.required),
            description: new FormControl(reviewModel && reviewModel.description || '', Validators.required),
            isPositive: new FormControl(reviewModel && (typeof reviewModel.isPositive === 'boolean') ? reviewModel.isPositive : true),
            gender: new FormControl(reviewModel && reviewModel.gender ? reviewModel.gender : EGender.MALE)
        })
    }

    private initReviews(reviewModels: ReviewModel[]): void {
        reviewModels.forEach((reviewModel: ReviewModel) => {
            this.formReview.push(this.createReview(reviewModel))
        });
    }

    public addNewReview(): void {
        const newReviewForm: FormGroup = this.createReview(null);
        this.formReview.push(newReviewForm);
        this.reviewCacheService.addNew(newReviewForm.get('id').value);
    }

    public deleteReviewByIndex(index: number): void {
        const removedId: string = this.formReview.controls[index]
            && this.formReview.controls[index]
            && this.formReview.controls[index].get('id')
            ? this.formReview.controls[index].get('id').value
            : null;
        this.formReview.removeAt(index);
        removedId && this.reviewCacheService.removeOne(removedId);
    }

    public resetReview(): void {
        this.formReview = new FormArray([]);
    }
}