import {EGender} from "../enum/gender.enum";

export interface IReview {
    id: string; // id отзыва
    productId: string; // id продукта
    name: string; // ФИО отзыва
    description: string; // описание комментария
    isPositive: boolean; // позитивный или негативный отзыв
    gender: EGender;
}

export class ReviewModel implements IReview {

    public readonly id: string;
    public readonly productId: string;
    public readonly name: string;
    public readonly description: string;
    public readonly isPositive: boolean;
    public readonly gender: EGender;

    public constructor(src: IReview) {
        this.id = src && src.id || '';
        this.productId = src && src.productId || '';
        this.name = src && src.name || '';
        this.description = src && src.description || '';
        this.isPositive = src && src.isPositive;
        this.gender = src && src.gender || EGender.MALE;
    }

}