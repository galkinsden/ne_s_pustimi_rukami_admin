export class ReviewCacheItemModel {

    public constructor(
        private items: string[] = []
    ) {}

    public setItem(id: string): ReviewCacheItemModel {
        this.items.push(id);
        return this;
    }

    public removeItem(id: string): ReviewCacheItemModel {
        const index: number = this.getIndexFromItems(id);
        if (!!~index) {
            this.items.splice(index, 1);
        }
        return this;
    }

    public getIdFromItems(id: string): string {
        const index: number = this.getIndexFromItems(id);
        return !!~index ? this.items[index] : null;
    }

    public getIndexFromItems(id: string): number {
        return this.items.indexOf(id);
    }

    public clear(): ReviewCacheItemModel {
        this.items = [];
        return this;
    }

}