import {ReviewModel} from "./review.model";
import {EStateModel} from "../../../libraries/common/utils/state/state-model.enum";
import {StateModel} from "../../../libraries/common/utils/state/state.model";

export interface IAbstractReview {
    reviews: ReviewModel[];
    state: EStateModel;
}

export class AbstractReviewModel extends StateModel implements IAbstractReview {

    public constructor(
        public readonly reviews: ReviewModel[] = [],
        public readonly state: EStateModel = EStateModel.NONE
    ) {
        super(state);
    }
}