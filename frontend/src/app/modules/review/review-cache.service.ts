import {ReviewCacheItemModel} from "./models/review-cache-item.model";
import {ReviewModel} from "./models/review.model";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {ReviewService} from "./review.service";
import {ConnectorErrorModel} from "../../libraries/common/utils/connector-error.model";

export interface ISubmitData {
    created: ReviewModel[],
    removed: ReviewModel[],
    updated: ReviewModel[]
}

@Injectable()
export class ReviewCacheService {

    private created: ReviewCacheItemModel = new ReviewCacheItemModel([]);
    private removed: ReviewCacheItemModel = new ReviewCacheItemModel([]);
    private updated: ReviewCacheItemModel = new ReviewCacheItemModel([]);
    private cacheOld: ReviewModel[] = [];

    public constructor(
        private reviewService: ReviewService
    ) {
        this.created.clear();
        this.updated.clear();
        this.removed.clear();
        this.cacheOld = [];
    }

    public setCacheOld(reviewModels: ReviewModel[]): void {
        this.cacheOld = reviewModels;
    }

    public getCacheOld(): ReviewModel[] {
        return this.cacheOld;
    }

    public addNew(id: string): void {

        this.created.setItem(id);
    }

    public removeOne(id: string): void {
        if (!!~this.created.getIndexFromItems(id)) {
            this.created.removeItem(id)
        } else if (!!~this.updated.getIndexFromItems(id)) {
            this.updated.removeItem(id);
            this.removed.setItem(id);
        } else {
            this.removed.setItem(id);
        }
    }

    public getSubmitData(newFormValue: ReviewModel[]): ISubmitData {

        const createdElements: ReviewModel[] = newFormValue.filter((reviewModel: ReviewModel) => {
            return !!~this.created.getIndexFromItems(reviewModel.id)
        });

        const removedElements: ReviewModel[] = this.cacheOld.filter((reviewModel: ReviewModel) => {
            return !!~this.removed.getIndexFromItems(reviewModel.id)
        });

        const noRemovedAndCreatedCacheOld: ReviewModel[] = this.cacheOld.filter((reviewModel: ReviewModel) => {
            return !~this.created.getIndexFromItems(reviewModel.id) && !~this.removed.getIndexFromItems(reviewModel.id)
        });

        const noRemovedAndCreatedNewFormValue: ReviewModel[] = newFormValue.filter((reviewModel: ReviewModel) => {
            return !~this.created.getIndexFromItems(reviewModel.id) && !~this.removed.getIndexFromItems(reviewModel.id)
        });

        const updatedElements: ReviewModel[] = noRemovedAndCreatedNewFormValue.filter((reviewModel: ReviewModel) => {
            const index: number = noRemovedAndCreatedCacheOld.map((r: ReviewModel) => r.id).indexOf(reviewModel.id);
            if (!!~index) {
                const elem: ReviewModel = noRemovedAndCreatedCacheOld[index];
                return !!(elem && (elem.id !== reviewModel.id
                    || elem.name !== reviewModel.name
                    || elem.description !== reviewModel.description
                    || elem.gender !== reviewModel.gender
                    || elem.isPositive !== reviewModel.isPositive));
            } else {
                return false;
            }
        });

        return {
            removed: removedElements,
            created: createdElements,
            updated: updatedElements,
        }
    }

    public changeReviews(submitReviews: ISubmitData, productId: string) : Observable<boolean> {
        return this.setReviewsById(submitReviews.created.map((r: ReviewModel) => new ReviewModel({...r, ...{productId: productId}})))
            .combineLatest(this.setUpdateReviewsByProductId(submitReviews.updated), this.setRemoveReviewsByIds(submitReviews.removed.map((r: ReviewModel) => r.id)))
            .map(() => true)
            .catch(() => Observable.throw(false));
    }

    public setRemoveReviewsByIds(reviewIds: string[]): Observable<boolean> {
        return (reviewIds && reviewIds.length ? this.reviewService.setRemoveReviewsByIds(reviewIds) : Observable.of(true))
            .map((r: boolean) => r)
            .catch((r: ConnectorErrorModel) => Observable.throw(false));
    }

    public setUpdateReviewsByProductId(reviews: ReviewModel[]): Observable<boolean> {
        return (reviews && reviews.length ? this.reviewService.setUpdateReviewsByProductId(reviews) : Observable.of(true))
            .map((r: boolean) => r)
            .catch((r: ConnectorErrorModel) => Observable.throw(false));
    }

    public setReviewsById(reviews: ReviewModel[]): Observable<boolean> {
        return  (reviews && reviews.length ? this.reviewService.setReviewsById(reviews) : Observable.of(true))
            .map((r: boolean) => r)
            .catch((r: ConnectorErrorModel) => Observable.throw(false));
    }

}