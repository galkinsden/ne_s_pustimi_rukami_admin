import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {EAbstractProductMode} from "../../products/abstract-product/enum/abstract-product-mode.enum";
import {ReviewService} from "../review.service";
import {UnSnackBarService} from "../../../libraries/common/ui/snackbar/un-snack-bar.service";
import {ReviewModel} from "../models/review.model";
import {EGender} from "../enum/gender.enum";

@Component({
    selector: 'un-review-item',
    templateUrl: 'review-item.component.html',
    styleUrls: ['./review-item.component.scss']
})
export class ReviewItemComponent {

    @Input() public formReviewItem: FormGroup = new FormGroup({
        id: new FormControl('', Validators.required),
        productId: new FormControl('', Validators.required),
        name: new FormControl('', Validators.required),
        description: new FormControl('', Validators.required),
        isPositive: new FormControl(true),
        gender: new FormControl(EGender.MALE)
    });
    @Input() public mode: EAbstractProductMode;
    @Input() public index: number;
    @Output() public deleteReview: EventEmitter<number> = new EventEmitter<number>();

    public EGender = EGender;

    public isModeCreate(): boolean {
        return this.mode === EAbstractProductMode.CREATE;
    }

    public isModeEdit(): boolean {
        return this.mode === EAbstractProductMode.EDIT;
    }

    public isModeView(): boolean {
        return this.mode === EAbstractProductMode.VIEW;
    }

    public deleteReviewByIndex(index: number): void {
        this.deleteReview.emit(index);
    }
}