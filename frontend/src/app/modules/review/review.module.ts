import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatButtonModule, MatFormFieldModule, MatIconModule, MatInputModule, MatRadioModule} from "@angular/material";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DisableControlModule} from "../../libraries/common/utils/disable-control/disable-control.module";
import {PreloaderModule} from "../../libraries/common/ui/preloader/preloader.module";
import {EmptyAreaModule} from "../../libraries/common/ui/empty-area/empty-area.module";
import {ReviewService} from "./review.service";
import {ReviewComponent} from "./review.component";
import {ReviewItemComponent} from "./review-item/review-item.component";
import {ReviewCacheService} from "./review-cache.service";


@NgModule({
    declarations: [ReviewComponent, ReviewItemComponent],
    exports     : [ReviewComponent, ReviewItemComponent],
    imports     : [
        MatInputModule,
        MatFormFieldModule,
        FormsModule,
        ReactiveFormsModule,
        DisableControlModule,
        PreloaderModule,
        EmptyAreaModule,
        MatIconModule,
        MatButtonModule,
        CommonModule,
        MatRadioModule
    ],
    providers   : [ReviewService, ReviewCacheService]
})

export class ReviewModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: ReviewModule};
    }
}
