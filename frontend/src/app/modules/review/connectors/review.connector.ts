import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {ConnectorErrorModel, IError} from '../../../libraries/common/utils/connector-error.model';
import {appConfig} from '../../../app.config';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {IReview} from "../models/review.model";

export interface IReviewConnectorGetReviewsByProductIdResponse {
    data: IReview[];
    error: IError;
}

export interface IReviewConnectorSetReviewByIdResponse {
    data: boolean;
    error: IError;
}

export interface IReviewConnectorSetUpdateReviewsByProductIdResponse {
    data: boolean;
    error: IError;
}

export interface IReviewConnectorSetRemoveReviewsByIdsResponse {
    data: boolean;
    error: IError;
}

export class ReviewConnector {

    constructor(private http: HttpClient) {}

    public getReviewsByProductId(productId: string): Observable<IReview[] | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/review-api/getReviewsByProductId`, { productId: productId })
            .map((res: IReviewConnectorGetReviewsByProductIdResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public setReviewsById(reviews: IReview[]): Observable<boolean | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/review-api/setReviewsById`, reviews)
            .map((res: IReviewConnectorSetReviewByIdResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public setUpdateReviewsByProductId(reviews: IReview[]): Observable<boolean | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/review-api/setUpdateReviewsByProductId`, reviews)
            .map((res: IReviewConnectorSetUpdateReviewsByProductIdResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public setRemoveReviewsByIds(reviewIds: string[]): Observable<boolean | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/review-api/setRemoveReviewsByIds`, reviewIds)
            .map((res: IReviewConnectorSetRemoveReviewsByIdsResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    private handleError(res: IError): Observable<ConnectorErrorModel> {
        return Observable.throw(new ConnectorErrorModel(res));
    }
}
