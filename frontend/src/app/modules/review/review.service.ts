import {Injectable} from "@angular/core";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {ConnectorErrorModel} from "../../libraries/common/utils/connector-error.model";
import {
    IReviewConnectorSetRemoveReviewsByIdsResponse, IReviewConnectorSetUpdateReviewsByProductIdResponse,
    ReviewConnector
} from "./connectors/review.connector";
import {IReview, ReviewModel} from "./models/review.model";
import {appConfig} from "../../app.config";

@Injectable()
export class ReviewService {

    private reviewConnector: ReviewConnector;

    public constructor(http: HttpClient) {
        this.reviewConnector = new ReviewConnector(http);
    }

    public getReviewsByProductId(productId: string): Observable<IReview[] | ConnectorErrorModel> {
        return this.reviewConnector.getReviewsByProductId(productId)
            .map((res: IReview[]) => res.map((review: IReview) => new ReviewModel(review)))
    }

    public setReviewsById(reviews: IReview[]): Observable<boolean | ConnectorErrorModel> {
        return this.reviewConnector.setReviewsById(reviews);
    }

    public setUpdateReviewsByProductId(reviews: IReview[]): Observable<boolean | ConnectorErrorModel> {
        return this.reviewConnector.setUpdateReviewsByProductId(reviews);
    }

    public setRemoveReviewsByIds(reviewIds: string[]): Observable<boolean | ConnectorErrorModel> {
        return this.reviewConnector.setRemoveReviewsByIds(reviewIds);
    }

}