import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {SettingsModel} from "./settings.model";
import {SettingsItemModel} from "./settings-item.model";
import {ESettingsCode} from "./settings-code.enum";
import {AuthService} from "../../libraries/auth/auth.service";
import {UserService} from "../../libraries/user/user.service";
import {EPermissionsCode} from "../../libraries/user/permissions/permissions-code.enum";
import {Observable} from "rxjs/Observable";
import {ConnectorErrorModel} from "../../libraries/common/utils/connector-error.model";
import {UnSnackBarService} from "../../libraries/common/ui/snackbar/un-snack-bar.service";

const SETTINGS: SettingsItemModel[] = [
  new SettingsItemModel({
      name: 'Доступ',
      code: ESettingsCode.permissions,
      action: null,
      icon: 'lock',
      disable: false
  }),
    new SettingsItemModel({
        name: 'План приема',
        code: ESettingsCode.plan,
        action: null,
        icon: 'list',
        disable: true
    }),
    new SettingsItemModel({
        name: 'Пользователи',
        code: ESettingsCode.users,
        action: null,
        icon: 'people',
        disable: true
    })
];

/**
 * Настройки
 */
@Component({
	selector: 'settings',
	templateUrl: './settings.component.html',
    styleUrls: ['./settings-component.scss']
})
export class SettingsComponent implements OnInit {
    public static PATH: string = 'settings';
    public model$: Observable<SettingsModel>;

    public constructor(
        private userService: UserService,
        private authService: AuthService,
        private unSnackBarService: UnSnackBarService
    ) {

    }

    public ngOnInit(): void {
        const userId: string = this.authService.getAuthUser().id;
        this.model$ = Observable.of(new SettingsModel(SETTINGS));

    }
}
