export enum ESettingsCode {
    permissions = 'permissions',
    plan = 'plan',
    users = 'users'
}