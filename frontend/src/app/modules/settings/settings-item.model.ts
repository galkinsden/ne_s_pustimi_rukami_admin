import {ESettingsCode} from "./settings-code.enum";

export interface ISettingsItem {
    name: string;
    icon: string;
    action: any;
    code: ESettingsCode;
    disable: boolean;
}

export class SettingsItemModel {

    public readonly name: string;
    public readonly action: any;
    public readonly code: ESettingsCode;
    public readonly icon: string;
    public readonly disable: boolean;

    public constructor(src: ISettingsItem) {
        this.name = src && src.name || '';
        this.icon = src && src.icon || '';
        this.action = src && src.action || null;
        this.code = src && src.code || null;
        this.disable = src && src.disable;
    }

}