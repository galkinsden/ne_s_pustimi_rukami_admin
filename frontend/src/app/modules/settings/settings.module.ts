import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {SettingsComponent} from "./settings.component";
import {MatIconModule} from "@angular/material";


@NgModule({
	declarations: [SettingsComponent],
	exports     : [SettingsComponent],
	imports     : [CommonModule, RouterModule, MatIconModule],
	providers   : []
})

export class SettingsModule {
	public static forRoot(): ModuleWithProviders {
		return {ngModule: SettingsModule};
	}
}
