import {SettingsItemModel} from "./settings-item.model";

export class SettingsModel {

    public constructor(
        public readonly items: SettingsItemModel[] = []
    ) {

    }

}