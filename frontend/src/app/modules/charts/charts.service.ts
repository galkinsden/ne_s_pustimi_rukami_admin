import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {ChartsConnector} from "../../connectors/charts.connector";

@Injectable()
export class ChartsService {

    private chartsConnector: ChartsConnector;

    constructor(http: HttpClient) {
        this.chartsConnector = new ChartsConnector(http);
    }

    public getPlanByGroup(userId: string): Observable<any> {
        return this.chartsConnector.getPlanByGroup(userId)
            .map((res: any) => res);
    }

}