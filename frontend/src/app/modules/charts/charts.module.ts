import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {ChartsComponent} from "./charts.component";
import {ChartsModule} from "ng2-charts";
import {ChartsService} from "./charts.service";


@NgModule({
	declarations: [ChartsComponent],
	exports     : [ChartsComponent],
	imports     : [CommonModule, RouterModule, ChartsModule],
	providers   : [ChartsService]
})

export class UnChartsModule {
	public static forRoot(): ModuleWithProviders {
		return {ngModule: UnChartsModule};
	}
}
