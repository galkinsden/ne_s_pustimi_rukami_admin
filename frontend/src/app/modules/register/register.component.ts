﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from '../../libraries/user/user.service';
import {StateModel} from '../../libraries/common/utils/state/state.model';
import {UserModel} from '../../libraries/user/user.model';
import {EStateModel} from '../../libraries/common/utils/state/state-model.enum';
import {UnSnackBarService} from '../../libraries/common/ui/snackbar/un-snack-bar.service';
import {ConnectorErrorModel} from 'app/libraries/common/utils/connector-error.model';
import {FormGroup, Validators, FormControl} from '@angular/forms';
import {SettingsModel} from "../settings/settings.model";
import {Observable} from "rxjs/Observable";
import {EPermissionsCode} from "../../libraries/user/permissions/permissions-code.enum";
import {AuthService} from "../../libraries/auth/auth.service";
import {RegisterModel} from "./register.model";

@Component({
    moduleId: module.id,
    templateUrl: 'register.component.html',
    styleUrls: ['./register.component.scss']
})

export class RegisterComponent implements OnInit {
    public state: StateModel = new StateModel(EStateModel.NONE);
    public model$: Observable<RegisterModel>;
    public userForm: FormGroup;
    public static PATH: string = 'register';

    constructor(
        private router: Router,
        private authService: AuthService,
        private userService: UserService,
        private unSnackBar: UnSnackBarService
    ) {
    }

    public ngOnInit(): void {
        this.userForm = new FormGroup({
            username: new FormControl('', Validators.required),
            password: new FormControl('', Validators.required),
            permissions: new FormGroup({
                plan: new FormGroup({
                    read: new FormControl(false),
                    write: new FormControl(false)
                }),
                settings: new FormGroup({
                    read: new FormControl(false),
                    write: new FormControl(false)
                }),
                charts: new FormGroup({
                    read: new FormControl(false),
                    write: new FormControl(false)
                }),
                register: new FormGroup({
                    read: new FormControl(false),
                    write: new FormControl(false)
                }),
                products: new FormGroup({
                    read: new FormControl(false),
                    write: new FormControl(false)
                }),
                ingredients: new FormGroup({
                    read: new FormControl(false),
                    write: new FormControl(false)
                })
            })
        })
    }

    public register(): void {
        if (this.userForm.valid) {
            this.state = new StateModel(EStateModel.LOADING);
            this.userService.create(new UserModel(this.userForm.value))
                .subscribe(
                    (data: boolean) => {
                        this.state = new StateModel(EStateModel.LOADED);
                        this.unSnackBar.openSucces(`Регистрация прошла успешно!`);
                        this.router.navigate(['/login']);
                    },
                    (error: ConnectorErrorModel) => {
                        this.unSnackBar.openError(`${error.description} ${error.code}`);
                        this.state = new StateModel(EStateModel.ERROR);
                    });
        }
    }
}
