import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {RegisterComponent} from "./register.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatInputModule} from "@angular/material";
import {PreloaderModule} from "../../libraries/common/ui/preloader/preloader.module";
import {UnSnackBarModule} from "../../libraries/common/ui/snackbar/un-snack-bar.module";


@NgModule({
    declarations: [RegisterComponent],
    exports: [RegisterComponent],
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatButtonModule,
        PreloaderModule,
        UnSnackBarModule,
        MatInputModule,
        MatCheckboxModule
    ],
    providers: []
})

export class RegisterModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: RegisterModule};
    }
}
